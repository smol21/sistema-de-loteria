const customString = (el, binding, meth) => {
    const doApply = binding.arg && binding.arg === 'upper';
    if (doApply && el.firstElementChild?.classList) el.firstElementChild.classList[meth]('text-uppercase');
}

const do_ = (el, binding) => customString(el, binding, 'add');
const unDo_ = (el, binding) => customString(el, binding, 'remove');

export const strCustom = {
    mounted: do_,
    updated: do_,
    unmounted: unDo_,
}