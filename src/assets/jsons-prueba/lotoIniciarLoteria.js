const theDate = (hour, min = 55) => {
    const now = new Date(), min55 = min == 55;
    const timeNowMinus5 = new Date(now.setHours(now.getHours(), now.getMinutes(), 0, 0) - 300000).getTime() / 1000;
    const srtDate = new Date(new Date().setHours(min55 ? (hour - 1) : hour, !min55 ? (min - 5) : min, 0, 0)).getTime() / 1000;
    if (srtDate > timeNowMinus5) return srtDate;
    return null;
};

// const wbtoa = (len, addZ = true) => Array.from(Array(len + 1), (_,i) => i).forEach(n => console.log(!addZ || n > 9 ? (n+': '+window.btoa(String(n))) : ('0'+n+': '+window.btoa('0'+n))));

const b2a = (str) => window.btoa(str);

const resDym = (sales) => ({
    ...(sales ? {
      "Precision": "7,2",
      "impuesto": "0",
      "limiteJugadas": 100,
      "estatusPunta": "0",
      "estatusTerminal": "1",
      "turno": [
          {
              "codigoTurno": "MDE=",
              "nombreTurno": "Mañana"
          },
          {
              "codigoTurno": "MDI=",
              "nombreTurno": "Tarde"
          },
          {
              "codigoTurno": "MDM=",
              "nombreTurno": "Noche"
          }
      ],
      "loterias": [
          {
              "codigoLoteria": "TDA1",
              "nombreLoteria": b2a("LOTERIA DE COJEDES"),
              "nombreResumido": b2a("LOT. COJEDES")
          },
          {
              "codigoLoteria": "TDAz",
              "nombreLoteria": b2a("LOTERIA DE ORIENTE"),
              "nombreResumido": b2a("LOT. ORIENTE")
          }
      ],
      "sorteosFrecuentes": [
          {
              "codigoProducto": "UDAwMw==",
              "numeroSorteo": "MTE2Mw=="
          },
          {
              "codigoProducto": "UDAwMw==",
              "numeroSorteo": "MTE2NA=="
          }
      ],
      "moneda": [
          {
              "codigoMoneda": "MDE=",
              "monedaBase": "MA==",
              "nombreMoneda": "BOLÍVAR",
              "simboloMoneda": "QnM=",
              "valorMoneda": "MQ==",
              "monMaximoTic": "5000",
              "monMinimoTic": "5",
              "permiteDecimal": "MQ=="
          },
          {
              "codigoMoneda": "MDI=",
              "monedaBase": "MQ==",
              "nombreMoneda": "DÓLAR",
              "simboloMoneda": "JA==",
              "valorMoneda": "MzMsODE2MQ==",
              "monMaximoTic": "100",
              "monMinimoTic": "1",
              "permiteDecimal": "MA=="
          },
          {
              "codigoMoneda": "MDM=",
              "monedaBase": "MA==",
              "nombreMoneda": "PESO",
              "simboloMoneda": "Q09Q",
              "valorMoneda": "MCwwMDg2NjI1Nw==",
              "monMaximoTic": "400000",
              "monMinimoTic": "500",
              "permiteDecimal": "MQ=="
          }
      ],
    } : {
      "maximoCorrida": "10",
      "maximoPermuta": "3",
    }),
    "tipoFuncion": [
        {
            "codigoTipoFuncion": "MQ==",
            "nombreTipoFuncion": "SERIE"
        },
        {
            "codigoTipoFuncion": "Mg==",
            "nombreTipoFuncion": "PERMUTA"
        },
        {
            "codigoTipoFuncion": "Mw==",
            "nombreTipoFuncion": "CORRIDA"
        }
    ],
    "modalidades": [
        {
            "codigoModalidad": "MDE=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TRIPLE FIJO"),
            "nombreResumido": b2a("TRIPLE FIJO"),
            "ordenSeleccion": "0,3" // o "0,2" o "-0,2"
        },
        {
            "codigoModalidad": "MDI=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("COLA"),
            "nombreResumido": b2a("COLA"),
            "ordenSeleccion": "-0,2"
        },
        {
            "codigoModalidad": "MDM=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("PUNTA"),
            "nombreResumido": b2a("PUNTA"),
            "ordenSeleccion": "0,2"
        },
        {
            "codigoModalidad": "MDQ=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TRIPLE A Y B"),
            "nombreResumido": b2a("TRIPLE A Y B"),
            "ordenSeleccion": "0,3"
        },
        {
            "codigoModalidad": "MDU=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TRIPLE Y SIGNO"),
            "nombreResumido": b2a("TRIPLE Y SIGNO"),
            "ordenSeleccion": "0,3"
        },
        {
            "codigoModalidad": "MDY=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("COLA Y SIGNO"),
            "nombreResumido": b2a("COLA Y SIGNO"),
            "ordenSeleccion": "-0,2"
        },
        {
            "codigoModalidad": "MDc=",
            "maximoPermuta": 0,
            "nombreModalidad": b2a("ANIMALITO"),
            "nombreResumido": b2a("ANIMALITO"),
            "ordenSeleccion": "0,3"
        },
        {
            "codigoModalidad": "MDg=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TRIPLE Y ANIMALITO"),
            "nombreResumido": b2a("TRIPLE + ANI"),
            "ordenSeleccion": "0,3"
        },
        {
            "codigoModalidad": "MDk=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TRIPLE Y CACHO"),
            "nombreResumido": b2a("TRIPLE + CAC"),
            "ordenSeleccion": "0,3"
        },
        {
            "codigoModalidad": "MTA=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("TERMINAL A Y TERMINAL B"),
            "nombreResumido": b2a("TER A + TE. B"),
            "ordenSeleccion": "-0,2"
        },
        {
            "codigoModalidad": "MTE=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("COLA Y CACHO"),
            "nombreResumido": b2a("COLA + CACHO"),
            "ordenSeleccion": "-0,2"
        },
        {
            "codigoModalidad": "MTI=",
            "maximoPermuta": 5,
            "nombreModalidad": b2a("COLA Y ANIMALITO"),
            "nombreResumido": b2a("COLA + ANIMALITO"),
            "ordenSeleccion": "-0,2"
        },
        {
            "codigoModalidad": "MTM=",
            "maximoPermuta": 0,
            "nombreModalidad": b2a("ANIMALITO Y CACHO"),
            "nombreResumido": b2a("ANI. + CACHO"),
            "ordenSeleccion": "0,3"
        }
    ],
    "productos": [
        {
            "codigoProducto": "UDAwMw==",
            "nombreProducto": b2a("CHANCE"),
            "nombreResumido": b2a("CHANCE"),
            "combinaSeccion": "0",
            "ordenProducto": "MQ==",
            "seccion": []	
        },
        {
            "codigoProducto": "UDAzMw==",
            "nombreProducto": b2a("CHANCE ANIMALITO"),
            "nombreResumido": b2a("CHANCE ANIMALITO"),
            "combinaSeccion": "0",
            "ordenProducto": "Mg==",
            "seccion": []
        },
        {
            "codigoProducto": "UDAzNg==",
            "nombreProducto": b2a("TRIPLE POPULAR"),
            "nombreResumido": b2a("TRIPLE POPULAR"),
            "combinaSeccion": "1",
            "ordenProducto": "Mw==",
            "seccion": [
                {
                    "codigoSeccion": "MDU="
                },
                {
                    "codigoSeccion": "MDY="
                },
                {
                    "codigoSeccion": "MDc="
                }
            ]
        }
    ],
    "secciones": [
        {
            "codigoSeccion": "MDE=",
            "codigoTipoSeccion": "MDI=",
            "nombreSeccion": b2a("ANIMALITO"),
            "nombreResumido": b2a("ANIMALITO")
        },
        {
            "codigoSeccion": "MDM=",
            "codigoTipoSeccion": "MDE=",
            "nombreSeccion": b2a("A Y B MILLONARIO"),
            "nombreResumido": b2a("A+B MILLONARIO")
        },
        {
            "codigoSeccion": "MDQ=",
            "codigoTipoSeccion": "MDE=",
            "nombreSeccion": b2a("ASTRAL"),
            "nombreResumido": b2a("ASTRAL")
        },
        {
            "codigoSeccion": "MDU=",
            "codigoTipoSeccion": "MDE=",
            "nombreSeccion": b2a("TRIPLE POPULAR"),
            "nombreResumido": b2a("TRIPLE POPULAR")
        },
        {
            "codigoSeccion": "MDY=",
            "codigoTipoSeccion": "MDE=",
            "nombreSeccion": b2a("TERMINAL POPULAR"),
            "nombreResumido": b2a("TE POPULAR")
        },
        {
            "codigoSeccion": "MDc=",
            "codigoTipoSeccion": "MDI=",
            "nombreSeccion": b2a("ANIMALITO POPULAR"),
            "nombreResumido": b2a("ANI. POPULAR")
        }
    ],
    "listsimbolo": [
        {
            "simbolo": [
                {
                    "codigoSimbolo": "MDk=",
                    "nombreResumido": "QUdV",
                    "nombreSimbolo": "QUdVSUxB"
                },
                {
                    "codigoSimbolo": "MDQ=",
                    "nombreResumido": "QUxB",
                    "nombreSimbolo": "QUxBQ1JBTg=="
                },
                {
                    "codigoSimbolo": "MzI=",
                    "nombreResumido": "QVJE",
                    "nombreSimbolo": "QVJESUxMQQ=="
                },
                {
                    "codigoSimbolo": "MDA=",
                    "nombreResumido": "QkFM",
                    "nombreSimbolo": "QkFMTEVOQQ=="
                },
                {
                    "codigoSimbolo": "MTg=",
                    "nombreResumido": "QlVS",
                    "nombreSimbolo": "QlVSUk8="
                },
                {
                    "codigoSimbolo": "MTI=",
                    "nombreResumido": "Q0FC",
                    "nombreSimbolo": "Q0FCQUxMTw=="
                },
                {
                    "codigoSimbolo": "MTk=",
                    "nombreResumido": "Q0FC",
                    "nombreSimbolo": "Q0FCUkE="
                },
                {
                    "codigoSimbolo": "MzA=",
                    "nombreResumido": "Q0FJ",
                    "nombreSimbolo": "Q0FJTUFO"
                },
                {
                    "codigoSimbolo": "MjI=",
                    "nombreResumido": "Q0FN",
                    "nombreSimbolo": "Q0FNRUxMTw=="
                },
                {
                    "codigoSimbolo": "MDE=",
                    "nombreResumido": "Q0FS",
                    "nombreSimbolo": "Q0FSTkVSTw=="
                },
                {
                    "codigoSimbolo": "MjM=",
                    "nombreResumido": "Q0VC",
                    "nombreSimbolo": "Q0VCUkE="
                },
                {
                    "codigoSimbolo": "MDM=",
                    "nombreResumido": "Q0lF",
                    "nombreSimbolo": "Q0lFTVBJRVM="
                },
                {
                    "codigoSimbolo": "MjA=",
                    "nombreResumido": "Q09D",
                    "nombreSimbolo": "Q09DSElOTw=="
                },
                {
                    "codigoSimbolo": "MzY=",
                    "nombreResumido": "Q1VM",
                    "nombreSimbolo": "Q1VMRUJSQQ=="
                },
                {
                    "codigoSimbolo": "MA==",
                    "nombreResumido": "REVM",
                    "nombreSimbolo": "REVMRklO"
                },
                {
                    "codigoSimbolo": "Mjk=",
                    "nombreResumido": "RUxF",
                    "nombreSimbolo": "RUxFRkFOVEU="
                },
                {
                    "codigoSimbolo": "MjU=",
                    "nombreResumido": "R0FM",
                    "nombreSimbolo": "R0FMTElOQQ=="
                },
                {
                    "codigoSimbolo": "MjE=",
                    "nombreResumido": "R0FM",
                    "nombreSimbolo": "R0FMTE8="
                },
                {
                    "codigoSimbolo": "MTE=",
                    "nombreResumido": "R0FU",
                    "nombreSimbolo": "R0FUTw=="
                },
                {
                    "codigoSimbolo": "MjQ=",
                    "nombreResumido": "SUdV",
                    "nombreSimbolo": "SUdVQU5B"
                },
                {
                    "codigoSimbolo": "MzU=",
                    "nombreResumido": "SklS",
                    "nombreSimbolo": "SklSQUZB"
                },
                {
                    "codigoSimbolo": "MzE=",
                    "nombreResumido": "TEFQ",
                    "nombreSimbolo": "TEFQQQ=="
                },
                {
                    "codigoSimbolo": "MDU=",
                    "nombreResumido": "TEVP",
                    "nombreSimbolo": "TEVPTg=="
                },
                {
                    "codigoSimbolo": "MDc=",
                    "nombreResumido": "TE9S",
                    "nombreSimbolo": "TE9STw=="
                },
                {
                    "codigoSimbolo": "MTM=",
                    "nombreResumido": "TU9O",
                    "nombreSimbolo": "TU9OTw=="
                },
                {
                    "codigoSimbolo": "MTY=",
                    "nombreResumido": "T1NP",
                    "nombreSimbolo": "T1NP"
                },
                {
                    "codigoSimbolo": "MTQ=",
                    "nombreResumido": "UEFM",
                    "nombreSimbolo": "UEFMT01B"
                },
                {
                    "codigoSimbolo": "MTc=",
                    "nombreResumido": "UEFW",
                    "nombreSimbolo": "UEFWTw=="
                },
                {
                    "codigoSimbolo": "Mjc=",
                    "nombreResumido": "UEVS",
                    "nombreSimbolo": "UEVSUk8="
                },
                {
                    "codigoSimbolo": "MzM=",
                    "nombreResumido": "UEVT",
                    "nombreSimbolo": "UEVTQ0FETw=="
                },
                {
                    "codigoSimbolo": "MDg=",
                    "nombreResumido": "UkFU",
                    "nombreSimbolo": "UkFUT04="
                },
                {
                    "codigoSimbolo": "MDY=",
                    "nombreResumido": "U0FQ",
                    "nombreSimbolo": "U0FQTw=="
                },
                {
                    "codigoSimbolo": "MTA=",
                    "nombreResumido": "VElH",
                    "nombreSimbolo": "VElHUkU="
                },
                {
                    "codigoSimbolo": "MDI=",
                    "nombreResumido": "VE9S",
                    "nombreSimbolo": "VE9STw=="
                },
                {
                    "codigoSimbolo": "MjY=",
                    "nombreResumido": "VkFD",
                    "nombreSimbolo": "VkFDQQ=="
                },
                {
                    "codigoSimbolo": "MzQ=",
                    "nombreResumido": "VkVO",
                    "nombreSimbolo": "VkVOQURP"
                },
                {
                    "codigoSimbolo": "Mjg=",
                    "nombreResumido": "WkFN",
                    "nombreSimbolo": "WkFNVVJP"
                },
                {
                    "codigoSimbolo": "MTU=",
                    "nombreResumido": "Wk9S",
                    "nombreSimbolo": "Wk9SUk8="
                }
            ],
            "codigoElemento": "MDU=",
            "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9DaGFuY2UvQW5pbWFsZXMve2NvZGlnb30ucG5n"
        },
        {
            "simbolo": [
                {
                    "codigoSimbolo": "MTE=",
                    "nombreResumido": "QUNV",
                    "nombreSimbolo": "QUNVQVJJTw=="
                },
                {
                    "codigoSimbolo": "MDE=",
                    "nombreResumido": "QVJJ",
                    "nombreSimbolo": "QVJJRVM="
                },
                {
                    "codigoSimbolo": "MDQ=",
                    "nombreResumido": "Q0FO",
                    "nombreSimbolo": "Q0FOQ0VS"
                },
                {
                    "codigoSimbolo": "MTA=",
                    "nombreResumido": "Q0FQ",
                    "nombreSimbolo": "Q0FQUklDT1JOSU8="
                },
                {
                    "codigoSimbolo": "MDg=",
                    "nombreResumido": "RVND",
                    "nombreSimbolo": "RVNDT1JQSU9O"
                },
                {
                    "codigoSimbolo": "MDM=",
                    "nombreResumido": "R0VN",
                    "nombreSimbolo": "R0VNSU5JUw=="
                },
                {
                    "codigoSimbolo": "MDU=",
                    "nombreResumido": "TEVP",
                    "nombreSimbolo": "TEVP"
                },
                {
                    "codigoSimbolo": "MDc=",
                    "nombreResumido": "TElC",
                    "nombreSimbolo": "TElCUkE="
                },
                {
                    "codigoSimbolo": "MTI=",
                    "nombreResumido": "UElT",
                    "nombreSimbolo": "UElTQ0lT"
                },
                {
                    "codigoSimbolo": "MDk=",
                    "nombreResumido": "U0FH",
                    "nombreSimbolo": "U0FHSVRBUklP"
                },
                {
                    "codigoSimbolo": "MDI=",
                    "nombreResumido": "VEFV",
                    "nombreSimbolo": "VEFVUk8="
                },
                {
                    "codigoSimbolo": "MDY=",
                    "nombreResumido": "VklS",
                    "nombreSimbolo": "VklSR08="
                }
            ],
            "codigoElemento": "MDQ=",
            "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9DaGFuY2UvIFNpZ25vcy97Y29kaWdvfS5wbmc="
        },
        {
            "simbolo": [
                {
                    "codigoSimbolo": "MDI=",
                    "nombreResumido": "Q09O",
                    "nombreSimbolo": "Q09OIENBQ0hP"
                },
                {
                    "codigoSimbolo": "MDE=",
                    "nombreResumido": "RE9C",
                    "nombreSimbolo": "RE9CTEUgQ0FDSE8="
                },
                {
                    "codigoSimbolo": "MDM=",
                    "nombreResumido": "TUVE",
                    "nombreSimbolo": "TUVESU8gQ0FDSE8="
                }
            ],
            "codigoElemento": "MDg=",
            "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0NhY2hvL3tjb2RpZ299LnBuZw=="
        },
        {
            "simbolo": [
                {
                    "codigoSimbolo": "MDk=",
                    "nombreResumido": "QUdV",
                    "nombreSimbolo": "QUdVSUxB"
                },
                {
                    "codigoSimbolo": "MDQ=",
                    "nombreResumido": "QUxB",
                    "nombreSimbolo": "QUxBQ1JBTg=="
                },
                {
                    "codigoSimbolo": "MzI=",
                    "nombreResumido": "QVJE",
                    "nombreSimbolo": "QVJESUxMQQ=="
                },
                {
                    "codigoSimbolo": "Mzg=",
                    "nombreResumido": "QkFM",
                    "nombreSimbolo": "QkFMTEVOQQ=="
                },
                {
                    "codigoSimbolo": "Mzk=",
                    "nombreResumido": "QlVG",
                    "nombreSimbolo": "QlVGQUxP"
                },
                {
                    "codigoSimbolo": "MTg=",
                    "nombreResumido": "QlVS",
                    "nombreSimbolo": "QlVSUk8="
                },
                {
                    "codigoSimbolo": "MTI=",
                    "nombreResumido": "Q0FC",
                    "nombreSimbolo": "Q0FCQUxMTw=="
                },
                {
                    "codigoSimbolo": "MTk=",
                    "nombreResumido": "Q0FC",
                    "nombreSimbolo": "Q0FCUkE="
                },
                {
                    "codigoSimbolo": "MzA=",
                    "nombreResumido": "Q0FJ",
                    "nombreSimbolo": "Q0FJTUFO"
                },
                {
                    "codigoSimbolo": "MjI=",
                    "nombreResumido": "Q0FN",
                    "nombreSimbolo": "Q0FNRUxMTw=="
                },
                {
                    "codigoSimbolo": "MDE=",
                    "nombreResumido": "Q0FS",
                    "nombreSimbolo": "Q0FSTkVSTw=="
                },
                {
                    "codigoSimbolo": "MjM=",
                    "nombreResumido": "Q0VC",
                    "nombreSimbolo": "Q0VCUkE="
                },
                {
                    "codigoSimbolo": "NDA=",
                    "nombreResumido": "Q0hJ",
                    "nombreSimbolo": "Q0hJVk8="
                },
                {
                    "codigoSimbolo": "MDM=",
                    "nombreResumido": "Q0lF",
                    "nombreSimbolo": "Q0lFTVBJRVM="
                },
                {
                    "codigoSimbolo": "MjA=",
                    "nombreResumido": "Q09D",
                    "nombreSimbolo": "Q09DSElOTw=="
                },
                {
                    "codigoSimbolo": "MzY=",
                    "nombreResumido": "Q1VM",
                    "nombreSimbolo": "Q1VMRUJSQQ=="
                },
                {
                    "codigoSimbolo": "Mzc=",
                    "nombreResumido": "REVM",
                    "nombreSimbolo": "REVMRklO"
                },
                {
                    "codigoSimbolo": "Mjk=",
                    "nombreResumido": "RUxF",
                    "nombreSimbolo": "RUxFRkFOVEU="
                },
                {
                    "codigoSimbolo": "MjU=",
                    "nombreResumido": "R0FM",
                    "nombreSimbolo": "R0FMTElOQQ=="
                },
                {
                    "codigoSimbolo": "MjE=",
                    "nombreResumido": "R0FM",
                    "nombreSimbolo": "R0FMTE8="
                },
                {
                    "codigoSimbolo": "MTE=",
                    "nombreResumido": "R0FU",
                    "nombreSimbolo": "R0FUTw=="
                },
                {
                    "codigoSimbolo": "MjQ=",
                    "nombreResumido": "SUdV",
                    "nombreSimbolo": "SUdVQU5B"
                },
                {
                    "codigoSimbolo": "MzU=",
                    "nombreResumido": "SklS",
                    "nombreSimbolo": "SklSQUZB"
                },
                {
                    "codigoSimbolo": "MzE=",
                    "nombreResumido": "TEFQ",
                    "nombreSimbolo": "TEFQQQ=="
                },
                {
                    "codigoSimbolo": "MDU=",
                    "nombreResumido": "TEVP",
                    "nombreSimbolo": "TEVPTg=="
                },
                {
                    "codigoSimbolo": "MDc=",
                    "nombreResumido": "TE9S",
                    "nombreSimbolo": "TE9STw=="
                },
                {
                    "codigoSimbolo": "MTM=",
                    "nombreResumido": "TU9O",
                    "nombreSimbolo": "TU9OTw=="
                },
                {
                    "codigoSimbolo": "MTY=",
                    "nombreResumido": "T1NP",
                    "nombreSimbolo": "T1NP"
                },
                {
                    "codigoSimbolo": "MTQ=",
                    "nombreResumido": "UEFM",
                    "nombreSimbolo": "UEFMT01B"
                },
                {
                    "codigoSimbolo": "MTc=",
                    "nombreResumido": "UEFW",
                    "nombreSimbolo": "UEFWTw=="
                },
                {
                    "codigoSimbolo": "Mjc=",
                    "nombreResumido": "UEVS",
                    "nombreSimbolo": "UEVSUk8="
                },
                {
                    "codigoSimbolo": "MzM=",
                    "nombreResumido": "UEVT",
                    "nombreSimbolo": "UEVTQ0FETw=="
                },
                {
                    "codigoSimbolo": "MDg=",
                    "nombreResumido": "UkFU",
                    "nombreSimbolo": "UkFUT04="
                },
                {
                    "codigoSimbolo": "MDY=",
                    "nombreResumido": "U0FQ",
                    "nombreSimbolo": "U0FQTw=="
                },
                {
                    "codigoSimbolo": "MTA=",
                    "nombreResumido": "VElH",
                    "nombreSimbolo": "VElHUkU="
                },
                {
                    "codigoSimbolo": "MDI=",
                    "nombreResumido": "VE9S",
                    "nombreSimbolo": "VE9STw=="
                },
                {
                    "codigoSimbolo": "MjY=",
                    "nombreResumido": "VkFD",
                    "nombreSimbolo": "VkFDQQ=="
                },
                {
                    "codigoSimbolo": "MzQ=",
                    "nombreResumido": "VkVO",
                    "nombreSimbolo": "VkVOQURP"
                },
                {
                    "codigoSimbolo": "Mjg=",
                    "nombreResumido": "WkFN",
                    "nombreSimbolo": "WkFNVVJP"
                },
                {
                    "codigoSimbolo": "MTU=",
                    "nombreResumido": "Wk9S",
                    "nombreSimbolo": "Wk9SUk8="
                }
            ],
            "codigoElemento": "MDY=",
            "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0FuaW1hbGl0b3Mve2NvZGlnb30ucG5n"
        }
    ],
    "componente": [
        {
            "codigoElemento": "MDE=",
            "nombreElemento": "TlVNRVJPQQ==",
            "tipoElemento": "MDE="
        },
        {
            "codigoElemento": "MDI=",
            "nombreElemento": "TlVNRVJPQg==",
            "tipoElemento": "MDE="
        },
        {
            "codigoElemento": "MDQ=",
            "nombreElemento": "QVNUUkFM",
            "tipoElemento": "MDI="
        },
        {
            "codigoElemento": "MDU=",
            "nombreElemento": "QU5JTUFMSVRPQQ==",
            "tipoElemento": "MDI="
        },
        {
            "codigoElemento": "MDg=",
            "nombreElemento": "Q0FDSE8=",
            "tipoElemento": "MDI="
        },
        {
            "codigoElemento": "MDY=",
            "nombreElemento": "QW5pbWFsaXRvUG9wdWxhcg==",
            "tipoElemento": "MDI="
        }
    ],
    "componenteSeccion": [
        {
            "codigoSeccion": "MDE=",
            "elemento": [
                {
                    "codigoElemento": "MDU=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "QW5pbWFsaXRv",
                    "maximoSimbolo": "Mw==", // -1: seleccion multiple, 0: sin seleccion, N: cantidad maxima de seleccion, *N: cantidad obligatoria
                    "ordenJerarquia": "",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                }
            ]
        },
        {
            "codigoSeccion": "MDM=",
            "elemento": [
                {
                    "codigoElemento": "MDE=",
                    "longitudElemento": "Mw==",
                    "descripcionEtiqueta": "VHJpcGxlIEE=",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                },
                {
                    "codigoElemento": "MDI=",
                    "longitudElemento": "Mw==",
                    "descripcionEtiqueta": "VHJpcGxlIEI=",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                }
            ]
        },
        {
            "codigoSeccion": "MDQ=",
            "elemento": [
                {
                    "codigoElemento": "MDE=",
                    "longitudElemento": "Mw==",
                    "descripcionEtiqueta": "VHJpcGxl",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                },
                {
                    "codigoElemento": "MDQ=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "U2lnbm8=",
                    "maximoSimbolo": "MTI=",
                    "ordenJerarquia": "",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "Mg==" } : {})
                }
            ]
        },
        {
            "codigoSeccion": "MDU=",
            "elemento": [
                {
                    "codigoElemento": "MDE=",
                    "longitudElemento": "Mw==",
                    "descripcionEtiqueta": "VHJpcGxl",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "Mw==",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                }
            ]
        },
        {
            "codigoSeccion": "MDY=",
            "elemento": [
                {
                    "codigoElemento": "MDE=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "VGVybWluYWwgQQ==",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "MQ==",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                },
                {
                    "codigoElemento": "MDI=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "VGVybWluYWwgQg==",
                    "maximoSimbolo": "MA==",
                    "ordenJerarquia": "Mg==",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                }
            ]
        },
        {
            "codigoSeccion": "MDc=",
            "elemento": [
                {
                    "codigoElemento": "MDU=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "QW5pbWFsaXRvIFBvcHVsYXI=",
                    "maximoSimbolo": "Mw==",
                    "ordenJerarquia": "NA==",
                    "dropdown": "MA==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                },
                {
                    "codigoElemento": "MDg=",
                    "longitudElemento": "Mg==",
                    "descripcionEtiqueta": "Q2FjaG8=",
                    "maximoSimbolo": "Mw==", // -1 es LTE=
                    "ordenJerarquia": "NQ==",
                    "dropdown": "MQ==",
                    ...(sales ? { "permiteEscritura": "MA==" } : {})
                }
            ]
        }
    ],
    "componenteModalidad": [
        {
            "codigoModalidad": "MDE=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("t")
                }
            ]
        },
        {
            "codigoModalidad": "MDI=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("c")
                }
            ]
        },
        {
            "codigoModalidad": "MDM=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("p")
                }
            ]
        },
        {
            "codigoModalidad": "MDQ=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("tA")
                }, 
                {
                    "codigoElemento": "MDI=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("tB")
                }
            ]
        },
        {
            "codigoModalidad": "MDU=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("t")
                }, 
                {
                    "codigoElemento": "MDQ=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("SIGNO")
                }
            ]
        },
        {
            "codigoModalidad": "MDY=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("c")
                }, 
                {
                    "codigoElemento": "MDQ=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("SIGNO")
                }
            ]
        },
        {
            "codigoModalidad": "MDc=",
            "componente": [ 
                {
                    "codigoElemento": "MDU=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("ANI")
                }
            ]
        },
        {
            "codigoModalidad": "MDg=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("t")
                }, 
                {
                    "codigoElemento": "MDU=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("ANI")
                }
            ]
        },
        {
            "codigoModalidad": "MDk=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("t")
                }, 
                {
                    "codigoElemento": "MDg=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("CACHO")
                }
            ]
        },
        {
            "codigoModalidad": "MTA=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("tmlA")
                }, 
                {
                    "codigoElemento": "MDI=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("tmlB")
                }
            ]
        },
        {
            "codigoModalidad": "MTE=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("c")
                }, 
                {
                    "codigoElemento": "MDg=",
                    "obligatorio": "MA==",
                    "nombreResumido": b2a("CACHO")
                }
            ]
        },
        {
            "codigoModalidad": "MTI=",
            "componente": [ 
                {
                    "codigoElemento": "MDE=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("c")
                }, 
                {
                    "codigoElemento": "MDU=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("ANI")
                }
            ]
        },
        {
            "codigoModalidad": "MTM=",
            "componente": [ 
                {
                    "codigoElemento": "MDU=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("ANI")
                }, 
                {
                    "codigoElemento": "MDg=",
                    "obligatorio": "MQ==",
                    "nombreResumido": b2a("CACHO")
                }
            ]
        }
    ],
    "sorteos": [
        ...(theDate(13) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDM="],
            "fechaCierre": 0,
            "hora": "01:00 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDQ=" : {
                    "codigoModalidad": "MDQ=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE A Y B",
            "numeroSorteo": "MDAwMQ==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(16,30) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDM="],
            "fechaCierre": 0,
            "hora": "04:30 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDQ=" : {
                    "codigoModalidad": "MDQ=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE A Y B",
            "numeroSorteo": "MDAwMg==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(19) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDM="],
            "fechaCierre": 0,
            "hora": "07:00 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.5,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 32.5,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 32.5,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDQ=" : {
                    "codigoModalidad": "MDQ=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 32.5,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE A Y B",
            "numeroSorteo": "MDAwMw==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(13) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDQ="],
            "fechaCierre": 0,
            "hora": "01:00 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDU=" : {
                    "codigoModalidad": "MDU=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDY=" : {
                    "codigoModalidad": "MDY=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ASTRAL",
            "numeroSorteo": "MDAwNA==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(16,30) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDQ="],
            "fechaCierre": 0,
            "hora": "04:30 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDU=" : {
                    "codigoModalidad": "MDU=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDY=" : {
                    "codigoModalidad": "MDY=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ASTRAL",
            "numeroSorteo": "MDAwNQ==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(19) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAwMw==",
            "seccion": ["MDQ="],
            "fechaCierre": 0,
            "hora": "07:00 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDM=" : {
                    "codigoModalidad": "MDM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDU=" : {
                    "codigoModalidad": "MDU=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDY=" : {
                    "codigoModalidad": "MDY=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ASTRAL",
            "numeroSorteo": "MDAwNg==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(11) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDE=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "11:00 AM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAwOQ==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(12) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "12:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxMA==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(13) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "01:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxMQ==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(14) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "02:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxMg==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(15) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "03:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxMw==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(16) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "04:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxNA==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(17) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDI=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "05:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxNQ==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(18) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "06:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxNg==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(19) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzMw==",
            "seccion": ["MDE="],
            "fechaCierre": 0,
            "hora": "07:00 PM",
            "modalidad": [
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "CHANCE ANIMALITO",
            "numeroSorteo": "MDAxNw==",
            "tipoSorteo": "MDI="
        }] : []),
        ...(theDate(19) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDAz",
            "codigoProducto": "UDAzNg==",
            "seccion": ["MDU=", "MDY=", "MDc="],
            "fechaCierre": 0,
            "hora": "07:00 PM",
            "modalidad": [
                !sales ? "MDE=" : {
                    "codigoModalidad": "MDE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDg=" : {
                    "codigoModalidad": "MDg=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDk=" : {
                    "codigoModalidad": "MDk=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MTA=" : {
                    "codigoModalidad": "MTA=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDI=" : {
                    "codigoModalidad": "MDI=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MTE=" : {
                    "codigoModalidad": "MTE=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MTI" : {
                    "codigoModalidad": "MTI",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MDc=" : {
                    "codigoModalidad": "MDc=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                },
                !sales ? "MTM=" : {
                    "codigoModalidad": "MTM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "TRIPLE POPULAR",
            "numeroSorteo": "MDA4OA==",
            "tipoSorteo": "MDE="
        }] : []),
        ...(theDate(20) ? [{
            "codigoAtajo": null,
            "codigoTurno": "MDM=",
            "codigoLoteria": "TDA1",
            "codigoProducto": "UDAzNg==",
            "codigoSeccion": null,
            "fechaCierre": 0,
            "hora": "08:00 PM",
            "modalidad": [
                // {
                //     "codigoModalidad": "MDc=",
                //     "monto": [
                //         {
                //             "codigoMoneda": "MDE=",
                //             "montoMinimo": 33.8161,
                //             "montoMultiplo": 0.01
                //         },
                //         {
                //             "codigoMoneda": "MDI=",
                //             "montoMinimo": 1,
                //             "montoMultiplo": 0.01
                //         },
                //         {
                //             "codigoMoneda": "MDM=",
                //             "montoMinimo": 5000,
                //             "montoMultiplo": 0.01
                //         }
                //     ]
                // },
                !sales ? "MTM=" : {
                    "codigoModalidad": "MTM=",
                    "monto": [
                        {
                            "codigoMoneda": "MDE=",
                            "montoMinimo": 33.8161,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDI=",
                            "montoMinimo": 1,
                            "montoMultiplo": 0.01
                        },
                        {
                            "codigoMoneda": "MDM=",
                            "montoMinimo": 5000,
                            "montoMultiplo": 0.01
                        }
                    ]
                }
            ],
            ...(sales ? { "moneda": ["MDE=","MDI="] } : {}),
            "nombreSorteo": "ANIMALITO POPULAR",
            "numeroSorteo": "MTIwMQ==",
            "seccion": [
                "MDc="
            ],
            "tipoSorteo": "MDI="
        }] : [])
    ],
    "tipoSorteo": [
        {
            "codigo": "MDE=",
            "descripcion": "TRIPLE"
        },
        {
            "codigo": "MDI=",
            "descripcion": "FIGURA"
        }
    ]
});

const resStat = {
    "Precision": "7,2",
    "componente": [
      {
        "codigoElemento": "MDAxNg==",
        "nombreElemento": "TlVNRVJP",
        "tipoElemento": "MDE="
      },
      {
        "codigoElemento": "MDAxMg==",
        "nombreElemento": "QU5JTUFMSVRPIEEtTE9UTyBBQ1RJVk8tR1JBTkpJVEE=",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAxMw==",
        "nombreElemento": "QU5JTUFMSVRPIEItTE9UTyBBQ1RJVk8tR1JBTkpJVEE=",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAxNA==",
        "nombreElemento": "RUwgR1JBTkpBWk8=",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAxNQ==",
        "nombreElemento": "R1VBQ0hBUk8=",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwMQ==",
        "nombreElemento": "TlVNRVJPQQ==",
        "tipoElemento": "MDE="
      },
      {
        "codigoElemento": "MDAwMg==",
        "nombreElemento": "TlVNRVJPQg==",
        "tipoElemento": "MDE="
      },
      {
        "codigoElemento": "MDAwMw==",
        "nombreElemento": "TlVFTVJPQw==",
        "tipoElemento": "MDE="
      },
      {
        "codigoElemento": "MDAwNA==",
        "nombreElemento": "QVNUUkFM",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwNQ==",
        "nombreElemento": "QU5JTUFMSVRPIEEuIENIQU5DRS1DQVJBQ0FT",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwNg==",
        "nombreElemento": "QU5JTUFMSVRPICBQT1BVTEFSIA==",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwNw==",
        "nombreElemento": "Q0FDSE8=",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwOA==",
        "nombreElemento": "QU5JTUFMSVRPIE9SSUVOVEUgQQ==",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAwOQ==",
        "nombreElemento": "QU5JTUFMSVRPIE9SSUVOVEUgQg==",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAxMA==",
        "nombreElemento": "QU5JTUFMSVRPIE9SSUVOVEUgQw==",
        "tipoElemento": "MDI="
      },
      {
        "codigoElemento": "MDAxMQ==",
        "nombreElemento": "QU5JTUFMSVRPIEIuIENBUkFDQVM=",
        "tipoElemento": "MDI="
      }
    ],
    "componenteModalidad": [
      {
        "codigoModalidad": "MTg=",
        "componente": [
          {
            "codigoElemento": "MDAwOA==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwOQ==",
            "nombreResumido": "Qg==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjE=",
        "componente": [
          {
            "codigoElemento": "MDAwOA==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDQ=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dEE=",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "nombreResumido": "dEI=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTA=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "Y0E=",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "nombreResumido": "Y0I=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDE=",
        "componente": [
          {
            "codigoElemento": "MDAxNg==",
            "nombreResumido": "dA==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDI=",
        "componente": [
          {
            "codigoElemento": "MDAxNg==",
            "nombreResumido": "Yw==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDU=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dA==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwNA==",
            "nombreResumido": "",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDY=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dA==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwNA==",
            "nombreResumido": "",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDk=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dA==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "nombreResumido": "",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MzI=",
        "componente": [
          {
            "codigoElemento": "MDAxNA==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjY=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dEE=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjQ=",
        "componente": [
          {
            "codigoElemento": "MDAwNQ==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjU=",
        "componente": [
          {
            "codigoElemento": "MDAxMQ==",
            "nombreResumido": "Qg==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "Mjc=",
        "componente": [
          {
            "codigoElemento": "MDAwMg==",
            "nombreResumido": "dEI=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTE=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "Yw==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "nombreResumido": "",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTM=",
        "componente": [
          {
            "codigoElemento": "MDAwNg==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "nombreResumido": "",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjA=",
        "componente": [
          {
            "codigoElemento": "MDAwOA==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTk=",
        "componente": [
          {
            "codigoElemento": "MDAwOA==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAwOQ==",
            "nombreResumido": "Qg==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAxMA==",
            "nombreResumido": "Qw==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjI=",
        "componente": [
          {
            "codigoElemento": "MDAwNg==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MDc=",
        "componente": [
          {
            "codigoElemento": "MDAwNQ==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "Mjk=",
        "componente": [
          {
            "codigoElemento": "MDAxMg==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAxMw==",
            "nombreResumido": "Qg==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "Mjg=",
        "componente": [
          {
            "codigoElemento": "MDAxMg==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MzE=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "dA==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAxMg==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MzM=",
        "componente": [
          {
            "codigoElemento": "MDAxNQ==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTQ=",
        "componente": [
          {
            "codigoElemento": "MDAwMQ==",
            "nombreResumido": "Y0E=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MTU=",
        "componente": [
          {
            "codigoElemento": "MDAwMg==",
            "nombreResumido": "Y0I=",
            "obligatorio": "MQ=="
          }
        ]
      },
      {
        "codigoModalidad": "MjM=",
        "componente": [
          {
            "codigoElemento": "MDAwNQ==",
            "nombreResumido": "QQ==",
            "obligatorio": "MQ=="
          },
          {
            "codigoElemento": "MDAxMQ==",
            "nombreResumido": "Qg==",
            "obligatorio": "MQ=="
          }
        ]
      }
    ],
    "componenteSeccion": [
      {
        "codigoSeccion": "MDAwNA==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VFJJUExF",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VFJJUExFIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VFJJUExFIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwNg==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VFJJUExF",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "descripcionEtiqueta": "Q0FDSE8=",
            "dropdown": "MQ==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwNw==",
        "elemento": [
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VEVSTUlOQUwgQQ==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VEVSTUlOQUwgQg==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwOA==",
        "elemento": [
          {
            "codigoElemento": "MDAwNg==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "descripcionEtiqueta": "Q0FDSE8=",
            "dropdown": "MQ==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwOQ==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMT1RP",
            "dropdown": "MA==",
            "longitudElemento": "Ng==",
            "maximoSimbolo": "KjYg",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxMA==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwOQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAxMA==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEM=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mw==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxMg==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxNA==",
        "elemento": [
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VEVSTUlOQUwgSVpRVUlFUkRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VEVSTUlOQUwgREVSRUNITw==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxNg==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VFJJUExF",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxNw==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VEVSTUlOQUw=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxOA==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyMQ==",
        "elemento": [
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VEVSTUlOQUwgQQ==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VEVSTUlOQUwgQg==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyMw==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VEVSTUlOQUw=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwNw==",
            "descripcionEtiqueta": "Q0FDSE8=",
            "dropdown": "MQ==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyNQ==",
        "elemento": [
          {
            "codigoElemento": "MDAxMg==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAxMw==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyNg==",
        "elemento": [
          {
            "codigoElemento": "MDAxMg==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "Mzg=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyNw==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VEVSTUlOQUw=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyOQ==",
        "elemento": [
          {
            "codigoElemento": "MDAxNA==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAzMA==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VEVSTUlOQUw=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAzMg==",
        "elemento": [
          {
            "codigoElemento": "MDAwNQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwMQ==",
        "elemento": [
          {
            "codigoElemento": "MDAwNQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "Mzg=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAwNQ==",
        "elemento": [
          {
            "codigoElemento": "MDAxNg==",
            "descripcionEtiqueta": "VFJJUExF",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VFJJUExF",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwNA==",
            "descripcionEtiqueta": "U0lHTk8=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MTI=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "Mg=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxMQ==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwOQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxMw==",
        "elemento": [
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VFJJUExFIElaUVVJRVJETw==",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VFJJUExFIERFUkVDSE8=",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAxNQ==",
        "elemento": [
          {
            "codigoElemento": "MDAwOA==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIElaUVVJRVJETw==",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwOQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIERFUkVDSE8=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyMA==",
        "elemento": [
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VFJJUExFIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMg==",
            "descripcionEtiqueta": "VFJJUExFIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mw==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyMg==",
        "elemento": [
          {
            "codigoElemento": "MDAwNQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEE=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAxMQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRPIEI=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyNA==",
        "elemento": [
          {
            "codigoElemento": "MDAxMg==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "Mzg=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAyOA==",
        "elemento": [
          {
            "codigoElemento": "MDAxMg==",
            "descripcionEtiqueta": "RklHVVJB",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          },
          {
            "codigoElemento": "MDAwMQ==",
            "descripcionEtiqueta": "VEVSTUlOQUw=",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MCA=",
            "ordenJerarquia": "Mg==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAzMQ==",
        "elemento": [
          {
            "codigoElemento": "MDAxNQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      },
      {
        "codigoSeccion": "MDAzMw==",
        "elemento": [
          {
            "codigoElemento": "MDAwNQ==",
            "descripcionEtiqueta": "QU5JTUFMSVRP",
            "dropdown": "MA==",
            "longitudElemento": "Mg==",
            "maximoSimbolo": "MSA=",
            "ordenJerarquia": "MQ==",
            "permiteEscritura": "MA=="
          }
        ]
      }
    ],
    "estatusPunta": "0",
    "estatusTerminal": "1",
    "limiteJugadas": 100,
    "listsimbolo": [
      {
        "codigoElemento": "MDAwNg==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVa",
            "nombreSimbolo": "MzMgUEVa"
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MzcgREVMRklO"
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MzggQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "MzkgQlVGQUxP"
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "NDAgQ0hJVk8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0FuaW1hbGl0b3Mve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAwOQ==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MzcgREVMRklO"
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MzggQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "MzkgQlVGQUxP"
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "NDAgQ0hJVk8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0FuaW1hbGl0b3Mve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAxMA==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MzcgREVMRklO"
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MzggQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "MzkgQlVGQUxP"
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "NDAgQ0hJVk8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0FuaW1hbGl0b3Mve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAxMQ==",
        "simbolo": [
          {
            "codigoSimbolo": "MA==",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MCBERUxGSU4="
          },
          {
            "codigoSimbolo": "MDA=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MDAgQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9DaGFuY2UvQW5pbWFsZXMve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAwNA==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "QVJJ",
            "nombreSimbolo": "MDEgQVJJRVM="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VEFV",
            "nombreSimbolo": "MDIgVEFVUk8="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "R0VN",
            "nombreSimbolo": "MDMgR0VNSU5JUw=="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "Q0FO",
            "nombreSimbolo": "MDQgQ0FOQ0VS"
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "VklS",
            "nombreSimbolo": "MDYgVklSR08="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TElC",
            "nombreSimbolo": "MDcgTElCUkE="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "RVND",
            "nombreSimbolo": "MDggRVNDT1JQSU9O"
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "U0FH",
            "nombreSimbolo": "MDkgU0FHSVRBUklP"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "Q0FQ",
            "nombreSimbolo": "MTAgQ0FQUklDT1JOSU8="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "QUNV",
            "nombreSimbolo": "MTEgQUNVQVJJTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "UElT",
            "nombreSimbolo": "MTIgUElTQ0lT"
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9DaGFuY2UvU2lnbm9zL3tjb2RpZ299LnBuZyA="
      },
      {
        "codigoElemento": "MDAwNw==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "RE9CTEUtQ0FDSE8=",
            "nombreSimbolo": "MDEgRE9CTEUgQ0FDSE8="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "Q09OLUNBQ0hP",
            "nombreSimbolo": "MDIgQ09OIENBQ0hP"
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "TUVESU8tQ0FDSE8=",
            "nombreSimbolo": "MDMgTUVESU8gQ0FDSE8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0NhY2hvL3tjb2RpZ299LnBuZyA="
      },
      {
        "codigoElemento": "MDAwNQ==",
        "simbolo": [
          {
            "codigoSimbolo": "MA==",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MCBERUxGSU4="
          },
          {
            "codigoSimbolo": "MDA=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MDAgQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9DaGFuY2UvQW5pbWFsZXMve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAwOA==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "U0FQ",
            "nombreSimbolo": "MDYgU0FQTw=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "TE9S",
            "nombreSimbolo": "MDcgTE9STw=="
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0FCUg==",
            "nombreSimbolo": "MTkgQ0FCUkE="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MzcgREVMRklO"
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MzggQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "MzkgQlVGQUxP"
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "NDAgQ0hJVk8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9UcmlwbGVQb3B1bGFyL0FuaW1hbGl0b3Mve2NvZGlnb30ucG5nIA=="
      },
      {
        "codigoElemento": "MDAxMg==",
        "simbolo": [
          {
            "codigoSimbolo": "MA==",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MCBERUxGSU4="
          },
          {
            "codigoSimbolo": "MDA=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MDAgQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "UkFO",
            "nombreSimbolo": "MDYgUkFOQQ=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MDcgUEVSSUNP"
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "MTkgQ0hJVk8="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVSUg==",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9Mb3RvQUdyYW5qaXRhL0FuaW1hbGVzL3tjb2RpZ299LnBuZyA="
      },
      {
        "codigoElemento": "MDAxMw==",
        "simbolo": [
          {
            "codigoSimbolo": "MA==",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MCBERUxGSU4="
          },
          {
            "codigoSimbolo": "MDA=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MDAgQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "UkFO",
            "nombreSimbolo": "MDYgUkFOQQ=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MDcgUEVSSUNP"
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "MTkgQ0hJVk8="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVSUg==",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9Mb3RvQUdyYW5qaXRhL0FuaW1hbGVzL3tjb2RpZ299LnBuZyA="
      },
      {
        "codigoElemento": "MDAxNA==",
        "simbolo": [
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "UkFO",
            "nombreSimbolo": "MDYgUkFOQQ=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MDcgUEVSSUNP"
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "MTkgQ0hJVk8="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVSUg==",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "TUFS",
            "nombreSimbolo": "MzcgTUFSSVBPU0E="
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "Q09O",
            "nombreSimbolo": "MzggQ09ORUpP"
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "UklO",
            "nombreSimbolo": "MzkgUklOT1NFUk9OVEU="
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "SElQ",
            "nombreSimbolo": "NDAgSElQT1BPVEFNTw=="
          },
          {
            "codigoSimbolo": "NDE=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "NDEgQlVGQUxP"
          },
          {
            "codigoSimbolo": "NDI=",
            "nombreResumido": "TVVS",
            "nombreSimbolo": "NDIgTVVSQ0lFTEFHTw=="
          },
          {
            "codigoSimbolo": "NDM=",
            "nombreResumido": "S09B",
            "nombreSimbolo": "NDMgS09BTEE="
          },
          {
            "codigoSimbolo": "NDQ=",
            "nombreResumido": "UFVN",
            "nombreSimbolo": "NDQgUFVNQQ=="
          },
          {
            "codigoSimbolo": "NDU=",
            "nombreResumido": "Q0FO",
            "nombreSimbolo": "NDUgQ0FOR1VSTw=="
          },
          {
            "codigoSimbolo": "NDY=",
            "nombreResumido": "TE9C",
            "nombreSimbolo": "NDYgTE9CTw=="
          },
          {
            "codigoSimbolo": "NDc=",
            "nombreResumido": "VE9SVA==",
            "nombreSimbolo": "NDcgVE9SVFVHQQ=="
          },
          {
            "codigoSimbolo": "NDg=",
            "nombreResumido": "TU9S",
            "nombreSimbolo": "NDggTU9SU0E="
          },
          {
            "codigoSimbolo": "NDk=",
            "nombreResumido": "SElF",
            "nombreSimbolo": "NDkgSElFTkE="
          },
          {
            "codigoSimbolo": "NTA=",
            "nombreResumido": "Q09N",
            "nombreSimbolo": "NTAgQ09NQURSRUpB"
          },
          {
            "codigoSimbolo": "NTE=",
            "nombreResumido": "RFJB",
            "nombreSimbolo": "NTEgRFJBR09O"
          },
          {
            "codigoSimbolo": "NTI=",
            "nombreResumido": "Q0hB",
            "nombreSimbolo": "NTIgQ0hBQ0FM"
          },
          {
            "codigoSimbolo": "NTM=",
            "nombreResumido": "SkFH",
            "nombreSimbolo": "NTMgSkFHVUFS"
          },
          {
            "codigoSimbolo": "NTQ=",
            "nombreResumido": "Q0FNQQ==",
            "nombreSimbolo": "NTQgQ0FNQUxFT04="
          },
          {
            "codigoSimbolo": "NTU=",
            "nombreResumido": "Rk9D",
            "nombreSimbolo": "NTUgRk9DQQ=="
          },
          {
            "codigoSimbolo": "NTY=",
            "nombreResumido": "QVJN",
            "nombreSimbolo": "NTYgQVJNSU5P"
          },
          {
            "codigoSimbolo": "NTc=",
            "nombreResumido": "SkFC",
            "nombreSimbolo": "NTcgSkFCQUxJ"
          },
          {
            "codigoSimbolo": "NTg=",
            "nombreResumido": "Q0FT",
            "nombreSimbolo": "NTggQ0FTVE9S"
          },
          {
            "codigoSimbolo": "NTk=",
            "nombreResumido": "R1VB",
            "nombreSimbolo": "NTkgR1VBQ0FNQVlP"
          },
          {
            "codigoSimbolo": "NjA=",
            "nombreResumido": "VElC",
            "nombreSimbolo": "NjAgVElCVVJPTg=="
          },
          {
            "codigoSimbolo": "NjE=",
            "nombreResumido": "VFVS",
            "nombreSimbolo": "NjEgVFVSUElBTA=="
          },
          {
            "codigoSimbolo": "NjI=",
            "nombreResumido": "VFVD",
            "nombreSimbolo": "NjIgVFVDQU4="
          },
          {
            "codigoSimbolo": "NjM=",
            "nombreResumido": "Q0FD",
            "nombreSimbolo": "NjMgQ0FDSElDQU1P"
          },
          {
            "codigoSimbolo": "NjQ=",
            "nombreResumido": "SE9S",
            "nombreSimbolo": "NjQgSE9STUlHQQ=="
          },
          {
            "codigoSimbolo": "NjU=",
            "nombreResumido": "R09S",
            "nombreSimbolo": "NjUgR09SSUxB"
          },
          {
            "codigoSimbolo": "NjY=",
            "nombreResumido": "QklT",
            "nombreSimbolo": "NjYgQklTT05URQ=="
          },
          {
            "codigoSimbolo": "Njc=",
            "nombreResumido": "RkxB",
            "nombreSimbolo": "NjcgRkxBTUlOR08="
          },
          {
            "codigoSimbolo": "Njg=",
            "nombreResumido": "QVJB",
            "nombreSimbolo": "NjggQVJBTkE="
          },
          {
            "codigoSimbolo": "Njk=",
            "nombreResumido": "UEFWUg==",
            "nombreSimbolo": "NjkgUEFWTyBSRUFM"
          },
          {
            "codigoSimbolo": "NzA=",
            "nombreResumido": "Q0FORw==",
            "nombreSimbolo": "NzAgQ0FOR1JFSk8="
          },
          {
            "codigoSimbolo": "NzE=",
            "nombreResumido": "UkFC",
            "nombreSimbolo": "NzEgUkFCSVBFTEFP"
          },
          {
            "codigoSimbolo": "NzI=",
            "nombreResumido": "UEVM",
            "nombreSimbolo": "NzIgUEVMSUNBTk8="
          },
          {
            "codigoSimbolo": "NzM=",
            "nombreResumido": "Q09M",
            "nombreSimbolo": "NzMgQ09MSUJSSQ=="
          },
          {
            "codigoSimbolo": "NzQ=",
            "nombreResumido": "RVND",
            "nombreSimbolo": "NzQgRVNDQVJBQkFKTw=="
          },
          {
            "codigoSimbolo": "NzU=",
            "nombreResumido": "Q09E",
            "nombreSimbolo": "NzUgQ09ET1JOSVo="
          },
          {
            "codigoSimbolo": "NzY=",
            "nombreResumido": "TUFO",
            "nombreSimbolo": "NzYgTUFOVElT"
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9HcmFuamF6by97Y29kaWdvfS5wbmcg"
      },
      {
        "codigoElemento": "MDAxNQ==",
        "simbolo": [
          {
            "codigoSimbolo": "MA==",
            "nombreResumido": "REVM",
            "nombreSimbolo": "MCBERUxGSU4="
          },
          {
            "codigoSimbolo": "MDA=",
            "nombreResumido": "QkFM",
            "nombreSimbolo": "MDAgQkFMTEVOQQ=="
          },
          {
            "codigoSimbolo": "MDE=",
            "nombreResumido": "Q0FS",
            "nombreSimbolo": "MDEgQ0FSTkVSTw=="
          },
          {
            "codigoSimbolo": "MDI=",
            "nombreResumido": "VE9S",
            "nombreSimbolo": "MDIgVE9STw=="
          },
          {
            "codigoSimbolo": "MDM=",
            "nombreResumido": "Q0lF",
            "nombreSimbolo": "MDMgQ0lFTVBJRVM="
          },
          {
            "codigoSimbolo": "MDQ=",
            "nombreResumido": "QUxB",
            "nombreSimbolo": "MDQgQUxBQ1JBTg=="
          },
          {
            "codigoSimbolo": "MDU=",
            "nombreResumido": "TEVP",
            "nombreSimbolo": "MDUgTEVPTg=="
          },
          {
            "codigoSimbolo": "MDY=",
            "nombreResumido": "UkFO",
            "nombreSimbolo": "MDYgUkFOQQ=="
          },
          {
            "codigoSimbolo": "MDc=",
            "nombreResumido": "UEVS",
            "nombreSimbolo": "MDcgUEVSSUNP"
          },
          {
            "codigoSimbolo": "MDg=",
            "nombreResumido": "UkFU",
            "nombreSimbolo": "MDggUkFUT04="
          },
          {
            "codigoSimbolo": "MDk=",
            "nombreResumido": "QUdV",
            "nombreSimbolo": "MDkgQUdVSUxB"
          },
          {
            "codigoSimbolo": "MTA=",
            "nombreResumido": "VElH",
            "nombreSimbolo": "MTAgVElHUkU="
          },
          {
            "codigoSimbolo": "MTE=",
            "nombreResumido": "R0FU",
            "nombreSimbolo": "MTEgR0FUTw=="
          },
          {
            "codigoSimbolo": "MTI=",
            "nombreResumido": "Q0FC",
            "nombreSimbolo": "MTIgQ0FCQUxMTw=="
          },
          {
            "codigoSimbolo": "MTM=",
            "nombreResumido": "TU9O",
            "nombreSimbolo": "MTMgTU9OTw=="
          },
          {
            "codigoSimbolo": "MTQ=",
            "nombreResumido": "UEFM",
            "nombreSimbolo": "MTQgUEFMT01B"
          },
          {
            "codigoSimbolo": "MTU=",
            "nombreResumido": "Wk9S",
            "nombreSimbolo": "MTUgWk9SUk8="
          },
          {
            "codigoSimbolo": "MTY=",
            "nombreResumido": "T1NP",
            "nombreSimbolo": "MTYgT1NP"
          },
          {
            "codigoSimbolo": "MTc=",
            "nombreResumido": "UEFW",
            "nombreSimbolo": "MTcgUEFWTw=="
          },
          {
            "codigoSimbolo": "MTg=",
            "nombreResumido": "QlVS",
            "nombreSimbolo": "MTggQlVSUk8="
          },
          {
            "codigoSimbolo": "MTk=",
            "nombreResumido": "Q0hJ",
            "nombreSimbolo": "MTkgQ0hJVk8="
          },
          {
            "codigoSimbolo": "MjA=",
            "nombreResumido": "Q09D",
            "nombreSimbolo": "MjAgQ09DSElOTw=="
          },
          {
            "codigoSimbolo": "MjE=",
            "nombreResumido": "R0FMTA==",
            "nombreSimbolo": "MjEgR0FMTE8="
          },
          {
            "codigoSimbolo": "MjI=",
            "nombreResumido": "Q0FN",
            "nombreSimbolo": "MjIgQ0FNRUxMTw=="
          },
          {
            "codigoSimbolo": "MjM=",
            "nombreResumido": "Q0VC",
            "nombreSimbolo": "MjMgQ0VCUkE="
          },
          {
            "codigoSimbolo": "MjQ=",
            "nombreResumido": "SUdV",
            "nombreSimbolo": "MjQgSUdVQU5B"
          },
          {
            "codigoSimbolo": "MjU=",
            "nombreResumido": "R0FM",
            "nombreSimbolo": "MjUgR0FMTElOQQ=="
          },
          {
            "codigoSimbolo": "MjY=",
            "nombreResumido": "VkFD",
            "nombreSimbolo": "MjYgVkFDQQ=="
          },
          {
            "codigoSimbolo": "Mjc=",
            "nombreResumido": "UEVSUg==",
            "nombreSimbolo": "MjcgUEVSUk8="
          },
          {
            "codigoSimbolo": "Mjg=",
            "nombreResumido": "WkFN",
            "nombreSimbolo": "MjggWkFNVVJP"
          },
          {
            "codigoSimbolo": "Mjk=",
            "nombreResumido": "RUxF",
            "nombreSimbolo": "MjkgRUxFRkFOVEU="
          },
          {
            "codigoSimbolo": "MzA=",
            "nombreResumido": "Q0FJ",
            "nombreSimbolo": "MzAgQ0FJTUFO"
          },
          {
            "codigoSimbolo": "MzE=",
            "nombreResumido": "TEFQ",
            "nombreSimbolo": "MzEgTEFQQQ=="
          },
          {
            "codigoSimbolo": "MzI=",
            "nombreResumido": "QVJE",
            "nombreSimbolo": "MzIgQVJESUxMQQ=="
          },
          {
            "codigoSimbolo": "MzM=",
            "nombreResumido": "UEVT",
            "nombreSimbolo": "MzMgUEVTQ0FETw=="
          },
          {
            "codigoSimbolo": "MzQ=",
            "nombreResumido": "VkVO",
            "nombreSimbolo": "MzQgVkVOQURP"
          },
          {
            "codigoSimbolo": "MzU=",
            "nombreResumido": "SklS",
            "nombreSimbolo": "MzUgSklSQUZB"
          },
          {
            "codigoSimbolo": "MzY=",
            "nombreResumido": "Q1VM",
            "nombreSimbolo": "MzYgQ1VMRUJSQQ=="
          },
          {
            "codigoSimbolo": "Mzc=",
            "nombreResumido": "VE9SVA==",
            "nombreSimbolo": "MzcgVE9SVFVHQQ=="
          },
          {
            "codigoSimbolo": "Mzg=",
            "nombreResumido": "QlVG",
            "nombreSimbolo": "MzggQlVGQUxP"
          },
          {
            "codigoSimbolo": "Mzk=",
            "nombreResumido": "TEVD",
            "nombreSimbolo": "MzkgTEVDSFVaQQ=="
          },
          {
            "codigoSimbolo": "NDA=",
            "nombreResumido": "QVZJ",
            "nombreSimbolo": "NDAgQVZJU1BB"
          },
          {
            "codigoSimbolo": "NDE=",
            "nombreResumido": "Q0FO",
            "nombreSimbolo": "NDEgQ0FOR1VSTw=="
          },
          {
            "codigoSimbolo": "NDI=",
            "nombreResumido": "VFVD",
            "nombreSimbolo": "NDIgVFVDQU4="
          },
          {
            "codigoSimbolo": "NDM=",
            "nombreResumido": "TUFS",
            "nombreSimbolo": "NDMgTUFSSVBPU0E="
          },
          {
            "codigoSimbolo": "NDQ=",
            "nombreResumido": "Q0hJRw==",
            "nombreSimbolo": "NDQgQ0hJR1VJUkU="
          },
          {
            "codigoSimbolo": "NDU=",
            "nombreResumido": "R0FS",
            "nombreSimbolo": "NDUgR0FSWkE="
          },
          {
            "codigoSimbolo": "NDY=",
            "nombreResumido": "UFVN",
            "nombreSimbolo": "NDYgUFVNQQ=="
          },
          {
            "codigoSimbolo": "NDc=",
            "nombreResumido": "UEFWUg==",
            "nombreSimbolo": "NDcgUEFWT1JFQUw="
          },
          {
            "codigoSimbolo": "NDg=",
            "nombreResumido": "UFVF",
            "nombreSimbolo": "NDggUFVFUkNPRVNQSU4="
          },
          {
            "codigoSimbolo": "NDk=",
            "nombreResumido": "UEVSRQ==",
            "nombreSimbolo": "NDkgUEVSRVpB"
          },
          {
            "codigoSimbolo": "NTA=",
            "nombreResumido": "Q0FOQQ==",
            "nombreSimbolo": "NTAgQ0FOQVJJTw=="
          },
          {
            "codigoSimbolo": "NTE=",
            "nombreResumido": "UEVM",
            "nombreSimbolo": "NTEgUEVMSUNBTk8="
          },
          {
            "codigoSimbolo": "NTI=",
            "nombreResumido": "UFVM",
            "nombreSimbolo": "NTIgUFVMUE8="
          },
          {
            "codigoSimbolo": "NTM=",
            "nombreResumido": "Q0FSQQ==",
            "nombreSimbolo": "NTMgQ0FSQUNPTA=="
          },
          {
            "codigoSimbolo": "NTQ=",
            "nombreResumido": "R1JJ",
            "nombreSimbolo": "NTQgR1JJTExP"
          },
          {
            "codigoSimbolo": "NTU=",
            "nombreResumido": "T1NPSA==",
            "nombreSimbolo": "NTUgT1NPIEhPUk1JR1VFUk8="
          },
          {
            "codigoSimbolo": "NTY=",
            "nombreResumido": "VElC",
            "nombreSimbolo": "NTYgVElCVVJPTg=="
          },
          {
            "codigoSimbolo": "NTc=",
            "nombreResumido": "UEFU",
            "nombreSimbolo": "NTcgUEFUTw=="
          },
          {
            "codigoSimbolo": "NTg=",
            "nombreResumido": "SE9S",
            "nombreSimbolo": "NTggSE9STUlHQQ=="
          },
          {
            "codigoSimbolo": "NTk=",
            "nombreResumido": "UEFO",
            "nombreSimbolo": "NTkgUEFOVEVSQQ=="
          },
          {
            "codigoSimbolo": "NjA=",
            "nombreResumido": "Q0FNQQ==",
            "nombreSimbolo": "NjAgQ0FNQUxFT04="
          },
          {
            "codigoSimbolo": "NjE=",
            "nombreResumido": "UEFORA==",
            "nombreSimbolo": "NjEgUEFOREE="
          },
          {
            "codigoSimbolo": "NjI=",
            "nombreResumido": "Q0FD",
            "nombreSimbolo": "NjIgQ0FDSElDQU1P"
          },
          {
            "codigoSimbolo": "NjM=",
            "nombreResumido": "Q0FORw==",
            "nombreSimbolo": "NjMgQ0FOR1JFSk8="
          },
          {
            "codigoSimbolo": "NjQ=",
            "nombreResumido": "R0FW",
            "nombreSimbolo": "NjQgR0FWSUxBTg=="
          },
          {
            "codigoSimbolo": "NjU=",
            "nombreResumido": "QVJB",
            "nombreSimbolo": "NjUgQVJBTkE="
          },
          {
            "codigoSimbolo": "NjY=",
            "nombreResumido": "TE9C",
            "nombreSimbolo": "NjYgTE9CTw=="
          },
          {
            "codigoSimbolo": "Njc=",
            "nombreResumido": "QVZF",
            "nombreSimbolo": "NjcgQVZFU1RSVVo="
          },
          {
            "codigoSimbolo": "Njg=",
            "nombreResumido": "SkFH",
            "nombreSimbolo": "NjggSkFHVUFS"
          },
          {
            "codigoSimbolo": "Njk=",
            "nombreResumido": "Q09O",
            "nombreSimbolo": "NjkgQ09ORUpP"
          },
          {
            "codigoSimbolo": "NzA=",
            "nombreResumido": "QklT",
            "nombreSimbolo": "NzAgQklTT05URQ=="
          },
          {
            "codigoSimbolo": "NzE=",
            "nombreResumido": "R1VBQw==",
            "nombreSimbolo": "NzEgR1VBQ0FNQVlB"
          },
          {
            "codigoSimbolo": "NzI=",
            "nombreResumido": "R09S",
            "nombreSimbolo": "NzIgR09SSUxB"
          },
          {
            "codigoSimbolo": "NzM=",
            "nombreResumido": "SElQ",
            "nombreSimbolo": "NzMgSElQT1BPVEFNTw=="
          },
          {
            "codigoSimbolo": "NzQ=",
            "nombreResumido": "VFVS",
            "nombreSimbolo": "NzQgVFVSUElBTA=="
          },
          {
            "codigoSimbolo": "NzU=",
            "nombreResumido": "R1VB",
            "nombreSimbolo": "NzUgR1VBQ0hBUk8="
          }
        ],
        "urlSimbolo": "aHR0cDovLzE5Mi4xNjguMjAwLjIyNjo5NDAyL2ltZy9HdWFjaGFyby97Y29kaWdvfS5wbmcg"
      }
    ],
    "loterias": [
      {
        "codigoLoteria": "TDA1",
        "nombreLoteria": "TE9URVJJQSBERSBDT0pFREVT",
        "nombreResumido": "TE9ULiBDT0pFREVT"
      },
      {
        "codigoLoteria": "TDAy",
        "nombreLoteria": "TE9URVJJQSBERUwgWlVMSUE=",
        "nombreResumido": "TE9ULiBaVUxJQQ=="
      },
      {
        "codigoLoteria": "TDEw",
        "nombreLoteria": "TE9URVJJQSBERSBNQVJHQVJJVEE=",
        "nombreResumido": "TE8uTUFSR0FSSVRB"
      },
      {
        "codigoLoteria": "TDAz",
        "nombreLoteria": "TE9URVJJQSBERSBPUklFTlRF",
        "nombreResumido": "TE9ULiBPUklFTlRF"
      },
      {
        "codigoLoteria": "TDA0",
        "nombreLoteria": "TE9URVJJQSBERSBDQVJBQ0FT",
        "nombreResumido": "TE9ULiBDQVJBQ0FT"
      },
      {
        "codigoLoteria": "TDAx",
        "nombreLoteria": "TE9URVJJQSBERUwgVEFDSElSQQ==",
        "nombreResumido": "TE9ULiBUQUNISVJB"
      }
    ],
    "modalidades": [
      {
        "codigoModalidad": "MDE=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIEZJSk8=",
        "nombreResumido": "VFJJUExFIEZJSk8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MDQ=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIEEgWSBC",
        "nombreResumido": "VFJJUExFIEEgWSBC",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MzM=",
        "maximoPermuta": 0,
        "nombreModalidad": "R1VBQ0hBUk8gQUNUSVZP",
        "nombreResumido": "R1VBQ0hBUk8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MjI=",
        "maximoPermuta": 5,
        "nombreModalidad": "QU5JTUFMSVRP",
        "nombreResumido": "QU5JLiBQT1BVTEFS",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MTk=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIEFOSU1BTElUTw==",
        "nombreResumido": "VFJJUCBBTklNQUxJVE8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "Mjg=",
        "maximoPermuta": 0,
        "nombreModalidad": "QU5JTUFMSVRP",
        "nombreResumido": "QU5JTUFMSVRP",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MjM=",
        "maximoPermuta": 5,
        "nombreModalidad": "RE9CTEUgQU5JTUFMSVRP",
        "nombreResumido": "RE9CIEFOSU1BTElUTw==",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MjU=",
        "maximoPermuta": 5,
        "nombreModalidad": "QU5JTUFMSVRPIEI=",
        "nombreResumido": "QU5JTUFMSVRPIEI=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "Mjc=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIEI=",
        "nombreResumido": "VFJJUExFIEI=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MTQ=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgQQ==",
        "nombreResumido": "VEVSTUlOQUwgQQ==",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MDc=",
        "maximoPermuta": 0,
        "nombreModalidad": "QU5JTUFMSVRP",
        "nombreResumido": "QU5JTUFMSVRP",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MDI=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUw=",
        "nombreResumido": "VEVSTUlOQUw=",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MzE=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgICsgRklHVVJB",
        "nombreResumido": "VEVSICsgRklHVVJB",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MTM=",
        "maximoPermuta": 0,
        "nombreModalidad": "QU5JTUFMSVRPICsgQ0FDSE8=",
        "nombreResumido": "QU5JLiArIENBQ0hP",
        "ordenSeleccion": "  0,0"
      },
      {
        "codigoModalidad": "MjE=",
        "maximoPermuta": 5,
        "nombreModalidad": "U0VOQ0lMTE8gQU5JTUFMSVRP",
        "nombreResumido": "U0VOQyBBTklNQUxJVE8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MjY=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIEE=",
        "nombreResumido": "VFJJUExFIEE=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MDU=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIFkgU0lHTk8=",
        "nombreResumido": "VFJJUExFIFkgU0lHTk8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MDk=",
        "maximoPermuta": 5,
        "nombreModalidad": "VFJJUExFIFkgQ0FDSE8=",
        "nombreResumido": "VFJJUExFICsgQ0FD",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MTU=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgQg==",
        "nombreResumido": "VEVSTUlOQUwgQg==",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "Mjk=",
        "maximoPermuta": 0,
        "nombreModalidad": "RFVQTEVUQQ==",
        "nombreResumido": "RFVQTEVUQQ==",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MDY=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgWSBTSUdOTw==",
        "nombreResumido": "VEVSTUlOQUwgWSBTSUdOTw==",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MTE=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgKyBDQUNITw==",
        "nombreResumido": "VEVSICArIENBQ0hP",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MzI=",
        "maximoPermuta": 0,
        "nombreModalidad": "RUwgR1JBTkpBWk8=",
        "nombreResumido": "R1JBTkpBWk8=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MTg=",
        "maximoPermuta": 5,
        "nombreModalidad": "RE9CTEUgQU5JTUFMSVRP",
        "nombreResumido": "RE9CIEFOSU1BTElUTw==",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MTA=",
        "maximoPermuta": 5,
        "nombreModalidad": "VEVSTUlOQUwgQSArIFRFUk1JTkFMIEI=",
        "nombreResumido": "VEVSIEEgKyBURVIgQg==",
        "ordenSeleccion": " -0,2"
      },
      {
        "codigoModalidad": "MjQ=",
        "maximoPermuta": 5,
        "nombreModalidad": "QU5JTUFMSVRPIEE=",
        "nombreResumido": "QU5JTUFMSVRPIEE=",
        "ordenSeleccion": "  0,3"
      },
      {
        "codigoModalidad": "MjA=",
        "maximoPermuta": 5,
        "nombreModalidad": "TE9UTyBBTklNQUxJVE8=",
        "nombreResumido": "TE9UTyBBTklNQUxJVE8=",
        "ordenSeleccion": "  0,3"
      }
    ],
    "moneda": [
      {
        "codigoMoneda": "MDI=",
        "monMaximoTic": "1000",
        "monMinimoTic": "1",
        "monedaBase": "MQ==",
        "nombreMoneda": "DÓLAR",
        "simboloMoneda": "JA==",
        "valorMoneda": "MzYsMDQ="
      },
      {
        "codigoMoneda": "MDE=",
        "monMaximoTic": "2000",
        "monMinimoTic": "1",
        "monedaBase": "MQ==",
        "nombreMoneda": "BOLÍVAR",
        "simboloMoneda": "QnMu",
        "valorMoneda": "MzYsMDQ="
      }
    ],
    "productos": [
      {
        "codigoProducto": "UDAwMw==",
        "combinaSeccion": "0",
        "nombreProducto": "Q0hBTkNF",
        "nombreResumido": "Q0hBTkNF",
        "ordenProducto": "MQ==",
        "seccion": []
      },
      {
        "codigoProducto": "UDAzNQ==",
        "combinaSeccion": "0",
        "nombreProducto": "Q0hBTkNFIEFOSU1BTElUTw==",
        "nombreResumido": "Qy4gQU5JTUFMSVRP",
        "ordenProducto": "Mg==",
        "seccion": []
      },
      {
        "codigoProducto": "UDAzNg==",
        "combinaSeccion": "1",
        "nombreProducto": "VFJJUExFIFBPUFVMQVI=",
        "nombreResumido": "VC5QT1BVTEFS",
        "ordenProducto": "Mw==",
        "seccion": [
          "MDAwNg==",
          "MDAwNw==",
          "MDAwOA==",
          "MDAyMw==",
          "MDAxOQ=="
        ]
      },
      {
        "codigoProducto": "UDA0MA==",
        "combinaSeccion": "0",
        "nombreProducto": "TEEgR1JBTkpJVEE=",
        "nombreResumido": "R1JBTkpJVEE=",
        "ordenProducto": "NA==",
        "seccion": []
      },
      {
        "codigoProducto": "UDA0Mg==",
        "combinaSeccion": "0",
        "nombreProducto": "RUwgR1JBTkpBWk8gR1JBTkpBIE1JTExPTkFSSUE=",
        "nombreResumido": "R1JBTkpBWk8=",
        "ordenProducto": "NQ==",
        "seccion": []
      },
      {
        "codigoProducto": "UDA0Mw==",
        "combinaSeccion": "0",
        "nombreProducto": "RUwgVEVSTUlOQUxJVE8=",
        "nombreResumido": "VEVSTUlOQUxJVE8=",
        "ordenProducto": "NQ==",
        "seccion": []
      },
      {
        "codigoProducto": "UDA0NA==",
        "combinaSeccion": "0",
        "nombreProducto": "R1VBQ0hBUk8gQUNUSVZP",
        "nombreResumido": "R1VBQ0hBUk8=",
        "ordenProducto": "NQ==",
        "seccion": []
      },
      {
        "codigoProducto": "UDA0MQ==",
        "combinaSeccion": "0",
        "nombreProducto": "TE9UTyBBQ1RJVk8=",
        "nombreResumido": "TE9UTyBBQ1RJVk8=",
        "ordenProducto": "NQ==",
        "seccion": []
      }
    ],
    "secciones": [
      {
        "codigoSeccion": "MDAxMQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "RC4gQU5JTUFMSVRP",
        "nombreSeccion": "RE9CTEUgQU5JTUFMSVRPIE9SSUVOVEU="
      },
      {
        "codigoSeccion": "MDAwOQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMT1RP",
        "nombreSeccion": "QU5JTUFMT1RPIE9SSUVOVEU="
      },
      {
        "codigoSeccion": "MDAxMA==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "VC4gQU5JTUFMSVRP",
        "nombreSeccion": "VFJJUExFIEFOSU1BTElUTyBPUklFTlRF"
      },
      {
        "codigoSeccion": "MDAxMw==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VC4gTU9ST0NITw==",
        "nombreSeccion": "VFJJUExFIE1PUk9DSE8="
      },
      {
        "codigoSeccion": "MDAxNg==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "U09MTyBUUklQTEU=",
        "nombreSeccion": "TU9ST0NITyBTT0xPIFRSSVBMRQ=="
      },
      {
        "codigoSeccion": "MDAxNw==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "U09MTyBURVJNSU5BTA==",
        "nombreSeccion": "TU9ST0NITyBTT0xPIFRFUk1JTkFM"
      },
      {
        "codigoSeccion": "MDAwMQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "Q0hBTkNFIEFOSU1BTElUTw=="
      },
      {
        "codigoSeccion": "MDAwNQ==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "Q0hBTkNFIEFTVFJBTA==",
        "nombreSeccion": "Q0hBTkNFIEFTVFJBTA=="
      },
      {
        "codigoSeccion": "MDAwNw==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VEVSTUlOQUwoQStCKQ==",
        "nombreSeccion": "VEVSTUlOQUwoQSArIEIpIFBPUFVMQVI="
      },
      {
        "codigoSeccion": "MDAyNQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "RFVQTEVUQQ==",
        "nombreSeccion": "RFVQTEVUQQ=="
      },
      {
        "codigoSeccion": "MDAyNA==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "TE9UTyBBTklNQUxJVE8="
      },
      {
        "codigoSeccion": "MDAyNw==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VEVSTUlOQUw=",
        "nombreSeccion": "VEVSTUlOQUwgR1JBTkpJVEE="
      },
      {
        "codigoSeccion": "MDAyNg==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "RklHVVJBUyBHUkFOSklUQQ=="
      },
      {
        "codigoSeccion": "MDAzMg==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "TE9UVE8gUkVZ"
      },
      {
        "codigoSeccion": "MDAzMA==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VEVSTUlOQUxJVE8=",
        "nombreSeccion": "RUwgVEVSTUlOQUxJVE8="
      },
      {
        "codigoSeccion": "MDAyMw==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VEVSTUlOQUw=",
        "nombreSeccion": "VEVSTUlOQUwgUE9QVUxBUg=="
      },
      {
        "codigoSeccion": "MDAwOA==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "QU5JTUFMSVRPIFBPUFVMQVI="
      },
      {
        "codigoSeccion": "MDAxMg==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "Uy4gQU5JTUFMSVRP",
        "nombreSeccion": "U0VOQ0lMTE8gQU5JTUFMSVRPIE9SSUVOVEU="
      },
      {
        "codigoSeccion": "MDAxNA==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VEVSTS4gTU9ST0NITw==",
        "nombreSeccion": "VEVSTUlOQUwgTU9ST0NITw=="
      },
      {
        "codigoSeccion": "MDAxNQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JLiBNT1JPQ0hP",
        "nombreSeccion": "QU5JTUFMSVRPIE1PUk9DSE8="
      },
      {
        "codigoSeccion": "MDAxOA==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "U09MTyBBTklNQUxJVE8=",
        "nombreSeccion": "TU9ST0NITyBTT0xPIEFOSU1BTElUTw=="
      },
      {
        "codigoSeccion": "MDAwNA==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "QVlC",
        "nombreSeccion": "Q0hBTkNFIEFZQg=="
      },
      {
        "codigoSeccion": "MDAwNg==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "VFJJUExF",
        "nombreSeccion": "VFJJUExFIFBPUFVMQVI="
      },
      {
        "codigoSeccion": "MDAyOA==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "RVNQRUNJQUw=",
        "nombreSeccion": "R1JBTkpJVEEgRVNQRUNJQUw="
      },
      {
        "codigoSeccion": "MDAzMw==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "U0VMVkEgUExVUw=="
      },
      {
        "codigoSeccion": "MDAyOQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "R1JBTkpBWk8=",
        "nombreSeccion": "RUwgR1JBTkpBWk8="
      },
      {
        "codigoSeccion": "MDAzMQ==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "QU5JTUFMSVRP",
        "nombreSeccion": "R1VBQ0hBUk8gQUNUSVZP"
      },
      {
        "codigoSeccion": "MDAyMA==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "RC4gVFJJUExF",
        "nombreSeccion": "RE9CTEUgVFJJUExFIENBUkFDQVMg"
      },
      {
        "codigoSeccion": "MDAyMQ==",
        "codigoTipoSeccion": "MDE=",
        "nombreResumido": "RC4gVEVSTUlOQUw=",
        "nombreSeccion": "RE9CTEUgVEVSTUlOQUwgQ0FSQUNBUyA="
      },
      {
        "codigoSeccion": "MDAyMg==",
        "codigoTipoSeccion": "MDI=",
        "nombreResumido": "RC4gQU5JTUFM",
        "nombreSeccion": "Q0FSQUNBUyBET0JMRSBBTklNQUw="
      }
    ],
    "sorteos": [
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNQ==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDc=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE ANIMALITO",
        "numeroSorteo": "MDQwMQ==",
        "seccion": [
          "MDAwMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDk=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR + CACHO",
        "numeroSorteo": "MTA4MQ==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR",
        "numeroSorteo": "MTA4MA==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTA=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "POPULAR TERMINAL(A+B)",
        "numeroSorteo": "MTM4Mw==",
        "seccion": [
          "MDAwNw=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MjI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO",
        "numeroSorteo": "MDE4MQ==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO + CACHO",
        "numeroSorteo": "MDk5MA==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL + CACHO",
        "numeroSorteo": "MTIxNA==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL",
        "numeroSorteo": "MTIxMw==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "FIGURAS GRANJITA",
        "numeroSorteo": "MTQzNw==",
        "seccion": [
          "MDAyNg=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:05 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TERMINAL GRANJITA",
        "numeroSorteo": "MTUxNA==",
        "seccion": [
          "MDAyNw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0MQ==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "LOTO ACTIVO ANIMALITO",
        "numeroSorteo": "MTY2OA==",
        "seccion": [
          "MDAyNA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAz",
        "codigoProducto": "UDA0NA==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708376100,
        "hora": "05:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GUACHARO ACTIVO",
        "numeroSorteo": "MTgxNQ==",
        "seccion": [
          "MDAzMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mw==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708377000,
        "hora": "05:15 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL TERMINALITO",
        "numeroSorteo": "MTc0NA==",
        "seccion": [
          "MDAzMA=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mg==",
        "codigoTurno": "MDI=",
        "fechaCierre": 1708377900,
        "hora": "05:30 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GRANJAZO",
        "numeroSorteo": "MTU4OQ==",
        "seccion": [
          "MDAyOQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNQ==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDc=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE ANIMALITO",
        "numeroSorteo": "MDA1OQ==",
        "seccion": [
          "MDAwMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDk=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR + CACHO",
        "numeroSorteo": "MTEwNQ==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR",
        "numeroSorteo": "MTEwNA==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTA=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "POPULAR TERMINAL(A+B)",
        "numeroSorteo": "MTM4NQ==",
        "seccion": [
          "MDAwNw=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MjI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO",
        "numeroSorteo": "MDE4Nw==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO + CACHO",
        "numeroSorteo": "MDk4OQ==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL + CACHO",
        "numeroSorteo": "MTIzOA==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL",
        "numeroSorteo": "MTIzNw==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "FIGURAS GRANJITA",
        "numeroSorteo": "MTQzOA==",
        "seccion": [
          "MDAyNg=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:05 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TERMINAL GRANJITA",
        "numeroSorteo": "MTUxNQ==",
        "seccion": [
          "MDAyNw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0MQ==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "LOTO ACTIVO ANIMALITO",
        "numeroSorteo": "MTY2OQ==",
        "seccion": [
          "MDAyNA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAz",
        "codigoProducto": "UDA0NA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708379700,
        "hora": "06:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GUACHARO ACTIVO",
        "numeroSorteo": "MTgxNg==",
        "seccion": [
          "MDAzMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708380600,
        "hora": "06:15 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL TERMINALITO",
        "numeroSorteo": "MTc0NQ==",
        "seccion": [
          "MDAzMA=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708381500,
        "hora": "06:30 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GRANJAZO",
        "numeroSorteo": "MTU5MA==",
        "seccion": [
          "MDAyOQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNQ==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDc=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE ANIMALITO",
        "numeroSorteo": "MDA4Nw==",
        "seccion": [
          "MDAwMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR",
        "numeroSorteo": "MTE1Mg==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDk=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR + CACHO",
        "numeroSorteo": "MTE1Mw==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTA=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "POPULAR TERMINAL(A+B)",
        "numeroSorteo": "MTM4OA==",
        "seccion": [
          "MDAwNw=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MjI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO",
        "numeroSorteo": "MDE5Mw==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO + CACHO",
        "numeroSorteo": "MDk5OQ==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL",
        "numeroSorteo": "MTI5Nw==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL + CACHO",
        "numeroSorteo": "MTI5OA==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDAwMw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDQ=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 3,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "CHANCE A Y B",
        "numeroSorteo": "MDAwMQ==",
        "seccion": [
          "MDAwNA=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDAwMw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 3,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          },
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE TRIPLE A",
        "numeroSorteo": "MDg4OQ==",
        "seccion": [
          "MDAwNA=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDAwMw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 3,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          },
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE TRIPLE B",
        "numeroSorteo": "MDg5MA==",
        "seccion": [
          "MDAwNA=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDAwMw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          },
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE TRIPLE C",
        "numeroSorteo": "MDAxNQ==",
        "seccion": [
          "MDAwNQ=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDAwMw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDU=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          },
          {
            "codigoModalidad": "MDY=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "CHANCE ASTRAL",
        "numeroSorteo": "MDg5MQ==",
        "seccion": [
          "MDAwNQ=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "FIGURAS GRANJITA",
        "numeroSorteo": "MTQzOQ==",
        "seccion": [
          "MDAyNg=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDEw",
        "codigoProducto": "UDA0MA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:05 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TERMINAL GRANJITA",
        "numeroSorteo": "MTUxNg==",
        "seccion": [
          "MDAyNw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0MQ==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "Mjg=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "LOTO ACTIVO ANIMALITO",
        "numeroSorteo": "MTY3MA==",
        "seccion": [
          "MDAyNA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAz",
        "codigoProducto": "UDA0NA==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708383300,
        "hora": "07:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GUACHARO ACTIVO",
        "numeroSorteo": "MTgxNw==",
        "seccion": [
          "MDAzMQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mw==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708384200,
        "hora": "07:15 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL TERMINALITO",
        "numeroSorteo": "MTc0Ng==",
        "seccion": [
          "MDAzMA=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708385100,
        "hora": "07:30 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GRANJAZO",
        "numeroSorteo": "MTU5MQ==",
        "seccion": [
          "MDAyOQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR",
        "numeroSorteo": "MDk2MQ==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDk=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR + CACHO",
        "numeroSorteo": "MDk4OA==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTA=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "POPULAR TERMINAL(A+B)",
        "numeroSorteo": "MDk1NQ==",
        "seccion": [
          "MDAwNw=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO + CACHO",
        "numeroSorteo": "MDk5Mw==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MjI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO",
        "numeroSorteo": "MDE5OQ==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL + CACHO",
        "numeroSorteo": "MTE3OA==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708386900,
        "hora": "08:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL",
        "numeroSorteo": "MDk0OQ==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDA1",
        "codigoProducto": "UDA0Mg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708388700,
        "hora": "08:30 PM",
        "modalidad": [
          {
            "codigoModalidad": "MzI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "EL GRANJAZO",
        "numeroSorteo": "MTU5Mg==",
        "seccion": [
          "MDAyOQ=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR",
        "numeroSorteo": "MDIzNQ==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDk=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "TRIPLE POPULAR + CACHO",
        "numeroSorteo": "MDc5NQ==",
        "seccion": [
          "MDAwNg=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTA=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDI="
        ],
        "nombreSorteo": "POPULAR TERMINAL(A+B)",
        "numeroSorteo": "MDIzNg==",
        "seccion": [
          "MDAwNw=="
        ],
        "tipoSorteo": "MDM="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MjI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 5,
                "montoMultiplo": 0.1
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO",
        "numeroSorteo": "MDIwNQ==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTM=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR ANIMALITO + CACHO",
        "numeroSorteo": "MDk5Mg==",
        "seccion": [
          "MDAwOA=="
        ],
        "tipoSorteo": "MDI="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MDI=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL",
        "numeroSorteo": "MDg3OQ==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      },
      {
        "codigoLoteria": "TDAx",
        "codigoProducto": "UDAzNg==",
        "codigoTurno": "MDM=",
        "fechaCierre": 1708390500,
        "hora": "09:00 PM",
        "modalidad": [
          {
            "codigoModalidad": "MTE=",
            "monto": [
              {
                "codigoMoneda": "MDE=",
                "montoMinimo": 0.1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDI=",
                "montoMinimo": 1,
                "montoMultiplo": 0.01
              },
              {
                "codigoMoneda": "MDM=",
                "montoMinimo": 100,
                "montoMultiplo": 1
              }
            ]
          }
        ],
        "moneda": [
          "MDE=",
          "MDI=",
          "MDM="
        ],
        "nombreSorteo": "POPULAR  TERMINAL + CACHO",
        "numeroSorteo": "MTE3Ng==",
        "seccion": [
          "MDAyMw=="
        ],
        "tipoSorteo": "MDE="
      }
    ],
    "sorteosFrecuentes": [
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDAwMQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDAxNQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDA4Nw=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDE5Mw=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDg4OQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDg5MA=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDg5MQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MDk5OQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTE1Mg=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTE1Mw=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTI5Nw=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTI5OA=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTMzNA=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTM4OA=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTQzOQ=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTY3MA=="
      },
      {
        "codigoProducto": "UDAwMw==",
        "numeroSorteo": "MTgxNw=="
      }
    ],
    "tipoFuncion": [
      {
        "codigoTipoFuncion": "MQ==",
        "nombreTipoFuncion": "SERIE"
      },
      {
        "codigoTipoFuncion": "Mg==",
        "nombreTipoFuncion": "PERMUTA"
      },
      {
        "codigoTipoFuncion": "Mw==",
        "nombreTipoFuncion": "CORRIDA"
      }
    ],
    "tipoSorteo": [
      {
        "codigo": "MDE=",
        "descripcion": "TRIPLE"
      },
      {
        "codigo": "MDI=",
        "descripcion": "FIGURA"
      },
      {
        "codigo": "MDM=",
        "descripcion": "EXÓTICA"
      }
    ],
    "turno": [
      {
        "codigoTurno": "MDM=",
        "nombreTurno": "Noche"
      },
      {
        "codigoTurno": "MDI=",
        "nombreTurno": "Tarde"
      }
    ]
};

const wch = +'1';
const response = (sales) => JSON.stringify({
    "datos": wch ? resDym(sales) : resStat,
    "mensaje": {
        "code": "000",
        "description": "Respuesta Exitosa."
    }
});

export const resLoto = (sales = true) => JSON.parse(response(sales));