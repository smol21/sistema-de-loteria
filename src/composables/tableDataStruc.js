import { ref } from 'vue'

export function tableDataStruc (data) {
    const inTableData = data.map(attrs => ({
          name: attrs.name, // label de la columna en la tabla
          attr: attrs.attr, // atributo del cual obtener la info en el objeto
          cmp: attrs.cmp || false, // booleano para determinar si mostrar data en componente
          width: attrs.width || null, // ancho de la columna
          str: attrs.str || null, // string extra concatenado al valor del atributo del objeto
          slct: attrs.slct || false, // booleano para determinar si buscar valor a mostrar en la tabla de grupo de opciones (tipo select)
          slctExtra: attrs.slctExtra || '', // string para determinar si mostrar un texto extra al valor retornado por el select (slct)
          attrFunc: attrs.attrFunc || false, // atributo del cual obtener la info en el objeto, pero es determinado por una funcion
          sort: attrs.sort || false, // booleano para determinar si se puede ordenar en la tabla
          dir: attrs.dir || undefined, // determina si se aplica directiva y cual se aplica
          strATOB: attrs.strATOB || undefined, // determina si se aplica window.atob
          fromParent: attrs.fromParent || undefined, // determina si buscar un valor del componente padre
          inputMonto: attrs.inputMonto || false, // determina si puede mostrar input para edicion de monto
          inputable: attrs.inputable || '', // determina si muestra input para tipeado
        }))

    return inTableData;
}

export function setupData (num = 1) {
    const data = Array.from(Array(num), (_,i) => !i ? '' : (i + 1)).reduce((attrs, n) => ({
        ...attrs,
        ['canDelete'+n]: ref(''), //MOMENTANEO
        ['currentPage'+n]: ref(1), // PAGINA ACTUAL, PARA LA NAVEGACION DE LA TABLA
        ['filters'+n]: ref({}),
        ['pageSize'+n]: ref(100), // LIMITE DE ITEMS POR PAGINA EN TABLA
        ['selectedOnesFromTable'+n]: ref([]), // LISTA DE SELECCIONADOS EN LA TABLA (en caso que este activa la seleccion en tabla)
        ['setOnModify'+n]: ref(false),
        ['showPagination'+n]: ref(false), // MUESTRA PAGINACION EN TABLA
        ['tableData'+n]: ref([]),
        ['tableDataLoading'+n]: ref(false),
        ['tableState'+n]: ref(null), // ESTADO ACTUAL DE LA TABLA
        ['tRef'+n]: ref(''),
        ['tableMultidelete'+n]: ref(false),
    }), { tablesCount: +num });
    return data
}

export function optionsColWidthRemStup (colWidth, tableCols, num) {
    return {
        width: num ? `calc(${colWidth}% - ${num / tableCols}rem)` : `${colWidth}%`
    }   
}