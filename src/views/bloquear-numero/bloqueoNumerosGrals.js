import { Sorteo } from "@/_helpers/classes";
import { caseWrong, fetchRequest } from "@/_helpers/fetchToolkit";
import { depoched, hasStar, noStar, nowEpoched, numberAndComma, objOrNotAtobrToDash, objReduce } from "@/_helpers/formatters";
import { LABELS as labels } from "@/_helpers/labels";

async function fetchCombinaciones (initial = false) {
    const self = this;
    if (!initial) this.btnLoading = true;
    const resFull = !initial ? this.camposValidatorFull() : {};
    if (!resFull) {
      if (!initial) this.btnLoading = false;
      return;
    }
    const {apuestas = {}, sorteosYaValidados = []} = resFull;
    const sorteosYaValidadosObj = sorteosYaValidados.reduce((fullObj, srt_) => ({...fullObj, [srt_.id]: srt_}), {});
    const jsnK = this.forms[!initial ? (this.mode || 'n') : 'n'].jsonKey;
    const funcion = +jsnK > 0 ? window.btoa(jsnK) : 'MA==';
    // const cadenaSorteo = objReduce(sorteosYaValidadosObj, (all, srtId) => (all ? (all + ';') : '') + srtId, '');
    const cadenaSorteo = initial ? '0' : objReduce(sorteosYaValidadosObj, (all, srtId) => all + srtId + ';', '');
    const [cadenaApuesta, cadenaSimbolo] = initial ? [null, null] : 
        objReduce(apuestas, (cadenas, el) => {
            const hsStr = hasStar(el);
            // cadenas[0] += (cadenas[0] && !hsStr ? ';' : '') + (!hsStr ? (el+'-'+apuestas[el]) : '');
            cadenas[0] += !hsStr ? ((el+'-'+apuestas[el]) + ';') : '';
            // cadenas[1] += (cadenas[1] && hsStr ? ';' : '') + (hsStr ? (noStar(el)+'-'+apuestas[el].map(fg => fg.codigo)) : '');
            cadenas[1] += hsStr ? ((noStar(el)+'-'+apuestas[el].map(fg => fg.codigo)) + ';') : '';
            return cadenas
        }, ['', '']);

    const dteForCombs = this.inBloqueoModal ? (!initial ? depoched(this.fechaSorteo.getTime()) : depoched(nowEpoched())) : null;
    const api = this.inBloqueoModal ? `${this.$apiAdministrativo}ListarCombinacionBloqueada/${this.tokUsr}/${dteForCombs}/${funcion}/${window.btoa(cadenaSorteo)}/${window.btoa(cadenaApuesta || '0')}/${window.btoa(cadenaSimbolo || '0')}` 
        : `${this.$apiAdministrativo}ConsultarDatosCombinacion/${this.auth_tokenUsuario}/${funcion}/${window.btoa(cadenaSorteo)}/${window.btoa(cadenaApuesta || '0')}/${window.btoa(cadenaSimbolo || '0')}`;

    this.tableDataLoading = true;
    fetchRequest({
        api,
        self,
        success: (rjson) => {
            self.tableDataLoading = false;
            const sorteos = rjson.datos;
            if (self.inBloqueoModal) self.fechaCargaModal = new Date(self.fechaSorteo);

            const allSorteos = !sorteos ? [] : sorteos.reduce((sorteosTodos, sorteo_) => {
                const strs = sorteo_.combinacion.map(comb => {
                    const sec0 = sorteo_.codigoSeccion instanceof Array ? sorteo_.codigoSeccion[0] : sorteo_.codigoSeccion;
                    const idSorteo = objOrNotAtobrToDash(sorteo_,'codigoProducto',[sec0 || 'MA=='],'numeroSorteo','codigoTurno');
                    const sorteosToGet = initial ? self.sorteosAllByID : sorteosYaValidadosObj;
                    const listSrt = sorteosToGet[idSorteo] || {};
                    let theresNum, theresFig, theresManyFigs;
                    for (let i = 0; i < comb.elemento.length; i++) {
                        const elm = comb.elemento[i];
                        const hasTabla = self.getTablaFigura(/* window.atob( */elm.codigo/* ) */);
                        const theresFigCopy = !!theresFig;
                        if (!theresNum) theresNum = !hasTabla;
                        if (!theresFigCopy) theresFig = !!hasTabla;
                        if (!theresManyFigs && theresFigCopy) theresManyFigs = !!hasTabla;
                        if (theresNum && theresFig && theresManyFigs) break;
                    }
                    const elsApuestaFigs = comb.elemento.reduce((elements, elem) => {
                        const varEl = /* window.atob( */elem.descripcion/* ) */;
                        const figOfTable = self.getTablaFigura(/* window.atob( */elem.codigo/* ) */, varEl);
                        if (figOfTable) elements[0].push(figOfTable)
                        else elements[1] = [...(elements[1] || []), varEl];
                        return elements
                    }, [[]]);

                    const apusNum = elsApuestaFigs.length > 1 ? elsApuestaFigs.pop() : null;

                    const newSrt = new Sorteo({
                        ...listSrt,
                        id: idSorteo,
                        guid: idSorteo + '-' + (new Date().getTime()) + Math.random().toFixed(16),
                        sorteoNumber: sorteo_.numeroSorteo,
                        product: sorteo_.codigoProducto,
                        ...(!self.inBloqueoModal ? {
                            asked: numberAndComma(sorteo_.venta, true),
                            played: numberAndComma(sorteo_.venta, true),
                            weightPrice: numberAndComma(comb.premio, true),
                            typeBlocking: "0",
                            blockedBy: "100",
                        } : {
                            typeBlocking: comb.tipoBloqueo,
                            blockedBy: comb.porcentajeBloqueo,
                        }),
                        bet: comb.elemento.length == 1 ? (
                                theresFig 
                                ? self.getTablaFigura(/* window.atob( */comb.elemento[0].codigo/* ) */, /* window.atob( */comb.elemento[0].descripcion/* ) */) 
                                : /* window.atob( */comb.elemento[0].descripcion/* ) */
                            ) : [
                                ...(theresNum ? apusNum : []), 
                                ...(theresManyFigs ? elsApuestaFigs : theresFig ? elsApuestaFigs[0] : [])
                            ],
                        codigoTurno: window.atob(sorteo_.codigoTurno),
                        section: [sorteo_.codigoSeccion],
                        ...comb.elemento.reduce((numEls, elmnt, i, arr) => {
                            const isN = !self.getTablaFigura(/* window.atob( */elmnt.codigo/* ) */);
                            const isApu = isN && numEls.apuestas;
                            if (isApu || (!isN && numEls.simbolos)) {
                                numEls.betsShortcut += `-${elmnt.codigo}-${elmnt.descripcion}`;
                                numEls[isApu ? 'apuestas' : 'simbolos'].push({codigoElemento: /* window.atob( */elmnt.codigo/* ) */, jugada: /* window.atob( */elmnt.descripcion/* ) */});
                            }
                            if (i == arr.length - 1) {
                                if (!numEls.apuestas) numEls.apuestas = [];
                                if (!numEls.simbolos) numEls.simbolos = [];
                            }
                            return numEls
                        }, {apuestas: theresNum ? [] : null, simbolos: theresFig ? [] : null, betsShortcut: idSorteo}),
                    });
                    return newSrt
                });
                sorteosTodos.push(...strs);
                return sorteosTodos
            }, []);

            self.tableData = allSorteos;

            if (!self.inBloqueoModal) setTimeout(() => {
                self.setTableDeselected(false);
            });
        },
        error: (rjson) => {
            if (self.selectedOnesFromTable.length) self.setTableDeselected();
            self.tableData = [];
            return caseWrong({
                self, error: rjson.msg || rjson, passMsg: Boolean(rjson.msg), loading: 'tableDataLoading',
                defaultMsg: labels.defaultErrorMsg,
            });
        },
        done: () => { self.btnLoading = false }
    });
}

async function addBlockedOnes (sorteos, lock = true, tableN = '', tipoBloqueo = '0', bloqueadoPor = '0') {
    const self = this;  
    if (!lock) this['tableDataLoading'+tableN] = true;
    const lesSorteos = sorteos.reduce((srts, srt) => {
      srts[0][srt.id] = {
        ...(!srts[0][srt.id] ? {
          codigoProducto: srt.product,
          codigoSeccion: srt.section[0],
          codigoTurno: window.btoa(srt.codigoTurno),
          numeroSorteo: srt.sorteoNumber,
          // tipoFuncion: "MDc2Nw==",
        } : srts[0][srt.id]),
        combinacion: [
          ...(srts[0][srt.id]?.combinacion || []),
          {   
            elemento: [...srt.apuestas, ...srt.simbolos].map(elmn => ({
              codigo: elmn.codigoElemento,
              descripcion: elmn.jugada
            }))
          }
        ],
      };
      srts[1].push(srt.guid);
      return srts
    }, [{}, []]);
  
    let respAux;
    const resp = await fetchRequest({
        api: this.$apiAdministrativo+'BloquearCombinacion',
        method: 'POST',
        self, promiseReturn: !lock,
        body: {
          tokenUsuario: !self.inBloqueoModal ? self.auth_tokenUsuario : self.tokUsr,
          tipoBloqueo: window.btoa(tipoBloqueo),
          porcentajeBloqueo: window.btoa(bloqueadoPor),
          fecha: !self.inBloqueoModal ? depoched(nowEpoched()) : depoched(self.fechaSorteo.getTime()),
          tipoOperacion: window.btoa(lock ? '1' : '0'), // 1:Bloquear combinación, 0: Desbloquear combinación
          sorteo: Object.values(lesSorteos[0]),
        },
        success: (rjson) => {
          if (!lock) {
            if (self.inBloqueoModal && self.esHoy) self.updateLockCombinationsTable(sorteos);
            return rjson;
          }
          const tblDta_ = self['tableData'+tableN].reduce((dts, dt) => ({...dts, [dt.betsShortcut]: dt}), {});
          const forTable_ = sorteos.reduce((srts, srt) => {
            if (tblDta_[srt.betsShortcut]) tblDta_[srt.betsShortcut] = srt;
            else srts.push(new Sorteo({...srt}));
            return srts
          }, []);
          self['tableData'+tableN] = [...Object.values(tblDta_), ...forTable_];
          if (!self.inBloqueoModal) {
            if (sorteos.length == self.tableData.length) {
                self.setTableDeselected();
                self.tableData = [];
            }
            else self.removeFromTable(false, lesSorteos[1], '', 'guid');
            self.closeModal('opcionesbloqueo');
          }
          self.fireToastMsg('El bloqueo fue registrado con éxito', {icon: 'success'});
        },
        error: (rjson) => {
          console.error(rjson);
          const errObj = { isError: true, msg: rjson.msg, rjson };
          if (!lock) return errObj;
          else respAux = errObj;
        },
        done: () => { if (!lock) this.tableDataLoading = false }
    });
  
    if (resp && !resp.isError) {
      if (!lock) return !!resp;
    }
    else if (resp || respAux) {
      const resp_ = resp || respAux;
      if (!self.inBloqueoModal) this.closeModal('opcionesbloqueo');
      const resCW = caseWrong({
        self, error: resp_.msg || resp_.rjson, passMsg: Boolean(resp_.msg), loading: ('tableDataLoading' + (self.inBloqueoModal || lock ? '' : '2')),
        defaultMsg: labels.defaultErrorMsg,
      });
      if (!lock) return resCW;
    }
}

export default {
    data() {
        return {
            inBloqueoModal: false,
            btnLoading: false,
        }
    },
    methods: {
        fetchCombinaciones,
        addBlockedOnes,
    },
}