import { Sorteo } from "@/_helpers/classes";
import { /* dateDepocher, dateTimeOfTheDay,  */ hasStar, nameCode, nameDetailed, noStar, objFilter, objMap, objReduce, orientedSlice, randBetween, randNumbers, rounderPlusOne, sumaJS, theBreak, toMoney } from '@/_helpers/formatters';
import { caseWrong, fetchRequest } from "@/_helpers/fetchToolkit";
import { codesToLetters, resCodes } from "@/_helpers/general";
import { LABELS as labels } from "@/_helpers/labels";

//Metodos:
async function toggleSideFigsFunc(action = 'toggle') {
  if (this.applyingHelp || !this.selectedOnesUse || !this.theres_campo_figura) return;
  this.applyingHelp = true;
  
  if (action == 'toggle') this.toggleSideFigures();
  else this.updateSideFiguresVisible(action == 'show');

  await this.awaiter(this.timeOfSideHelp);
  this.applyingHelp = false;
  $('#helpers')[action == 'toggle' ? (!this.sideFiguresVisible && 'hide' || 'show') : action]();
}


// REDIRECIONADOR DE METODOS POR ATAJOS DEL TECLADO
async function shorcutCases(theCode, shouldFocus = false, srtId = null) {
  if (this.addingToTableInProgress || this.tableDataLoading) return;
  const self = this;
  const case_ = typeof theCode === 'string' ? theCode
    : this.shortcutsSet['Ayuda'].includes(theCode) ? 'Ayuda'
      : this.shortcutsSet['FiguresSide'].includes(theCode) ? 'FiguresSide'
        : this.shortcutsSet['Imprimir'].includes(theCode) ? 'Imprimir'
          : this.shortcutsSet['Azar'].includes(theCode) ? 'Azar'
            // : this.shortcutsSet['SorteoFiguras'].includes(theCode) ? 'SorteoFiguras' // SELECCIONO VER SOTEOS CON FIGURAS, O TRIPLES-TERMINALES
            : this.shortcutsSet['Refrescar'].includes(theCode) ? 'Refrescar'
              : this.shortcutsSet['Horario'].includes(theCode) ? 'Horario' // SELECCIONO ALGUNA OPCION DE HORARIO DE LOS SORTEOS
                : (/* !this.shortcuts.figuresTableShowed &&  */[ // SE CUMPLE SI LA TABLA DE ANIMALITOS NO ESTA MOSTRADA, para seleccion de filtros en la seccion de ayuda
                  ...this.shortcutsSet['TicketsHelpers'],
                  ...this.shortcutsSet['ApuestasHelpers'],
                  ...this.shortcutsSet['SorteosHelpers'],
                  ...this.shortcutsSet['ProductoHelpers'],
                  ...this.shortcutsSet['AtajosSorteo'],
                ].includes(theCode)) ? 'Helpers'
                  // 
                  : /* this.mode !== 'figures' ? ( */ // NO ESTA EN MODO VER TABLA DE FIGURAS
                  this.shortcutsSet['ModoApuesta'].includes(theCode) ? 'ModoApuesta' // SELECCIONO CUALQUIERA DE LAS OPCIONES DE APUESTA
                    /* : '') */
                    : (/* 
      (this.shortcuts.figuresTable && this.shortcutsSet['TablaFiguras'].includes(theCode)) ? 'TablaFiguras' // SELECCIONO VER TABLA DE FIGURAS
    :  */(this.shortcuts.figuresTableShowed && this.shortcutsSet['TablaFigurasShortcuts'].includes(theCode)) ? 'TablaFigurasShortcuts' // SELECCIONO ALGUNOS DE LOS ATAJOS DE LA TABLA DE ANIMALITOS
                        : '');

  if (this.printingPDF && case_ !== 'Imprimir') return;

  // LOS DIFERENTES CASOS QUE SE DISPARAN POR LAS ACCIONES DEL TECLADO
  switch (case_) {
    case 'Ayuda': // F1 // MUESTRA U OCULTA LA SECCION DE AYUDA
      if (this.applyingHelp) return;
      this.applyingHelp = true;
      this.toggleSideHelp();
      await this.awaiter(this.timeOfSideHelp);
      this.applyingHelp = false;
      // $('#helpers')[!this.sideHelpVisible && 'hide' || 'show']();
      break;
    case 'Azar': // F3
      if (this.selectedOnesUse) this.makeRandom();
      if (shouldFocus) setTimeout(this.checkFocusInput, 1);
      break;
    case 'FiguresSide': // F4 // MUESTRA U OCULTA LA SECCION DE LISTA DE FIGURAS
      this.toggleSideFigsFunc();      
      break;
    case 'Helpers': { // A B U - F G O Q X - H K Y Z - F8 - I J L R V W F7 // EJECUCION DE LAS ACCIONES DE LAS DISTINTAS OPCIONES DE LA SECCION DE AYUDA
      const ctl = codesToLetters;
      const isAtajosSorteos = this.shortcutsSet['AtajosSorteo'].includes(theCode);
      const theCase = isAtajosSorteos ? 'atajos' 
        : this.shortcutsSet['ProductoHelpers'].includes(theCode) ? ctl[theCode] 
        : this.helpers_obj[`${theCode}`][1];

      if (theCase !== ctl[74] && (theCase === ctl[90] || this.shortcutsSet['TicketsHelpers'].includes(theCode))) {
        if (['a', 'b', 'u', 'z'].includes(theCase)) this.closeHelp();
        switch (theCase) { // DESCOMENTAR LUEGO CUANDO ESTEN TODAS LAS OPCIONES DEL MENU
          case 'a':
            this.$router.push({ name: 'reporteCuadreCaja' });
            break;
          case 'b':
            this.$router.push({ name: 'TicketValidate' });
            break;
          case 'u':
            this.$router.push({ name: 'anularTicket' });
            break;
          // case 'v': 
          //   this.$router.push({ name: 'ticketRepeat' });
          //   break;
          case 'z':
            this.$router.push({ name: 'reporteResultados' });
            break;
          default: // otros
            // this.$router.push({ name: 'Dashboard' });
            break;
        }
      }
      else if (this.shortcutsSet['SorteosHelpers'].includes(theCode) || theCase === 'atajos' || theCase === ctl[119]) {
        const multiSelect = async (freq = false, byAtajo = null, byAllGroup = false) => {
          self.doingMultiSelection = true;
          const ss = self.selected_sorteos.map(ss_ => new Sorteo({ ...ss_ }));
          self.selectedOnesCopy = [
            self.sorteos_por_productos.map(s => new Sorteo({ ...s })),
            self.visibleFields.map(vf => ({ ...vf })),
            ss.map(ss => ss.id)
          ];
          self.multSelOn = [true, false];
          let cannot_, uncheck = 0;
          const srtsLen = self.sorteos_por_productos.length;
          const byAllGroupYesIds = [];

          for (let i = 0; i < srtsLen; i++) {
            const spp = self.sorteos_por_productos[i];
            if (
              (freq && !self.frequents.includes(spp.forFrequent())) ||
              (byAtajo && !spp.atajo.includes(byAtajo)) ||
              (byAllGroup && (spp.product != ss[0].product))
            ) {
              spp.checked = false;
              uncheck++;
              continue;
            }
            if (spp.checked) {
              if (byAllGroup) byAllGroupYesIds.push(spp.id);
              continue;
            }
            self.considerarApuesta(null, spp, false, byAllGroup);
            await self.awaiter();
            cannot_ = self.multSelOn[1];
            if (cannot_) {
              if (byAllGroup) {
                self.multSelOn[1] = false;
              }
              else break;
            }
            else if (byAllGroup) byAllGroupYesIds.push(spp.id);
          }

          self.sorteos_por_productos.forEach((srt) => 
            srt.checked = byAllGroup ? byAllGroupYesIds.includes(srt.id)
              : !cannot_ && uncheck < srtsLen ? (
                freq ? self.frequents.includes(srt.forFrequent()) :
                byAtajo ? srt.atajo.includes(byAtajo) : 
                true
              ) 
              : self.selectedOnesCopy[2].includes(srt.id)
          );
          if (byAllGroup || !cannot_) {
            if (self.selected_sorteos.length && uncheck < srtsLen) {
              self.showRefresh = true;
              setTimeout(async () => { 
                const srtsCheckers = document.querySelectorAll('.form-check.sorteo-check');
                let lastOneToScrollInto;
                for(let i = srtsCheckers.length; i > 0; i--) {
                    const inputItem = srtsCheckers.item(i - 1).children.item(0);
                    if (inputItem?.checked && inputItem.scrollIntoView) lastOneToScrollInto = inputItem;
                    if (inputItem?.checked) break;
                }
                if (self.visibleFields.some(f => f.tipo == 'img' && f.dropdown == '0')) {
                  if (self.applyingHelp) await self.awaiter(self.timeOfSideHelp);
                  await self.toggleSideFigsFunc('show');
                }
                if (lastOneToScrollInto) lastOneToScrollInto.scrollIntoView({block: "end", inline: "nearest"});
              });
            }
            else if ((freq || byAtajo) && srtsLen) {
              let msg_ = 'No hay sorteos frecuentes en la lista actual';
              if (byAtajo) {
                const complement = byAtajo == '01' ? 'doble terminal' : byAtajo == '02' ? 'triple' : /* byAtajo == '03' ? 'doble figura' :  */byAtajo == '04' ? '38 figuras' :
                  byAtajo == '05' ? '75 figuras' : byAtajo == '06' ? '76 figuras' : /* 07 */'40 figuras';
                msg_ = `No hay sorteos de ${complement} en la lista actual, por favor verifique haber seleccionado el tipo de sorteo correcto`;
              }
              self.fireToastMsg(msg_, { icon: 'info' });
            }
          }
          self.multSelOn = [false, false];
          self.selectedOnesCopy = [[], [], []];
          self.doingMultiSelection = false;
        };

        if (isAtajosSorteos) {
          const atajos = this.shortcutsSet.AtajosSorteo;
          const atajoCase = theCode === atajos[0] ? '01' : theCode === atajos[1] ? '02' : /* theCode === atajos[2] ? '03' :  */theCode === atajos[/* 3 */2] ? '04' : theCode === atajos[/* 4 */3] ? '05' : theCode === atajos[/* 5 */4] ? '06' : theCode === atajos[/* 6 */5] ? '07' : null;
          if (atajoCase) {
            if (this.multSelOn[0]) return;
            this.closeHelp();
            if (!this.doingMultiSelection) await multiSelect(false, atajoCase);
          }
        }
        else if (theCase === ctl[72]) {
          if (!this.frequents.length || this.multSelOn[0]) return;
          this.closeHelp();
          if (!this.doingMultiSelection) await multiSelect(true);
        }
        // case 'j':
        //   // if (this.sorteosAll.figures) {
        //   //   // if (this.mode !== 'figures') this.shorcutCases('SorteoFiguras');
        //   //   this.shorcutCases('TablaFiguras');
        //   // }
        //   break;
        else if (theCase === ctl[89]) {
          if (!srtId) this.closeHelp();
          this.sorteos.forEach(srt => { if (!srtId || srt.id != srtId) srt.checked = false });
        }
        // else if (theCase === ctl[73]) {
        //   const theres_astral_in = this.sorteosFilteredShared(/* false */);
        //   if (theres_astral_in.length/*  || this.astralShown !== 'both' */) {
        //     const val = /* this.astralShown === 'none' || this.astralShown === 'both' ? 'only' :  */'none';
        //     const doB = val === 'only'/*  && !theres_astral_in.some(srt => srt.isAstral); */
        //     if (doB) return;
        //     // this.astralShown = val;
        //     this.showRefresh = true;
        //     this.sorteos.forEach(srt => { 
        //       if (val !== 'both' && ((val === 'only'/*  && !srt.isAstral */) || (val === 'none'/*  && srt.isAstral */))) 
        //         srt.checked = false
        //     });
        //   }
        // }
        else {// k or f8
          if (this.multSelOn[0]) return;
          this.closeHelp();
          if (!this.doingMultiSelection) {
            const isF8 = theCase === ctl[119];
            if (isF8 && (!this.selected_sorteos.length || !this.sameSorteosSelected)) this.fireToastMsg('Para seleccionar todas las horas de un producto deben estar seleccionados solo uno o mas sorteos de ese tipo de producto', { icon: 'info' });
            else await multiSelect(false, null, isF8);
          }
        }
      }
      else if (['o', 'x'].includes(theCase)) {
        this.closeHelp();
        const len = this.tableData.length; //selectedSorteos
        const all = theCase === 'x';
        const doMulti = this.tableMultidelete && this.selectedOnesFromTable.length;
        const msg = len ? (all ? `¿Estás seguro que deseas borrar${doMulti ? '' : ' todas'} las apuestas${doMulti ? ' seleccionadas' : ''}?` : '¿Estás seguro que deseas borrar la última apuesta?') : this.labels.betsNotExist(null, '', true);
        const result = await this[`fire${len ? 'Alert' : 'Toast'}Msg`](msg, {});
        if (len && result.isConfirmed) {
          let dta_;
          if (doMulti) dta_ = this.selectedOnesFromTable.map(jug => jug.idJugada);
          this.removeFromTable(all && !doMulti, !all ? this.tableData[len - 1] : dta_, '', all && doMulti ? 'idJugada' : 'id'); //selectedSorteos
          this.fireToastMsg(all ? (doMulti ? this.labels.deleteAllSeleted() : this.labels.deleteAll()) : this.labels.deleteOne(), { icon: 'success' });
        }
      }
      else if (theCase /* === */ !== 'j' /* || */ && ![...(!this.sidersTotal ? ['g'/* , 'l' */] : [])].includes(theCase)) { // MOMENTANEO EL IF self.sidersTotal
        this.closeHelp();
        this.showModal = true;
        setTimeout(() => {
          if (theCase === 'j') {
            if (!self.visibleModalDuplicar) self.openModalDuplicar();
          }
          else if (!self.visibleModal) self.openModalLot({
            title: self.helpers_obj[`${theCode}`][0],
            scLttr: self.helpers_obj[`${theCode}`][1],
          });
        });
      }
      break;
    }
    case 'Horario': { // M T E D // SE SETEA EL TIPO DE HORARIO POR EL QUE SE ESTA FILTRANDO LA LISTA DE SORTEOS
      const hor = this.shortcutsSet.Horario;
      const cond = theCode === hor[2] ? '01' : theCode === hor[3] ? '02' : theCode === hor[1] ? '03' : null;
      if (!cond || this.apiLists.turno[cond]) {
        const previous = `${this.sorteoHorario}`;
        this.sorteoHorario = this.sorteos_list_helpers_obj[`${theCode}`];
        if (!this.sorteos_por_productos.length) {
          this.fireToastMsg(this.labels.notAvailableSorteos, {});
          this.sorteoHorario = previous;
        }
        else {
          this.closeHelp();
          this.uncheckWhichShould();
        }
      }
      this.updateSelectedProducts();
      break;
    }
    case 'Imprimir': // F2
      if (this.tableData.length) this.printPDF();
      break;
    case 'ModoApuesta': { // N S P C // CLICKEA LA OPCION DE JUGADA (numer, serie, permuta, corrida) DE ACUERDO AL ATAJO DE TECLADO SELECCIONADO
      const lttr = this.forms_key_obj[`${theCode}`];
      const modeJsonKey = this.forms[lttr].jsonKey;
      if (+modeJsonKey < 1 || this.apiLists.tipoFuncion[modeJsonKey]) this.$refs['shortcut' + this.forms[lttr].code][0]?.$el?.click();
      break;
    }
    case 'Refrescar':
      this.refreshMeth();
      break;
    // case 'SorteoFiguras': { // F3 // SETEA EL MODO DE VISUALIZACION, SI VER FIGURAS O POR TRIPLES-TERMINALES, Y DESELECCIONAS LOS SORTEOS QUE ESTEN TILDADOS
    //   if (!(!this.sorteosAll.triplesterminals || !this.sorteosAll.figures) && this.apiLists.tipoSeccion['01'] && this.apiLists.tipoSeccion['02']) this.resetLottery(true);
    //   break;
    // }
    // case 'TablaFiguras': // F4 // ABRE O CIERRA TABLA DE ANIMALITOS, Y SI CIERRA ANIMALITOS ABRE AYUDA SI ESTA ESTA CERRADA
    //   $('#tabla-figuras')[this.shortcuts.figuresTableShowed && 'hide' || 'show']();
    //   this.shortcuts.figuresTableShowed = !this.shortcuts.figuresTableShowed;   
    //   break;
    case 'TablaFigurasShortcuts': { // A B C G - N O P S // SETEA EL FILTRO ACTUAL APLICADO, CORRESPONDIENTE CON LOS ATAJOS DE TECLADO DE LA TABLA DE FIGURAS
      const filter = (code) => {
        const liners = ['liner1', 'liner2', 'liner4', 'liner6', 'liner8', 'liner9', 'liner11', 'liner12'];
        const shortCLiners = self.shortcutsSet['TablaFigurasShortcuts'].reduce((keysObj, key_, i) => ({ ...keysObj, [`${key_}`]: liners[i] }), {});
        return shortCLiners[`${code}`] || null;
      };
      const filter_ = filter(theCode);
      const theFilter = filter_ ? (this.filterSelected === filter_ ? '' : filter_) : null;
      this.filterSelected = theFilter == null ? this.filterSelected : theFilter;
      break;
    }
  }

  if (!this.selectedOnesUse/*  && this.astralShown === 'both' */) this.showRefresh = false;
}

// RETORNA LISTADO DE PERMUTACIONES DADO UN NUMERO, EL LEN MAXIMO PARA LAS PERMUTACIONES DEVUELTAS ES DE 3
function getPermutations(str = '', slc = 3) {
  if (!str.length || str.length === 1) return [str];
  const permutations = [];
  for (let i = 0; i < str.length; i++) {
    const splitStr = str.split('');
    const currentElem = splitStr.splice(i, 1)[0];
    const subPerms = getPermutations(splitStr.join(''), slc);
    subPerms.forEach(combination => {
      const cc = currentElem.concat(combination);
      const sliced = cc.slice(0, slc);
      if (!permutations.includes(sliced)) permutations.push(sliced);
    });
  }
  return permutations;
}

// RETORNA LISTADO DE CORRIDA DADO UN NUMERO DE INICIO Y UNO DE FIN
function getUntil(from = '', until = '') {
  if (!from.length || !until.length) return [from || until];
  const start = +from, end = +until, corrida = [];
  const fl_ = from.length < 3;
  for (let i = start; i <= end; i++) {
    const val = `${i}`.length;
    const result = val < 3 ? (val > 1 ? ((fl_ ? '' : '0') + i) : ((fl_ && '0' || '00') + i)) : `${i}`;
    corrida.push(result);
  }
  return corrida
}

// RETORNA LISTADO DE SERIE DADO UN NUMERO CON UNO O MAS *
function getSerie(str = '') {
  if (!str.length || !str.includes('*')) return [str];
  const serie = [];
  for (let i = 0; i < 10; i++) serie.push(...getSerie(str.replace('*', i)));
  return serie
}

async function toTable(data, msgs = [], discardMod = []) {
  const self = this;
  await this.awaiter(1000);
  let selectedSorteos = [], theAdded = 0, theSolo = false, someDiscard = discardMod.length ? {
    'mode': discardMod.reduce((mods, mod) => ({
      ...mods,
      [mod[0]]: window.atob(self.apiLists.modalidad[mod[0]].nombreModalidad || ''),
      ['sorteo-' + mod[0]]: mod[1]
    }), {})
  } : {};
  for (let i = 0; i < data.length; i++) {
    const vals = await this.addToTable(theAdded, i, ...data[i]); // MODIFICAR PROXIMAMENTE
    if (vals) {
      theAdded += vals.added;
      theSolo = vals.solo;
      selectedSorteos = [...selectedSorteos, ...vals.selectedSorteos];
      if (vals.someDiscard && typeof vals.someDiscard === 'object') Object.keys(vals.someDiscard).forEach(dis => {
        Object.keys(vals.someDiscard[dis]).forEach(mde => {
          if (!mde.includes('sorteo-') && (!someDiscard[dis] || !someDiscard[dis][mde])) someDiscard[dis] = { ...(someDiscard[dis] || {}), [mde]: vals.someDiscard[dis][mde] }
          else if (mde.includes('sorteo-')) {
            const toAdd = vals.someDiscard[dis][mde].filter(nameSorteo => !someDiscard[dis] || !someDiscard[dis][mde] || !someDiscard[dis][mde].includes(nameSorteo));
            if (toAdd.length) someDiscard[dis] = { ...(someDiscard[dis] || {}), [mde]: [...((someDiscard[dis] && someDiscard[dis][mde]) || []), ...toAdd] };
          }
        });
      });
    }
  }
  this.tableDataLoading = false;
  if (selectedSorteos.length) {
    selectedSorteos = selectedSorteos.map((sSorteo, si) => {
      const obj = new Sorteo({
        ...sSorteo,
        idJugada: self.betsIncrementIndex + si + 1,
      });
      return obj
    });
    if (selectedSorteos.length) self.increaseIndex(selectedSorteos.length);
    this.tableData = [...this.tableData, ...selectedSorteos]; //selectedSorteos
  }
  for (let k = 0; k < msgs.length; k++) {
    const dismissabledToast = await this.fireToastMsg(msgs[k], {});
    if (dismissabledToast.isDismissed && dismissabledToast.dismiss === 'close') {
      this.$swal.stopTimer();
      await this.awaiter();
    }
  }
  if (theAdded) {
    const bets2addMsg = theSolo ? this.labels.onlyAdded(theAdded, this.maxInTable) : this.labels.betsAdded(theAdded);
    await this.fireToastMsg(bets2addMsg, { icon: theSolo ? 'info' : 'success' });
  }

  const activeOnes = this.tableData.filter(thisSrt => {
    const passed = self.notPassed(thisSrt);
    if (!passed) {
      const modality = self.getModality(thisSrt);
      // if(!someDiscard || typeof someDiscard !== 'object') someDiscard = {}; // porque mas arriva en esta function se ve que sea como sea se inicializa en un objeto
      someDiscard['date'] = {
        ...(someDiscard['date'] || {}),
        [modality]: [],
        ['sorteo-' + modality]: [...((someDiscard['date'] && someDiscard['date']['sorteo-' + modality]) || []), thisSrt.nombreSorteo()]
      };
    }

    return passed
  });

  const some_discard = Object.keys(someDiscard);
  const br = some_discard.length && some_discard.length > 1;
  if (some_discard.length && ['min', 'mode', 'date', 'mult'].some(dis => some_discard.includes(dis))) {
    const discartedOnes = some_discard.map(dis => {
      let text, msgs;

      const mds = Object.keys(someDiscard[dis]).reduce((vals, md) => [...vals, ...(!md.includes('sorteo-') ? [[
        someDiscard[dis][md],
        someDiscard[dis]['sorteo-' + md]
      ]] : [])], []);

      const innerList = mds.map(md_ => md_[1]).reduce((lis, itm) => {
        const lis_ = itm.map(itm_ => `${br ? '<li>' : `${theBreak}- `}${itm_}${br ? '</li>' : ''}`);
        lis_.forEach(li => { if (!lis.includes(li)) lis.push(li) });
        return lis
      }, []).join('');

      switch (dis) {
        case 'min':
          msgs = mds.map(md_ => md_[0]).map(vals => `${vals[0]} mínimo $${vals[1]}`).join(', ');
          text = `no cumplir con su monto mínimo (${msgs}): ${br ? '<ul>' : ''}${innerList}${br ? '</ul>' : ''}`;
          break;
        case 'mode':
          msgs = mds.map(md_ => md_[0]).join(', ');
          text = `no corresponder con ${mds.length > 1 ? ' alguna de las modalidades' : 'la modalidad'} ${msgs}: ${br ? '<ul>' : ''}${innerList}${br ? '</ul>' : ''}`;
          break;
        case 'mult':
          msgs = mds.map(md_ => md_[0]).map(vals => `${vals[0]} múltiplo de ${vals[1]}`).join(', ');
          text = `que sus montos no cumplen con sus múltiplos (${msgs}): ${br ? '<ul>' : ''}${innerList}${br ? '</ul>' : ''}`;
          break;
        default: /* 'date' */
          text = `superar su fecha de cierre: ${br ? '<ul>' : ''}${innerList}${br ? '</ul>' : ''}`;
          break;
      }
      text = `${br ? '<li>' : ''}${text}${br ? '</li>' : ''}`;
      return text
    });
    if (activeOnes.length < this.tableData.length) this.tableData = [...activeOnes];
    this.fireToastMsg(`${br ? '<p>' : ''}Algunas jugadas de los siguientes sorteos fueron descartadas por${br ? ':</p><ul class="text-start">' : ' '}${discartedOnes.join(br ? '' : ', ')}${br ? '</ul>' : '.'}`, { icon: 'info', asHtml: br });
  }
}

function execSpecial(attr, size_p) {
  if (this.form[attr]['mult']) return;
  const childs = Object.keys(this.form[attr]);
  if (this.form[attr][childs[1]] && (+this.form[attr][childs[1]] <= +this.form[attr][childs[0]])) {
    const result = +this.form[attr][childs[0]] + 1;
    if (result.toString().length > size_p) {
      this.form[attr][childs[0]] = result - 2;
      this.form[attr][childs[1]] = result - 1;
    }
    else this.form[attr][childs[1]] = result;
  }
}

function setFilter(val) {
  this.filterSelected = val;
  if (val) this.clearAllFigureData();
}

// METODO PARA AGREGADO DE SORTEOS SELECCIONADOS, EN LA TABLA DE SORTEOS
async function addToTable(componentes, sorteos, toastMessagesParam = [], refreshSorteosList = false) {
  const vieneDeModal = componentes.esModal;
  const fieldsFilled = !vieneDeModal ? objReduce(componentes, (allFields, ck) => [...allFields, noStar(ck)], []) : null;
  const treatAsRepetir = vieneDeModal && componentes.funcName == 'repetir-apuesta';
  let toastMessages = [...toastMessagesParam];
  const self = this;
  if (this.addingToTableInProgress) return;
  this.addingToTableInProgress = true;

  if (this.tableData.length == this.maxInTable) { // SI YA SE ALCANZO EL MAXIMO DE ITEMS A ANNADIR EN LA TABLA DE APUESTAS
    this.fireToastMsg(`Ya se alcanzó el máximo de ${this.maxInTable} apuestas permitidas en la tabla.`, {});
    this.addingToTableInProgress = false;
    return;
  }

  this.tableDataLoading = true;
  const formCopy = { ...this.form };
  if (!vieneDeModal) this.clearAllForms(this, this.form);
  await this.awaiter(100);

  const losSorteos = [], losSorteosObj = {};
  let breakAll, someBetsFailed, count = this.tableData.length;

  const maxTbl = this.maxInTable;
  const makeSorteo = ({
    sorteo, secInd, i1 = 0, i2 = 0, i3 = 0, i4 = 0, calcMonto = 0,
    soloApuestas = [], lasApuestas = [], losSimbolos = [], ordenMultiplicador = [],
    toBet = false, soBets0 = '', descInTicket = ''
  }) => {
    const theSorteo = new Sorteo({
      ...sorteo,
      guid: sorteo.id + '-' + secInd + '-' + (new Date().getTime()) + `${i1}${i2}${i3}${i4}` + Math.random().toFixed(16),
      idJugada: self.betsIncrementIndex + 1,
      ...(!treatAsRepetir ? {
        bet: soloApuestas.length ? (toBet ? [...soloApuestas] : (soBets0 instanceof Array ? [soBets0] : soBets0)) : null,
        ...(!vieneDeModal ? {
          asked: calcMonto,
          played: calcMonto,
        } : {}),
        functionType: vieneDeModal ? '' : (+self.forms[self.mode].jsonKey > 0 ? self.forms[self.mode].jsonKey : ''),
        typeBet: i2,
        currentModality: i2,
        apuestas: lasApuestas,
        simbolos: losSimbolos,
        descApuesta: descInTicket,
        multiplicador: ordenMultiplicador,
      } : {}),
      section: [sorteo.section[secInd]],
      azar: vieneDeModal || !self.randomized ? '0' : '1',
    });

    const betD = theSorteo.betDetail() + ' ' + theSorteo.id + ' ' + theSorteo.typeBet + ' ' + sorteo.section[secInd];
    if (theSorteo.bet && !losSorteosObj[betD]) {
      if (count == maxTbl) return true;
      losSorteosObj[betD] = true;
      losSorteos.push(theSorteo);
      count++;
      self.increaseIndex(1);
    } else someBetsFailed = true;
    return false;
  };

  for (let i1 = 0; i1 < sorteos.length; i1++) {
    const sorteo = sorteos[i1];
    // console.log('El sorteo', sorteo);
    for (let iterSorteo = 0; iterSorteo < (!vieneDeModal || treatAsRepetir ? 1 : (componentes.sorteos[sorteo.guid]?.length || 0)); iterSorteo++) {
      const componentesCurr = !vieneDeModal || treatAsRepetir ? componentes : componentes.sorteos[sorteo.guid][iterSorteo];
      if (treatAsRepetir) for (let secIndOut = 0; secIndOut < (sorteo.section || []).length; secIndOut++) {
        const [montoMinSrt, montoMulSrt] = ['montoMinimo', 'montoMultiplo'].map(minMult => sorteo.allTypes[sorteo.typeBet][minMult][this.moneda.codigoMoneda]);
        const montoJSrt = sorteo.played;
        // const precSrt = this.formMonto.precision;
        const minMultFailSrt = montoJSrt < montoMinSrt || !!this.modForJS(montoJSrt, montoMulSrt)/* this.getTruncated(montoJSrt % montoMulSrt, precSrt) */;
        if (minMultFailSrt) someBetsFailed = true;
        const doBrk1 = minMultFailSrt ? false : makeSorteo({ sorteo, secInd: secIndOut });

        if (doBrk1) {
          breakAll = true;
          break;
        }
      }
      else {
        const allTypesFilterd = !vieneDeModal ? sorteo.allTypes
          : objFilter(sorteo.allTypes, (modK) => this.rgxSiders.test(this.getModObj(modK)?.ordenSeleccion?.trim() || ''));
        const sidersCustom = !vieneDeModal ? this.siders : componentes.siders;
        for (const i2 in allTypesFilterd) {
          const camposAll = this.modalidadComponentes[window.btoa(i2)];
          // ESTO ES NUEVO
          const [currModFields, currModMandatories] = camposAll.reduce((allSet, cmpCurr) => [
            [...allSet[0], window.atob(cmpCurr.codigoElemento)],
            allSet[1] + (!+window.atob(cmpCurr.obligatorio) ? 0 : 1)
          ], [[], 0]);
          const shouldSkip = fieldsFilled ? (fieldsFilled.length < currModMandatories || fieldsFilled.some(fldCode => !currModFields.includes(noStar(fldCode)))) : false;
          if (shouldSkip) continue;
          // FIN DE LO NUEVO
          const { ordenSeleccion: ordSel_ } = this.getModObj(i2) || {};
          const ordSel = ordSel_.trim();
          const minLen = ordSel ? ordSel.replace('-', '').split(',').reduce((frst, scnd) => scnd - frst) : 0;

          const siderPassed = (jug) => {
            return vieneDeModal || (ordSel && self.rgxSiders.test(ordSel))
              ? ((ordSel.includes('-') && (sidersCustom.terminal || (!sidersCustom.punta && !vieneDeModal && jug.length == 2))) ||
                (!ordSel.includes('-') && (sidersCustom.punta || (!sidersCustom.terminal && !vieneDeModal && jug.length == 2))))
              : ordSel && !self.rgxSiders.test(ordSel) && jug.length > 2 && jug.length == minLen;
          };

          //
          const [montoMin, montoMul] = ['montoMinimo', 'montoMultiplo'].map(minMult => allTypesFilterd[i2][minMult][this.moneda.codigoMoneda]);
          const montoJInp = +(vieneDeModal ? sorteo.played : formCopy.apuestamonto);
          // const precInp = this.formMonto.precision;
          const minMultFail = montoJInp < montoMin || !!this.modForJS(montoJInp, montoMul)/* this.getTruncated(montoJInp % montoMul, precInp) */;

          // if (/* !siderPassed ||  */minMultFail) someBetsFailed = true;
          /* else  */if (camposAll) {
            let modFailed;
            const getBets = (cod, askType = false) => {
              let apus = componentesCurr[cod];
              if (!apus) {
                if (askType) return 'LISTA';
                apus = componentesCurr['**' + cod];
                if (apus?.length > 1) apus = [apus];
                else apus = componentesCurr['*' + cod];
              }
              if (askType) return 'NUMERO';
              return apus;
            };

            camposAll.forEach(({ codigoElemento, obligatorio }) => {
              const bets = getBets(window.atob(codigoElemento));
              const oblig = vieneDeModal ? 1 : window.atob(obligatorio);
              if (!bets && +oblig) {
                someBetsFailed = true;
                modFailed = true;
              }
            });
            const campos = camposAll.map((cmp, cmpI) => ({ ...cmp, inx: cmpI }))
              .sort((a, b) => {
                const arrA = getBets(window.atob(a.codigoElemento)), arrB = getBets(window.atob(b.codigoElemento));
                const lenA = (arrA || []).length, lenB = (arrB || []).length;
                return lenA && lenB
                  ? (a.inx > b.inx ? 1 : -1)
                  : (lenA < lenB ? 1 : -1)
              });
            const camposHermanos = campos.map(({ codigoElemento, obligatorio, nombre, nombreResumido }) => ({
              codig: window.atob(codigoElemento),
              oblig: +window.atob(obligatorio),
              nameSlice: (window.atob(nombre || this.componentesDta[codigoElemento]?.nombreElemento || '') || getBets(window.atob(codigoElemento), true)).slice(0, 3),
              resume: window.atob(nombreResumido || '')
            }));
            const campo = camposHermanos.shift();
            const cantApuestasMin = camposHermanos.reduce((cant, c_h) => cant + c_h.oblig, 0);
            const campoComps = getBets(campo.codig);
            let startedOn = -1;
            const compCampHerm = camposHermanos.find((ch, i) => {
              const cCh = getBets(ch.codig);
              if (cCh && !campoComps) startedOn = i;
              return cCh
            });

            const campoHComps = compCampHerm ? getBets(compCampHerm.codig) : null;
            const [apuestas, codigoPrincipal, resumePrincipal] = campoComps
              ? [campoComps, campo.codig, campo.resume || campo.nameSlice]
              : (compCampHerm && campoHComps) ? [campoHComps, compCampHerm.codig, compCampHerm.resume || compCampHerm.nameSlice]
                : Array(2).fill(null);
            const apuestasHermanas = camposHermanos.reduce((sisterBets, campH, i) => {
              if (startedOn >= 0 && i < startedOn) return sisterBets;
              const leBets = getBets(campH.codig);
              const theBet = (!sisterBets.length ? apuestas : sisterBets[i - 1]) && (i == startedOn ? [] : leBets);
              sisterBets.push({ bets: theBet || [], mandatory: campH.oblig, code: campH.codig, resume: campH.resume || campH.nameSlice });
              return sisterBets;
            }, []);

            if (!modFailed)
              modFailed = [...(apuestas || []), ...(apuestasHermanas.reduce((aHs, aH) => [...aHs, ...(aH.mandatory ? aH.bets : [])], []) || [])].some(bet_ => typeof bet_ == 'string' && bet_.length < minLen);
            const apuestasHermanasF = apuestasHermanas.reduce((betsSis, betSis) => [...betsSis, ...(betSis.bets.length ? [[[betSis.code, betSis.resume], betSis.bets]] : [])], []);
            if (modFailed) /* someBetsFailed = true */continue;
            else if (minMultFail) someBetsFailed = true; // NUEVO
            else if (apuestas && (campos.length == 1 || !cantApuestasMin || apuestasHermanasF.length)) {
              const apuestasH = [];
              const mayor = apuestasHermanasF.length ? Math.max(...apuestasHermanasF.map(ahf => ahf[1].length)) : 0;

              apuestasHermanasF.forEach(aHs => { // se recorren las apuestas hermanas [[ah], [ah], [ah], ...]
                for (let ai = 0; ai < mayor; ai++) {
                  const apuestaH_ = aHs[1][ai] || (aHs[1].length == 1 && typeof aHs[1][0] != 'string' ? aHs[1][0] : null);
                  const typeOfSisterBet = apuestaH_ instanceof Array ? 'array' : typeof apuestaH_;
                  const apuestaH = typeOfSisterBet == 'string' && ordSel ? orientedSlice(apuestaH_, ordSel/* , true */) : apuestaH_;
                  if (apuestaH) apuestasH[ai] = [...(apuestasH[ai] || []), [aHs[0], apuestaH, typeOfSisterBet, apuestaH_]]; // [codigo del elemento, nombre resumido], la jugada e indicativo si es numerico
                }
              });

              for (let i3 = 0; i3 < apuestas.length; i3++) {
                const apuesta_ = apuestas[i3];
                const typeOfMainBet = apuesta_ instanceof Array ? 'array' : typeof apuesta_;
                const isBetNum = typeOfMainBet == 'string';
                const apuesta = isBetNum && ordSel ? orientedSlice(apuesta_, ordSel/* , true */) : apuesta_;

                for (let i4 = 0; i4 < (apuestasH.length || 1); i4++) {
                  const toBet = apuestasH.length && apuestasH[i4] && apuestasH[i4].length && apuestasH[i4].length >= cantApuestasMin;
                  const calcMonto = +formCopy.apuestamonto;

                  if (!apuestasH.length || toBet) {
                    const theresNum = typeOfMainBet == 'string' || (apuestasH[i4] || []).some(ai4 => ai4 && ai4[2] == 'string');
                    let someHereFailed;
                    for (let secIndIn = 0; secIndIn < (sorteo.section || []).length; secIndIn++) {
                      const [soloApuestas, lasApuestas, losSimbolos, descInTicket, ordenMultiplicador] =
                        [[[codigoPrincipal, resumePrincipal], apuesta, typeOfMainBet, apuesta_], ...(apuestasH[i4] || [])]
                          .reduce((dta, apHer, apHerI) => {
                            const [[codigoCampo, nombreCorto], jugada, tipoApuesta, pristineBet] = apHer;
                            const esNumerico = tipoApuesta == 'string';

                            const elements = self.seccionCodigosElementos[sorteo.section[secIndIn]];
                            if (!elements || !elements.includes(codigoCampo)) return dta;

                            dta[0].push(jugada);
                            if (esNumerico) {
                              dta[1].push({ codigoElemento: codigoCampo, jugada });
                              if (!someHereFailed) someHereFailed = !siderPassed(pristineBet);
                            }
                            else {
                              const simbsArr = (jugada instanceof Array ? jugada : [jugada]).map(simObj => ({ codigoElemento: codigoCampo, jugada: simObj.codigo }));
                              dta[2].push(...simbsArr);
                            }
                            // 
                            dta[4].push([nombreCorto && [nombreCorto, codigoCampo] || ['', codigoCampo], jugada]);
                            // 
                            const shortName = nombreCorto && (tipoApuesta != 'object' || !theresNum) ? (nombreCorto + ':') : '';
                            const idntfyr = esNumerico ? jugada
                              : (tipoApuesta == 'object'
                                ? (!theresNum ? nameCode(jugada) : nameDetailed(jugada))
                                : jugada.map(j => nameCode(j)));
                            dta[3] = dta[3] + (`${apHerI ? (theresNum ? '-y-' : ';') : ''}${shortName}${idntfyr}`);
                            return dta
                          }, [[], [], [], '', []]);

                      if (someHereFailed) /* someBetsFailed = true */continue;
                      else {
                        const soBets0 = soloApuestas?.length && soloApuestas[0];
                        const doBrk2 = makeSorteo({ sorteo, secInd: secIndIn, i1, i2, i3, i4, calcMonto, soloApuestas, lasApuestas, losSimbolos, ordenMultiplicador, toBet, soBets0, descInTicket });

                        if (doBrk2) {
                          breakAll = true;
                          break;
                        }
                      }
                    }
                  }
                  else someBetsFailed = true;
                }
                if (breakAll) break;
              }
            }
          }
          if (breakAll) break;
        }
      }
      if (breakAll) break;
    }
  }

  const losSorteosUpdated = [];
  let lastSrt;
  const sums = {
    theSumRegulars: 0,
    theSumExotics: 0,
    notMaxTicketAllowedByRegulars: (this.maxTicket - this.ticketTotalBy()).toFixed(2),
    notMaxTicketAllowedByExotics: (this.maxTicket - this.ticketTotalBy(true)).toFixed(2)
  };

  for (let si = 0; si < losSorteos.length; si++) {
    const leSrt = losSorteos[si];
    const isExotic = window.atob(leSrt.tipoSorteo) === this.exoticCode;
    const theSum = +sums[!isExotic ? 'theSumRegulars' : 'theSumExotics'];
    const theRes = +sums[!isExotic ? 'notMaxTicketAllowedByRegulars' : 'notMaxTicketAllowedByExotics'];
    const theResOposite = +sums[isExotic ? 'notMaxTicketAllowedByRegulars' : 'notMaxTicketAllowedByExotics'];
    if (theRes < 0 && theResOposite < 0) break;
    else if (theRes < 0) continue;
    const played = +leSrt.played;
    sums[!isExotic ? 'theSumRegulars' : 'theSumExotics'] = theSum + played;
    const remainer = theRes - played;
    sums[!isExotic ? 'notMaxTicketAllowedByRegulars' : 'notMaxTicketAllowedByExotics'] = remainer;
    const passed = remainer >= 0;
    if (passed) {
      losSorteosUpdated.push(leSrt);
      lastSrt = leSrt.idJugada;
    }
  }
  const len = losSorteosUpdated.length;
  const someSumFailed = (sums.notMaxTicketAllowedByRegulars < 0) || (sums.notMaxTicketAllowedByExotics < 0);
  if (sorteos.length || someSumFailed) {
    if (!len) toastMessages = [!len && someSumFailed ? 'Las jugadas fueron descartadas debido a que se superará el total permitido por el ticket' : 'Todas las jugadas fueron descartadas'];
    else if (someBetsFailed || someSumFailed) toastMessages.push([`Algunas jugadas no fueron aceptadas${someSumFailed && !someBetsFailed ? ' debido a que se superará el total permitido por el ticket' : ''}`, { icon: 'info' }]);
  }

  this.addingToTableInProgress = false;
  this.tableDataLoading = false;
  if (!vieneDeModal) this.randomized = false;
  await this.awaiter();

  if (len) this.tableData = [...this.tableData, ...losSorteosUpdated];
  this.checkFocusInput();

  if (!breakAll && len) toastMessages.push([this.labels.betsAdded(len), { icon: 'success' }]);
  else if (len) toastMessages.push([this.labels.onlyAdded(len, this.maxInTable), { icon: 'info' }]);


  if (lastSrt) setTimeout(() => {
    const lastAdded = document.getElementById('bet-jug-' + lastSrt);
    if (lastAdded?.scrollIntoView) lastAdded.scrollIntoView();
  });

  if (refreshSorteosList) this.fetchSorteos();

  for (let tmi = 0; tmi < toastMessages.length; tmi++) {
    const [msg, confs] = typeof toastMessages[tmi] == 'string' ? [toastMessages[tmi], {}] : toastMessages[tmi];
    const dismissabledToast = await this.fireToastMsg(msg, confs);
    if (dismissabledToast.isDismissed && dismissabledToast.dismiss === 'close') {
      this.$swal.stopTimer();
      await this.awaiter();
    }
  }
}

// METODO QUE ESCUCHA LOS ATAJOS DE TECLADOS SELECCIONADOS
async function captureKeyDown(e = null, fromMobile = false, fromOuter = false) {
  const self = this;
  if (e?.ctrlKey) this.ctrlKeyDown = true;
  // console.log(e);
  const theCode = fromMobile ? 13 : (e?.keyCode || e?.which || 1); // SI LA TECLA PRESIONADA NO BRINDA INFORMACION, POR DEFECTO SERA EL CODIGO 1
  if (fromMobile) this.isMobile = true;
  const isGrtrLwr = theCode === 226;
  const setearFoco = isGrtrLwr || (e?.target?.id != 'apuesta-monto' && (e?.key && /[0-9*,.]/.test(e.key))); // TECLA < ó >, ó Números ó * ó . ó ,
  const isShift = e?.shiftKey;
  const [flechaLeft, flechaTop, flechaRight, flechaBottom] = [37, 38, 39, 40].map(kCode => theCode === kCode);

  if (!this.isMobile && this.ctrlKeyDown && (isShift || isGrtrLwr)) {
    const firstElem = document.querySelector(`${isShift ? '.list-of-checks .form-check' : '.figure-grid .figure'} input`);
    if (firstElem?.focus) firstElem.focus();
    if (firstElem && isGrtrLwr && !this.sideFiguresVisible && !this.printingPDF) this.shorcutCases('FiguresSide');
  }

  // CONTROL PARA MOVIMIENTOS CON FLECHAS EN LISTA DE SORTEOS Y TABLA DE FIGURAS
  if (!this.isMobile && !fromOuter && (flechaLeft || flechaTop || flechaRight || flechaBottom)) {
    const actEl = document.activeElement;
    const topBottom = flechaTop || flechaBottom;
    const isFocusOn = actEl?.classList.contains('form-check-input') ? 'sorteo' :
      actEl?.classList.contains('check-figure') ? 'figura' : '';
    const isFig = isFocusOn == 'figura';
    if (isFocusOn && (isFig || (isFocusOn == 'sorteo' && topBottom))) {
      const fatherEl = actEl.parentElement;
      let siblingEl = fatherEl[(isFig && flechaLeft) || flechaTop ? 'previousElementSibling' : 'nextElementSibling'];
      if (isFig && topBottom) siblingEl = siblingEl ? siblingEl[flechaTop ? 'previousElementSibling' : 'nextElementSibling'] : null;
      const inputEl = siblingEl?.firstElementChild;
      if ((isFig || fatherEl.classList.contains('sorteo-check')) && inputEl?.focus) inputEl.focus();
    }
  }

  // RETURNERS------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  if (this.inputInTableHasFocus || e?.target?.className == 'vs__search') return;
  if (!this.isMobile && e?.returnValue && theCode === 27) {
    if (this.$swal.isVisible()) this.$swal.close();
    if (this.sideHelpVisible && !this.printingPDF) this.shorcutCases('Ayuda');
    return;
  }
  // 
  // INHABILITA EL F1, F2, F3, F4, F5, F6, F7 y F8 DE LOS SHORTCUTS POR DEFECTO DEL NAVEGADOR
  const isFs = [112, 113, 114, 115, ...(this.showRefresh ? [116] : []), 117, 118, 119].includes(theCode);
  const isF6 = theCode === 117;
  if (fromMobile || e && isFs) e.preventDefault();
  if ((this.sideHelpVisible || this.visibleModal) && isFs) return;
  // 
  if (this.printingPDF || (!fromMobile && ((this.btnFocused && theCode === 13) || this.isMobile || this.visibleModal/*  || this.figuresModalVisible */ || this.ctrlKeyDown || this.addingToTableInProgress || this.tableDataLoading || this.sorteoDataLoading))) return;
  // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  const notTabShiftBlankOrBackspace = ![8, 9, 16, 32].includes(theCode); // SI ES FALSE ENTONCES SE PRESIONO ALGUNO DE LOS SIGUIENTES: Backspace, Tab, Shift, BlankSpace
  const notArrowLeftRight = ![37, 39].includes(theCode); // SI ES FALSE ENTONCES SE PRESIONO ALGUNO DE LOS SIGUIENTES: ArrowLeft, ArrowRight

  // CONTROL DE ENFOCADO EN INPUTS AUTOMATICO
  if (!isF6 && ((setearFoco && fromOuter) || (setearFoco && !this.checkingFocusInput && this.selectedOnesUse &&
    (e && (e instanceof MouseEvent && typeof e.target?.className == 'string' && e.target.className.includes('inputlot-control')
      ? false : (typeof e.target?.className == 'string' &&
        (e.target.className.includes('monto-control') ||
          (e.target.className.includes('mode-') && (e.target.value.length + 1) < this.forms[this.mode].len) ||
          (e.target.className.includes('-figura') && !this.form['apuesta' + e.target.className.split('-')[0] + 'figura']?.figura?.length)
        ) ? false : true))) &&
    notTabShiftBlankOrBackspace && notArrowLeftRight && (!this.prevKey || this.prevKey != 'Tab'))))
    setTimeout(this.checkFocusInput);
  this.prevKey = e && e.key || '';

  if (setearFoco && fromOuter) return;

  // SE TRATA DE ALGUN ATAJO DE TECLADO
  if (isF6) { // F6 --> foco en tipo seccion
    const btnTipSec = document.querySelector('.btn-tipo-seccion');
    if (btnTipSec?.focus) btnTipSec.focus();
  }
  else if (Object.values(this.shortcutsSet).some(set => set.includes(theCode))) { // SHORTCUTS
    if (theCode === 113) {
      if (self.tableData.length) this.makePrint(113);
    }
    else setTimeout(() => {
      self.shorcutCases(theCode, theCode === 114);
    });
  }
  // SE TRATA DE UN NUMERO, PUESTO ESTA SOBRE ALGUN INPUT
  else {
    this.falser(this.formMonto, 'required', 'wrong');
    Object.keys(this.forms).forEach(frmK => this.falser(this.forms[frmK], 'required', 'wrong'));

    // DEL 0 AL 9 EN EL NUMPAD Y NUMS SOBRE TECLAS DE ABCDARIO (pasa si no los incluye)
    if (![...Array.from(Array(57 - 48 + 1), (_, i) => i + 48), ...Array.from(Array(105 - 96 + 1), (_, i) => i + 96)].includes(theCode)) {
      if (!this.enterPressed && theCode === 13 && this.selectedOnesUse) { // PRESIONO TECLA ENTER/INTRO
        this.enterPressed = true;
        const actEl = document.activeElement;
        await this.awaiter();
        const resFull = this.camposValidatorFull(actEl?.value ? (actEl?.classList?.contains('figure-input') && actEl?.id?.split('-')[1]) : null);
        this.enterPressed = false;
        if (!resFull) return;
        const { apuestas = {}, sorteosYaValidados = [], toastMsg = '', falloPorFecha = false } = resFull;
        this.addToTable(apuestas, sorteosYaValidados, toastMsg ? [toastMsg] : [], falloPorFecha);
      }
    }
  }
}

function captureKeyUp(e = null, fromOuter = false) {
  if (
    this.ctrlKeyDown &&
    e &&
    (
      e.code?.toLowerCase().includes('control') ||
      (e.type?.length && e.type === 'mousedown')
    )
  ) this.ctrlKeyDown = false;

  const theCode = e?.keyCode || e?.which || 1; // SI LA TECLA PRESIONADA NO BRINDA INFORMACION, POR DEFECTO SERA EL CODIGO 1

  if ([38, 40].includes(theCode)) {
    const [flechaTop, flechaBottom] = [38, 40].map(kCode => theCode === kCode);

    // CONTROL PARA MOVIMIENTOS CON FLECHAS EN LISTA DE SORTEOS Y TABLA DE FIGURAS
    if (!this.isMobile && !fromOuter && (flechaTop || flechaBottom)) {
      const actEl = document.activeElement;
      const isFocusOn = actEl?.classList.contains('form-check-input') ? 'sorteo' :
        actEl?.classList.contains('check-figure') ? 'figura' : '';
      const isFig = isFocusOn == 'figura';
      if (isFocusOn && !isFig) {
        const fatherEl = actEl.parentElement;
        const prevSiblingEl = fatherEl.previousElementSibling;
        const inputEl = prevSiblingEl?.firstElementChild;
        if (fatherEl.classList.contains('sorteo-check') && inputEl?.scrollIntoView) inputEl.scrollIntoView();
      }
    }
  }
}

// LLAMA AL METODO CORRESPONDIENTE PARA MOSTRAR EL DIALOGO DE IMPRESION EN PANTALLA
function makePrint(dta) {
  const self = this;
  const isP = !!this.printingPDF;
  if (!this.addingToTableInProgress && !this.tableDataLoading && !this.printingPDF) this.printingPDF = true;
  setTimeout(() => {
    if (!self.addingToTableInProgress && !self.tableDataLoading && !isP) self.shorcutCases(dta);
  });
}

function makeRandom() {
  const self = this;
  this.visibleFields.forEach(vf => {
    const input = vf.tipo == 'input';
    let rnd = input
      ? randNumbers(self.mode == 'p' ? self.forms.p.len : vf.longitudElemento)
      : randBetween(vf.tabla.length - 1);
    const mxSimb = +noStar(vf.maxSimbolo);
    if (input) {
      if (self.mode == 's') {
        const rndSplt = rnd.split('');
        const othrRand = randBetween(rnd.length - 1);
        rndSplt[othrRand] = '*';
        rnd = rndSplt.join('');
      }
      else if (self.mode == 'c') {
        const rndRange = randNumbers(vf.longitudElemento);
        const min = Math.min(rnd, rndRange);
        const max = Math.max(rnd, rndRange);
        const [range1, range2] = self.forms.c.range;
        self.form[`apuesta${vf.codigo}${self.forms.c.code}`] = {
          [range1]: `${min}`,
          [range2]: min == max ? `${min + 1}` : `${max}`
        };
      }

      if (self.mode != 'c') self.form[`apuesta${vf.codigo}${self.forms[self.mode].code}`] = rnd;
    }
    else if (mxSimb) {
      const isDropdown = !!+vf.dropdown;
      const elemsCopy = vf.tabla.map(elem => ({ ...elem }));
      const times2iter = hasStar(vf.maxSimbolo) && mxSimb > 1 ? mxSimb : 1;
      const figures = [];
      let code;

      for (let i = 0; i < times2iter; i++) {
        const randN = randBetween(elemsCopy.length - 1);
        const elem = { ...elemsCopy[randN] };
        const theCode = isDropdown ? elem.codigo : window.atob(elem.codigo);
        code = code ? [...(typeof code == 'string' ? [code] : code), theCode] : theCode;
        figures.push({ codigo: `${theCode}`, nombre: elem.nombre, nombreResumido: elem.nombreResumido, ...(isDropdown ? { img: elem.img } : {}) });
        // if (mxSimb > 1 && times2iter > 1) code = '';
        elemsCopy.splice(randN, 1);
      }

      self.form[`apuesta${vf.codigo}${self.forms.figures.code}`] = {
        nro: isDropdown ? (times2iter == 1 ? figures[0] : figures) : `${code}`,
        figura: isDropdown ? '' : JSON.stringify(figures)
      };
    }
  });
  this.falser(this.formMonto, 'required', 'wrong');
  Object.keys(this.forms).forEach(frmK => this.falser(this.forms[frmK], 'required', 'wrong'));
  this.wronger();
  setTimeout(this.checkFocusInput);
  this.randomized = true;
}

// SETEA VARIABLE QUE MANEJA EL TIPO DE SORTEO POR EL QUE SE ESTA FILTRANDO ACTUALMENTE
// function setActiveProduct (productIdentifier) {
//   const previous = `${this.productActive}`;
//   this.productActive = productIdentifier;
//   if (!this.sorteos_por_productos.length) {
//     this.fireToastMsg(this.labels.notAvailableSorteos, {});
//     this.productActive = previous;
//   }
//   else this.uncheckWhichShould();
// }

// EJECUTA LIBRERIA QUE ABRE EL DIALOGO DE IMPRESION DE DOCUMENTOS EN EL NAVEGADOR
async function printPDF() {
  const self = this;
  let shouldRefresh;
  const theresDecimalsInTotal = !this.permiteDecimal && !Number.isInteger(this.ticketTotal);
  const lowerThanMinimumRegulars = this.ticketTotalBy() < +this.minTicket;
  const lowerThanMinimumExotics = this.ticketTotalBy(true) < +this.minTicket;
  const lowerThanMinimum = lowerThanMinimumRegulars && lowerThanMinimumExotics;
  const tableEmpty = !this.tableData.length;
  const actEl = document.activeElement;
  if (!lowerThanMinimum && !tableEmpty && actEl?.blur) {
    actEl.blur();
    await this.awaiter();
  }
  const confirm = theresDecimalsInTotal || lowerThanMinimum || tableEmpty ? false : await this.fireAlertMsg(`¿Está seguro que desea cobrar ${this.moneda.simboloMoneda}${toMoney(this.totalVenta, this.truncVal)}?`, {});
  if (!confirm || !confirm.isConfirmed) {
    this.printingPDF = false;
    if (theresDecimalsInTotal || lowerThanMinimum || tableEmpty) this.fireToastMsg(
      theresDecimalsInTotal ? `No se permiten decimales en el total del ticket para la moneda ${this.moneda.nombreMoneda}` :
        lowerThanMinimum ? `No se puede imprimir debido a que el total mínimo permitido por ticket es de ${this.moneda.simboloMoneda}${toMoney(this.minTicket, this.truncVal)}` :
          'Debe tener al menos una apuesta para poder imprimir.',
      { icon: 'info' }
    );
    return;
  }

  this.printingPDF = true;
  const { forTicketData, docDef } = require('./docDefinition');
  const { dataStructured, onesEliminated } = this.tableData.reduce((dtas, dta) => {
    const byClosingDate = self.notPassed(dta);
    const isExotic = window.atob(dta.tipoSorteo) === self.exoticCode;
    const byMaxMontoTicket = (isExotic && lowerThanMinimumExotics) || (!isExotic && lowerThanMinimumRegulars);
    if (!shouldRefresh && !byClosingDate) shouldRefresh = true;
    if (!byClosingDate || byMaxMontoTicket) {
      dtas.onesEliminated.push(dta.guid);
      return dtas
    }
    const idJugada = String(dta.idJugada).padStart(4, '0');
    dtas.dataStructured[isExotic ? 1 : 0].push({
      idJugada: window.btoa(idJugada),
      producto: dta.product,
      seccion: dta.section[0],
      numeroSorteo: dta.sorteoNumber,
      modalidad: window.btoa(dta.typeBet),
      ...objMap(
        {
          apuestas: dta.apuestas,
          simbolos: dta.simbolos,
        },
        (pv) => pv.map(j => objMap(j, (hv) => window.btoa(hv), true)),
        true
      ),
      monto: dta.played,
      funcion: window.btoa(dta.functionType),
      azar: window.btoa(dta.azar),
    });
    return dtas
  }, { dataStructured: [[], []], onesEliminated: [] });

  if (onesEliminated.length) {
    const allDataStructured = [...dataStructured[0], ...dataStructured[1]];
    const infoTotal = rounderPlusOne(allDataStructured, 'monto', this.formMonto.precision);
    const fee = this.obtenerImpuesto(infoTotal);
    const infoTotalVenta = this.obtenerTotal(infoTotal, fee);
    await this.fireToastMsg(!allDataStructured.length
      ? 'No se puede continuar con el proceso de impresión debido a que se descartaron todas las jugadas.'
      : `Se descartarán algunas jugadas y se continuará con el proceso de impresión. Monto total de ${self.lotoFee ? 'venta' : 'jugadas'} actualizado: ${this.moneda.simboloMoneda}${toMoney(infoTotalVenta, this.truncVal)}`,
      // por superar su fecha de cierre 
      { icon: 'info' });
    this.removeFromTable(...(!allDataStructured.length ? [true] : [false, onesEliminated, '', 'guid']));
    if (!allDataStructured.length) {
      this.printingPDF = false;
      if (shouldRefresh) this.resetLottery(true, true, true);
      return;
    }
  }
  // console.log('dataStructured', dataStructured);
  const didSenc = !!dataStructured[0].length, didExot = !!dataStructured[1].length;
  
  if (!this.identificadorCode) {
    await this.identificador();
    await this.awaiter(100);
  }

  const body_ = {
    tokenUsuario: this.auth_tokenUsuario,
    identificador: this.identifierOrDefault,
    jugadaSencilla: dataStructured[0],
    jugadaExotica: dataStructured[1],
    codigoMoneda: this.moneda ? window.btoa(this.moneda.codigoMoneda) : null
  };

  const isPrintServiceOk = await this.checkPrintService(', no se cobró el ticket');
  if (isPrintServiceOk) fetchRequest({
    api: self.$apiLoteria + 'CobrarLoteria',
    // api: self.$apiPrueba,
    method: 'POST',
    self,
    body: body_,
    success: async (rjson) => {
      // const rjson = {datos: {
      //   "lineas": [
      //     "Qy4gREUgQVBVRVNUQSBEQVJJT1NDQVI=",
      //     "QVYuIExJQkVSVEFET1IgQ0VSQ0EgREUgSU5URVJDQUJMRSBMT0NBTCAwMiA=",
      //     "RkVDSEE6IDA1LzEwLzIwMjMgICAwOToxMSBBTQ==",
      //     "VEFRVUlMTEE6IDAx"
      //   ],
      //   "mensajeInformativo": "IEVsIEFwb3N0YWRvciBhY2VwdGEgbGFzIFJlZ2xhcyBkZWwgSnVlZ28gcHVibGljYWRhcyBlbiB3d3cuanVlZ2FzbG90ZXJpYS5jb20=",
      //   "ticketExotico": {
      //     "jugadasRechazadas": [],
      //     "mensajeRechazo": [
      //        "Q0hBTkNFIFRSSVBMRSBBIDAxOjAwIFBNIEFQVUVTVEE6MTIzIENVUE8gRElTUE9OSUJMRTpCUy4xMDA=",
      //        "Q0hBTkNFIFRSSVBMRSBCIDAxOjAwIFBNIEFQVUVTVEE6MTIzIENVUE8gRElTUE9OSUJMRTpCUy41MA=="
      //     ],
      //     "serialAlterno": "MTI0NjA=",
      //     "serialTicket": "MTkwMTI0NTMwNjIzMTEyMjAzNTQ=",
      //     "caducidadTicket": "Q0FEVUNBIEEgTE9TIDUgRElBUy4="
      //   },
      //   "ticketSencillo": {
      //     "jugadasRechazadas": [],
      //     "mensajeRechazo": [
      //        "Q0hBTkNFIFRSSVBMRSBBIDAxOjAwIFBNIEFQVUVTVEE6MTIzIENVUE8gRElTUE9OSUJMRTpCUy4xMDA=",
      //        "Q0hBTkNFIFRSSVBMRSBCIDAxOjAwIFBNIEFQVUVTVEE6MTIzIENVUE8gRElTUE9OSUJMRTpCUy41MA=="
      //     ],
      //     "serialAlterno": "MTI0NjA=",
      //     "serialTicket": "MTkwMTI0NTMwNjIzMTEyMjAzNTQ=",
      //     "caducidadTicket": "Q0FEVUNBIEEgTE9TIDUgRElBUy4="
      //   }
      // }};
      const { ticketSencillo = null, ticketExotico = null } = rjson?.datos || {};

      let theresMsgRejected = '';

      const [rejectedArrSenc, rejectedArrExot] = [rjson?.datos && ticketSencillo, rjson?.datos && ticketExotico].map((ticket, tipoI) => {
        if (!ticket)
          return !tipoI && didSenc ? body_.jugadaSencilla.map(js => +window.atob(js.idJugada)) :
            tipoI && didExot ? body_.jugadaExotica.map(je => +window.atob(je.idJugada)) :
              [];
        else if (ticket.mensajeRechazo?.length) theresMsgRejected = ticket.mensajeRechazo.reduce((ulDta, msgR) => `${ulDta}<li>${window.atob(msgR)}</li>`, theresMsgRejected || '<ul class="text-start ps-4">');
        return ticket.jugadasRechazadas.map(rej => +window.atob(rej));
      });

      if (theresMsgRejected) theresMsgRejected += '</ul>';

      if (!shouldRefresh && (rejectedArrSenc.length || rejectedArrExot.length)) shouldRefresh = true;
      const [acceptedOnesSenc, acceptedOnesExot] = [rejectedArrSenc, rejectedArrExot].map((rejectedArr, i) => {
        const filtered = self.tableData.filter(itm => {
          const isExotic = window.atob(itm.tipoSorteo) === self.exoticCode;
          return (!i ? !isExotic : isExotic) && !rejectedArr.includes(itm.idJugada)
        });
        return filtered
      });

      const [tiTotalSenc, tiTotalExot] = [acceptedOnesSenc, acceptedOnesExot].map(acceptedOnes => rounderPlusOne(acceptedOnes, 'played', self.formMonto.precision));
      const [feeSenc, feeExot] = [tiTotalSenc, tiTotalExot].map(tot => self.obtenerImpuesto(tot));
      const [infoTotalVentaSenc, infoTotalVentaExot] = [[tiTotalSenc, feeSenc], [tiTotalExot, feeExot]].map(totNfee => self.obtenerTotal(...totNfee));

      if (acceptedOnesSenc.length || acceptedOnesExot.length) {
        const someRejected = rejectedArrSenc.length || rejectedArrExot.length;
        if (someRejected || theresMsgRejected) {
          if (someRejected) self.removeFromTable(false, [...rejectedArrSenc, ...rejectedArrExot], '', 'idJugada');
          const regularMsg = `Algunas de las jugadas fueron rechazadas. Monto total de ${self.lotoFee ? 'venta' : 'jugadas'} actualizado: ${self.moneda.simboloMoneda}${toMoney(sumaJS(infoTotalVentaSenc, infoTotalVentaExot), self.truncVal)}`;
          await self.fireAlertMsg(
            theresMsgRejected ? `<p>${regularMsg}</p>${theresMsgRejected ? ('<h5>Jugadas Rechazadas:</h5>' + theresMsgRejected) : ''}` : regularMsg,
            {
              icon: "info",
              ...(theresMsgRejected ? { html: true } : { showCancelButton: false }),
              confirmButtonText: "OK"
            }
          );
        }

        const [ticketDtaSencillo, ticketDtaExotico] = [ticketSencillo, ticketExotico].map((ticket, ti) => {
          const ticktDta = !ti ? acceptedOnesSenc : acceptedOnesExot;
          if (!ticket || !ticktDta.length) return null;
          const lines = rjson.datos.lineas;
          const header = lines?.length ? [...lines, window.btoa('SERIAL ALTERNO: ' + window.atob(ticket.serialAlterno))] : [];
          return forTicketData({
            tableData: ticktDta,
            info: {
              header: header.map(l => window.atob(l || '')),
              footer: {
                totalBets: !ti ? tiTotalSenc : tiTotalExot,
                fee: !ti ? feeSenc : feeExot,
                total: !ti ? infoTotalVentaSenc : infoTotalVentaExot,
                trunc: self.truncVal,
                expirationDays: window.atob(ticket.caducidadTicket || ''),
                qrCode: window.atob(ticket.serialTicket),
                mensajeInformativo: window.atob(rjson.datos.mensajeInformativo || ''),
                moneda: self.moneda.simboloMoneda,
              },
              listas: self.apiLists,
              lineSize: self.auth_columnaTicket/* 50 */,
            }
          });
        });

        const [ticketDocSencillo, ticketDocExotico] = [ticketDtaSencillo, ticketDtaExotico].map(ticketDta => ticketDta ? docDef(ticketDta) : null);
        const boolSen = !!ticketDocSencillo, boolExo = !!ticketDocExotico;

        // const now = new Date();
        // const datePart = dateDepocher(now, false, false, true);
        // const timePart = dateTimeOfTheDay(now.getTime(), false, true, '_');
        // const ticketNameSencillo = `ticket_sencillo_${datePart}_${timePart}`;
        // const ticketNameExotico = `ticket_exotico_${datePart}_${timePart}`;

        const showTicket = (...tickets) => {
          if (self.mostrarTicketEnPantalla)
            self.fireAlertMsg(
              tickets.map((ticket, ti) => `${ti ? '<hr>' : ''}<pre class="text-start mx-auto fit-the-content">${ticket}</pre>`).join(''),
              { 
                html: true,
                showConfirmButton: false, 
                controlCancel: true, 
                cancelButtonText: 'Cerrar', 
              }
            );
        };

        const prevImpresion = await new Promise((resolve) => {
          self.saveAndPrintFile({
            // nombreArchivo: boolSen && ticketNameSencillo || ticketNameExotico,
            dataPrint: ticketDocSencillo || ticketDocExotico,
            continua: boolSen && boolExo,
            shouldAwait: self.mostrarTicketEnPantalla,
            // showPrinting: false,
            onLoader: (bool) => { self.printingPDF = bool },
            onDone: (ticketPrev = null, ticket = null) => {
              if (!ticketPrev) {
                self.resetLottery(true, true, !!shouldRefresh);
                if (ticket) showTicket(ticket);
              }
              resolve(ticketPrev);
            }
          })
        });
        if (prevImpresion)
          self.saveAndPrintFile({
            ticketPrev: prevImpresion,
            // nombreArchivo: ticketNameExotico,
            dataPrint: ticketDocExotico,
            showPrinting: false,
            shouldAwait: self.mostrarTicketEnPantalla,
            // showDownloading: false,
            onLoader: (bool) => { self.printingPDF = bool },
            onDone: (ticketPrev = null, ticket = null) => {
              if (ticketPrev) {
                self.resetLottery(true, true, !!shouldRefresh);
                if (ticket) showTicket(ticketPrev, ticket);
              }
            }
          });
      }
      else {
        const msg = 'No se puede continuar con el proceso de impresión debido a que se rechazaron todas las jugadas';
        self.removeFromTable(true);
        if (shouldRefresh) self.resetLottery(true, true, true);
        if (theresMsgRejected) self.fireAlertMsg(
          `<p>${msg}</p>${theresMsgRejected}`,
          {
            icon: "info",
            html: true,
            confirmButtonText: "OK"
          }
        );
        return caseWrong({
          self, error: msg, passMsg: true, loading: 'printingPDF',
          defaultMsg: labels.defaultErrorMsgPlus('obtener la información de los tickets'),
          notAlert: !!theresMsgRejected
        });
      }
    },
    error: (rjson) => {
      const is013 = resCodes(rjson).includes('013');
      if (is013) {
        self.removeFromTable(true);
        self.resetLottery(true, true, true);
        self.fireAlertMsg(
          rjson.msg || '<p>Todas de las jugadas fueron rechazadas</p>',
          {
            icon: "info",
            html: true,
            confirmButtonText: "OK"
          }
        );
      }
      else if (shouldRefresh) self.fetchSorteos();
      return caseWrong({
        self, error: rjson.msg || rjson, passMsg: Boolean(rjson.msg), loading: 'printingPDF',
        defaultMsg: labels.defaultErrorMsgPlus('obtener la información de los tickets'), notAlert: is013
      });
    },
  });
  else this.printingPDF = false;
}

export default {
  methods: {
    captureKeyDown,
    captureKeyUp,
    setFilter,
    addToTable,
    shorcutCases,
    getPermutations,
    getUntil,
    getSerie,
    toTable,
    execSpecial,
    makePrint,
    // setActiveProduct,
    printPDF,
    makeRandom,
    toggleSideFigsFunc,
  },
}