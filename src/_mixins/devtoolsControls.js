import DisableDevtool from 'disable-devtool';

export default {
    mounted() {
        if (this.$blockDevs) DisableDevtool({url: 'about:blank'});
    }
}