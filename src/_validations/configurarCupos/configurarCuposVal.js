import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function() {
    return{        
        producto: { required: cRequired, notSelect: cNotSelect('opción', true) },    
        seccion: { required: cRequired },
        moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },  
        }
}