import { cMin, cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (modalVisible) => ({
    ...(modalVisible ? {
        nombre: { required: cRequired },
        maximoPermuta: { required: cRequired, min: cMin(0) },
        componentes: {
            componente: { notSelect: cNotSelect('opción', true) },
            inicio: { min: cMin(1) },
            longitud: { min: cMin(1) },
            obligatorio: {},
        },
    } : {}),
})