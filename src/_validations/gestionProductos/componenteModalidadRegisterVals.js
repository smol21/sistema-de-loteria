import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default (modalVisible) => ({
    ...(modalVisible ? {
        componenteSeccion: { required: cRequired, notSelect: cNotSelect('opción', true) },
    } : {}),
})