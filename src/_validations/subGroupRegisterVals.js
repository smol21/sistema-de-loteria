import { cCredentials, cEmail, cNotSelect, cNumeric, cOnlyLettersSpecial, cRequired } from '@/_validations/validacionEspeciales';

export default (active = true) => ({
    ...(active ? {
        rif: { required: cRequired, credentials: cCredentials(8) },
        name: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
        phone1: { required: cRequired, numeric: cNumeric(true) },
        phone2: { numeric: cNumeric(true) },
        rName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
        rLastName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
        email: { required: cRequired, email: cEmail },
        state: { required: cRequired, notSelect: cNotSelect('opción', true) },
        city: { required: cRequired, notSelect: cNotSelect('opción', true) },
        municipality: { required: cRequired, notSelect: cNotSelect('opción', true) },
        status: { required: cRequired, notSelect: cNotSelect('opción', true) },
        lister: { required: cRequired, notSelect: cNotSelect('opción', true) },
        // 
        // gamblingType: {},
        esquemaNegociacion: {},
    } : {}),
})