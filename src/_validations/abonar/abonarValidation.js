import { cNumeric, cRequired, cMaxLength } from '@/_validations/validacionEspeciales';

export default {
    saldo: { required: cRequired, numeric: cNumeric(), maxlength: cMaxLength(20) },
    bancoSelect: { required: cRequired },
    typeSelect: { required: cRequired },
    referencia: { required: cRequired, },
    ctaDestino: { required: cRequired, numeric: cNumeric(), maxlength: cMaxLength(20) },
    monto: { required: cRequired, numeric: cNumeric(), maxlength: cMaxLength(20) },  
}