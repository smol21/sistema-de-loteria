import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function() {
    return{
        tipoUsuario: { required: cRequired, notSelect: cNotSelect('opción', true) },
        compradorComercial: { required: cRequired, notSelect: cNotSelect('opción', true) },    
        centroApuesta: { required: cRequired, notSelect: cNotSelect('opción', true) },
        grupo: { required: cRequired },
        taquilla: { required: cRequired },       
    }
}