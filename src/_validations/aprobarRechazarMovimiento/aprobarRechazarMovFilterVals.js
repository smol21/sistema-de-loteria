import {	cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function(value){
	if(value === 24){
        return{
            comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
            moneda: {required: cRequired, notSelect: cNotSelect('opción', true)},
        }
	}else if(value === 22){
		return{
            entidad: {required: cRequired, notSelect: cNotSelect('opción', true)},
            moneda: {required: cRequired, notSelect: cNotSelect('opción', true)},
            grupo: {required: cRequired, notSelect: cNotSelect('opción', true)},
            puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
		}
	}else if(value === 23){
		return{
            moneda: {required: cRequired, notSelect: cNotSelect('opción', true)},
            puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
		}
	}
}