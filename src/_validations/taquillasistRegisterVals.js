import { alphaNum, helpers,minLength} from '@vuelidate/validators'
import { cRequired, cMaxLength, cNotSelect } from '@/_validations/validacionEspeciales';

export default (modalVisible) => (
    {
    ...(modalVisible ? {
        clave: { required: cRequired,
                maxlength: cMaxLength(12),                 
                minLength: helpers.withMessage('Mínimo de carácteres permitidos 6', minLength(6)),
                alphaNum: helpers.withMessage('No acepta caracteres especiales',alphaNum),
        },
        identificador: { required: cRequired, maxlength: cMaxLength(20)},
        estatus: { required: cRequired },
        software: { required: cRequired },
        anular: { required: cRequired },
        cColumna: { required: cRequired, notSelect: cNotSelect('opción', true) },
        impresoraValue: { required: cRequired, notSelect: cNotSelect('opción', true)  },
        login: { 
            required: cRequired, 
            maxlength: cMaxLength(10),             
            minLength: helpers.withMessage('Mínimo de carácteres permitidos 6', minLength(6)),
            alphaNum: helpers.withMessage('No acepta caracteres especiales',alphaNum),
        },
        taquilla: { required: cRequired },
        // tipoUsuario: { required: cRequired, notSelect: cNotSelect('opción', true) },
        horaDesde: { required: cRequired },
        horaHasta: { required: cRequired },
    } : {}),
})