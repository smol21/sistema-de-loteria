import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';
import { helpers } from '@vuelidate/validators'
export default {
    fecha: { required: cRequired },
    radioType: {
        required: helpers.withMessage('Campo requerido', cRequired),
    },
    producto: { required: cRequired, notSelect: cNotSelect('opción', true) },
}