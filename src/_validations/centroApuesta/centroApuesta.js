import {	cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function(value){
	if(value === 24){
    return{
      comprador: {required: cRequired, notSelect: cNotSelect('opción', true)},
			puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
			estatus: {required: cRequired, notSelect: cNotSelect('opción', true)}
    }
	}else{
		return{
			puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
			estatus: {required: cRequired, notSelect: cNotSelect('opción', true)}
		}
	}
}