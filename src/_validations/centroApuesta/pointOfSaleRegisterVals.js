import { cCredentials, cEmail, cNotSelect, cNumeric, cOnlyLettersSpecial, cRequired, cPhoneNumber, cNumberAndDecimals, cMaxLength } from '@/_validations/validacionEspeciales';

export default {
    betCenter: { required: cRequired },
    dfRif: { required: cRequired, credentials: cCredentials(8) },
    dfBusinessName: { required: cRequired/* , onlyLettersSpecial: cOnlyLettersSpecial */ },
    dfPointOfSaleName: { required: cRequired/* , onlyLettersSpecial: cOnlyLettersSpecial */ },
    dfState: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dfCity: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dfMunicipality: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dfAddress: { required: cRequired },
    dfAvStreet: { required: cRequired },
    dfUrbanization: { required: cRequired },
    dfLocalOffice: { required: cRequired },
    dfReferencePoint: { required: cRequired },
    dfPointType: { required: cRequired, notSelect: cNotSelect('opción', true) },
    // 
    dgUbanization: { required: cRequired },
    dgAvStreet: { required: cRequired },
    dgReferencePoint: { required: cRequired },
    dgState: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgCity: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgMunicipality: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgLocalOffice: { required: cRequired },
    dgPhone1: { required: cRequired, phoneNumber: cPhoneNumber, maxlength: cMaxLength(12) },
    dgPhone2: { phoneNumber: cPhoneNumber, maxlength: cMaxLength(12) },
    dgLimitOfSell: { required: cRequired, numberAndDecimals: cNumberAndDecimals(0) },
    dgLister: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgEstatus: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgTipoFondo: { required: cRequired, notSelect: cNotSelect('opción', true) },
    dgEmail: { required: cRequired, email: cEmail },
    // 
    cName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
    cLastName: { required: cRequired, onlyLettersSpecial: cOnlyLettersSpecial },
    cIdetificationCard: { required: cRequired, numeric: cNumeric(true) },
    cPhone1: { required: cRequired, phoneNumber: cPhoneNumber, maxlength: cMaxLength(12) },
    cPhone2: { phoneNumber: cPhoneNumber, maxlength: cMaxLength(12) },
    cEmail: { required: cRequired, email: cEmail },
    // 
    sgSubGrupo: {},
    // 
    // enGamblingType: {},
    enEsquemaNegociacion: {},
}