import { cNumberAndDecimals, cRequired } from '@/_validations/validacionEspeciales';

export default {
    triples: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
    terminales: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
    figuras: { required: cRequired, numberAndDecimals: cNumberAndDecimals(2) },
}