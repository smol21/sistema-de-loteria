import { cRequired } from '@/_validations/validacionEspeciales';

export default {
    producto: { required: cRequired},
    estatus: { required: cRequired},
    fecha: { required: cRequired },
    horario: { required: cRequired },
    turno: { required: cRequired },
    simbolo: { required: cRequired },
}