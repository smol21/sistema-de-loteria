import { cNotSelect, cRequired } from '@/_validations/validacionEspeciales';

export default function(value)  {
 if(value === '21' || value === '20'){
    return{
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
      entidad: { required: cRequired },
    }
 }else if(value === '22' || value === '23')
 {
    return {
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
      entidad: { required: cRequired },
      grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
      puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
    }
  }else if(value === '0'){
    return{
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
      entidad: { },
    }
  }else if(value === '24'){
    return{
      fechaDesde: { required: cRequired },
      fechaHasta: { required: cRequired },
      moneda: { required: cRequired, notSelect: cNotSelect('opción', true) },
      entidad: { required: cRequired },
      grupo: { required: cRequired, notSelect: cNotSelect('opción', true) },
      puntoVenta: {required: cRequired, notSelect: cNotSelect('opción', true)},
      comprador: {required: cRequired, notSelect: cNotSelect('opción', true)}
    }
  }
}
