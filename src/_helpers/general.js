import { useAuth } from "@/store/auth";
import { caseWrong, fetchRequest } from "./fetchToolkit";
import { LABELS as labels } from "./labels";

export const softGray = '#dae8fd';
export const softerGray = '#f9f9f9';
export const softieGray = '#ebebeb';
export const softGrayT = 'rgba(218, 232, 253, 0)';
export const softGrayF = 'rgba(218, 232, 253, 1)';
export const middleGray = '#2155a5';
export const middleGrayT = 'rgba(33, 85, 165, 0.25)';
export const darkGray = '#1c4c96';
export const darkGrayT = '#e1e7f0';
export const graveRed = '#bd5151';
export const graveRedT = 'rgba(189, 81, 81, 0.25)';
export const cancelGray = '#c1c1c1';
export const cancelGrayT = 'rgba(193, 193, 193, 0.25)';
export const grayBorderTable = '#bdcfea';

export const timeOfSideHelp = 150;

export const resCodes = (rjson, asArray = true) => !asArray 
    ? (rjson?.mensaje?.code || rjson?.message?.code || rjson?.mensaje?.codigo || rjson?.message?.codigo || null) 
    : [rjson?.mensaje?.code, rjson?.message?.code, rjson?.mensaje?.codigo, rjson?.message?.codigo || null];

export const msgError = (rjson) => rjson?.mensaje?.description || rjson?.message?.description || rjson?.mensaje?.descripcion || rjson?.message?.descripcion || 
                                   rjson?.mensaje || rjson?.message || '';

export async function logOutSession (self, ask = true, justOut = false, closeAnyway = false, notAlert = false) {
    const { auth_tokenUsuario, auth_login, authClearAll } = useAuth();

    const logOut = () => {
        if (self && self.$router) self.$router.push({ path: "/pages/login" });
        authClearAll();
    };

    if (justOut || (!ask && !auth_tokenUsuario && !auth_login)) {
        logOut();
        return;
    }

    const res = ask && auth_tokenUsuario && auth_login ? (await self.fireAlertMsg('¿Desea cerrar la sesión?', {})) : { isConfirmed: true };
    if (res.isConfirmed && auth_tokenUsuario && auth_login) 
      fetchRequest({ 
          api: `${self.$apiUsuario}CerrarSesion/${auth_tokenUsuario}/${auth_login}`, 
          mock: 'userCerrarSesion',
          method: 'DELETE',
          fromLogOut: true,
          success: logOut, 
          error: (rjson) => {
              const loggedOut = resCodes(rjson).includes('030');
              if (closeAnyway || loggedOut) logOut();
              return caseWrong({
                  self, error: rjson.msg || rjson, passMsg: Boolean(rjson.msg),
                  defaultMsg: labels.defaultErrorMsg,
                  notAlert: notAlert || loggedOut,
              });
          },
      });
    else if (res.isConfirmed && closeAnyway) logOut();
}

export const shortcutsSet = {
    'Ayuda': [112], 
    'Imprimir': [113],
    'ModoApuesta': [67,78,80,83],
    'Azar': [114],
    'FiguresSide': [115], 
    'Refrescar': [116],
    'Horario': [68,69,77,84],
    'TablaFigurasShortcuts': [65,66,67,71,78,79,80,83],/* DE MOMENTO ESTO NO SE USA */
    'TicketsHelpers': [65,66,85],
    'ApuestasHelpers': [70,71,79,81,88],
    'SorteosHelpers': [72,75,89,90],
    'ProductoHelpers': [119],/* F8 - todos los sorteos de un producto tildado */
    'AtajosSorteo': [
        73/* i - doble terminal (01) - MDE= */, 
        74/* j - triple (02) - MDI= */, 
        // 76/* l - doble figura (03) - MDM= */, 
        82/* r - figura de 38 (04) - MDQ= */, 
        86/* v - figura de 75 (05) - MDU= */, 
        87/* w - figura de 76 (06) - MDY= */, 
        118/* F7 - figura de 40 (07) - MDc= */,
    ],
}

export const codesToLetters = {
    65: 'a',
    66: 'b',
    67: 'c',
    68: 'd',
    69: 'e',
    70: 'f',
    71: 'g',
    72: 'h',
    73: 'i',
    74: 'j',
    75: 'k',
    76: 'l',
    77: 'm',
    78: 'n',
    79: 'o',
    80: 'p',
    81: 'q',
    82: 'r',
    83: 's',
    84: 't',
    85: 'u',
    86: 'v',
    87: 'w',
    88: 'x',
    89: 'y',
    90: 'z',
    112: 'f1',
    113: 'f2',
    114: 'f3',
    115: 'f4',
    116: 'f5',
    117: 'f6',
    118: 'f7',
    119: 'f8',
}