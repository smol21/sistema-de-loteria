import { LABELS as labels } from '@/_helpers/labels';

export class Campo {
    constructor(campoJson) {
        this.codigo = campoJson.codigo;
        this.tipo = campoJson.tipo;
        this.etiqueta = campoJson.etiqueta;
        this.obligatorio = campoJson.obligatorio;
        this.longitudElemento = campoJson.longitudElemento || 0;
        this.tabla = campoJson.tabla || [];
        this.maxSimbolo = campoJson.maxSimbolo || '0';
        this.utils = campoJson.utils || {};
        this.ordenJer = campoJson.ordenJer || '';
        this.dropdown = campoJson.dropdown || '0';
        this.permiteEscritura = (campoJson.permiteEscritura && +campoJson.permiteEscritura) || 0;
    }
    resetAttr (attr, val) {
        if (val) this[attr] = val;
    }
}
export class EsquemaNegociacion {
    constructor(esquemaNJson) {
        this.id = esquemaNJson.id;
        this.guid = esquemaNJson.guid;
        this.description = esquemaNJson.description;
        this.applicate = esquemaNJson.applicate;
        this.status = esquemaNJson.status;
        this.sales = esquemaNJson.sales; // sales percentage
        this.utility = esquemaNJson.utility; // utilities percentage
        // this.category = esquemaNJson.category;
        // this.operation = esquemaNJson.operation;
        this.participation = esquemaNJson.participation;
        this.utilityFreq = esquemaNJson.utilityFreq;
        this.participationFreq = esquemaNJson.participationFreq;
    }
}

export class GrupoListero {
    constructor(gListeroJson) {
        this.dfRif = gListeroJson.dfRif;
        this.dfName = gListeroJson.dfName;
        this.dfAddress = gListeroJson.dfAddress;
        this.dfPhone1 = gListeroJson.dfPhone1;
        this.dfPhone2 = gListeroJson.dfPhone2;
        // 
        this.cName = gListeroJson.cName;
        this.cLastName = gListeroJson.cLastName;
        this.cIdetificationCard = gListeroJson.cIdetificationCard;
        this.cPhone1 = gListeroJson.cPhone1;
        this.cPhone2 = gListeroJson.cPhone2;
        // 
        this.dgState = gListeroJson.dgState;
        this.dgCity = gListeroJson.dgCity;
        this.dgMunicipality = gListeroJson.dgMunicipality;
        this.dgEmail = gListeroJson.dgEmail;
        this.dgAssociatedType = gListeroJson.dgAssociatedType;
        this.dgAssociated = gListeroJson.dgAssociated;
        this.dgListerType = gListeroJson.dgListerType;
        this.dgStatus = gListeroJson.dgStatus;
        this.dgPaymentFrequency = gListeroJson.dgPaymentFrequency;
        this.dgRiskFactor = gListeroJson.dgRiskFactor;
        this.dgTicketCaducity = gListeroJson.dgTicketCaducity;
        this.dgValidationCaducity = gListeroJson.dgValidationCaducity;
        this.dgApprovalRequired = gListeroJson.dgApprovalRequired;
        // 
        this.enEsquemaNegociacionData = gListeroJson.enEsquemaNegociacionData;
    }
}

export class PuntoVenta {
    constructor(posJson) {
        this.dfRif = posJson.dfRif;
        this.dfBusinessName = posJson.dfBusinessName;
        this.dfPointOfSaleName = posJson.dfPointOfSaleName;
        this.dfState = posJson.dfState;
        this.dfCity = posJson.dfCity;
        this.dfMunicipality = posJson.dfMunicipality;
        this.dfAddress = posJson.dfAddress;
        this.dfAvStreet = posJson.dfAvStreet;
        this.dfUrbanization = posJson.dfUrbanization;
        this.dfLocalOffice = posJson.dfLocalOffice;
        this.dfReferencePoint = posJson.dfReferencePoint;
        this.dfPointType = posJson.dfPointType;
        //
        this.dgUbanization = posJson.dgUbanization;
        this.dgAvStreet = posJson.dgAvStreet;
        this.dgReferencePoint = posJson.dgReferencePoint;
        this.dgState = posJson.dgState;
        this.dgCity = posJson.dgCity;
        this.dgMunicipality = posJson.dgMunicipality;
        this.dgLocalOffice = posJson.dgLocalOffice;
        this.dgPhone1 = posJson.dgPhone1;
        this.dgPhone2 = posJson.dgPhone2;
        this.dgLimitOfSell = posJson.dgLimitOfSell;
        this.dgLister = posJson.dgLister;
        this.dgEstatus = posJson.dgEstatus;
        this.dgTipoFondo = posJson.dgTipoFondo;
        this.dgEmail = posJson.dgEmail;
        //
        this.cName = posJson.cName;
        this.cLastName = posJson.cLastName;
        this.cIdetificationCard = posJson.cIdetificationCard;
        this.cPhone1 = posJson.cPhone1;
        this.cPhone2 = posJson.cPhone2;
        this.cEmail = posJson.cEmail;
        // 
        this.enEsquemaNegociacionData = posJson.enEsquemaNegociacionData;
        this.sgSubGrupoData = posJson.sgSubGrupoData;
    }
}

export class Sorteo {
    constructor(sorteoJson) {
        this.allTypes = sorteoJson.allTypes || {};
        this.apuestas = sorteoJson.apuestas || [];
        this.asked = sorteoJson.asked;
        this.associated = sorteoJson.associated;
        this.atajo = sorteoJson.atajo || [];
        this.azar = sorteoJson.azar || '0';
        this.bet = sorteoJson.bet;
        this.betGroup = sorteoJson.betGroup || [];
        this.betsShortcut = sorteoJson.betsShortcut || '';
        this.blockedBy = sorteoJson.blockedBy || '0';
        this.codigoTurno = sorteoJson.codigoTurno;
        this.checked = sorteoJson.checked || false;
        this.currentModality = sorteoJson.currentModality;
        this.dateTime = sorteoJson.dateTime;
        this.descApuesta = sorteoJson.descApuesta || '';
        this.fechacierre = sorteoJson.fechacierre;
        this.functionType = sorteoJson.functionType;
        this.guid = sorteoJson.guid;
        this.id = sorteoJson.id;
        this.idJugada = sorteoJson.idJugada;
        this.idJugadas = sorteoJson.idJugadas || [];
        this.idSorteoGroup = `${sorteoJson.associated}-${window.btoa(sorteoJson.name)}-${sorteoJson.product}`;
        // this.isAstral = sorteoJson.isAstral;
        this.isAB = sorteoJson.isAB;
        // this.isFigure = sorteoJson.isFigure;
        // this.isTerminal = sorteoJson.isTerminal;
        this.moneda = sorteoJson.moneda || [];
        this.multiplicador = sorteoJson.multiplicador || [];
        this.name = sorteoJson.name;
        this.nameSorteo = `${sorteoJson.name} ${sorteoJson.dateTime}`;
        this.played = sorteoJson.played;
        this.product = sorteoJson.product;
        this.section = sorteoJson.section;
        this.sign = sorteoJson.sign;
        this.simbolos = sorteoJson.simbolos || [];
        this.sorteoNumber = sorteoJson.sorteoNumber;
        this.tipoSorteo = sorteoJson.tipoSorteo || '';
        // this.type = sorteoJson.type;
        this.typeBet = sorteoJson.typeBet; 
        this.typeBlocking = sorteoJson.typeBlocking || '0';
        this.weightPrice =  sorteoJson.weightPrice || 0;
    }
    nombreSorteo (zero = '') {
        return `${this.name} ${zero}${this.dateTime}`
    }
    numeroSorteo () {
        return `${this.id}-${this.dateTime}-${this.codigoTurno}`
    }
    forFrequent () {
        return `${this.sorteoNumber}-${this.product}`
    }
    betDetail () {
        if (!this.bet) return '';
        const betsArr = this.bet instanceof Array 
            ? this.bet.map(b => typeof b == 'string' ? b 
                : (b instanceof Array 
                    ? b.reduce((allSimbs, simb, simbI, simbArr) => allSimbs+(simbI ? (simbI == simbArr.length - 1 && this.bet.length == 1 ? ' y ' : ', ') : '')+simb.nombre, '') 
                    : b.nombre)) 
            : [typeof this.bet == 'string' ? this.bet : this.bet.nombre];
        const joined = betsArr.reduce((wholeStr, betDesc) => wholeStr + (betDesc ? (+betDesc || /^[0]+$/.test(betDesc) ? ` y ${betDesc}` : `${isNaN(+wholeStr.slice(-1)) ? ' / ' : ' '}${betDesc}`) : ''));
        return joined
    }
    typeBetName (resumen = false) {
        let typeBetName_ = this.allTypes[this.typeBet][resumen ? 'nombreResumido' : 'nombreModalidad'];
        return resumen && typeBetName_ || this.allTypes[this.typeBet].nombreModalidad || ''
    }
    resetAttr (attr, val) {
        this[attr] = val;
    }
    bloqueadoPor () {
        const str = labels.progressLbl(this.blockedBy, labels.wholeBet);
        return str
    }
}

export class SubGrupo {
    constructor(sGrupoJson) {
        this.rif = sGrupoJson.rif;
        this.guid = sGrupoJson.guid;
        this.name = sGrupoJson.name;
        this.phone1 = sGrupoJson.phone1;
        this.phone2 = sGrupoJson.phone2;
        this.rName = sGrupoJson.rName;
        this.rLastName = sGrupoJson.rLastName;
        this.email = sGrupoJson.email;
        this.state = sGrupoJson.state;
        this.city = sGrupoJson.city;
        this.municipality = sGrupoJson.municipality;
        this.status = sGrupoJson.status;
        this.lister = sGrupoJson.lister;
        // 
        this.esquemaNegociacionData = sGrupoJson.esquemaNegociacionData;
    }
}

export class TaquillaSistAdministrativo {
    constructor(taquillaJson) {
        this.serial = taquillaJson.serial;
        this.guid = taquillaJson.guid;
        this.status = taquillaJson.status;
        this.agency = taquillaJson.agency;
        this.agencyName = taquillaJson.agencyName;        
        this.ticketStore = taquillaJson.ticketStore;
        this.betCenter = taquillaJson.betCenter;
    }
}