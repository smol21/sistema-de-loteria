export const LABELS = Object.freeze({
    alphaNumSpace: 'Debe ser solo letras, números y espacios en blanco',
    cNumberLetterSpecial:'Debe ser solo letras, números y espacios en blanco',
    betsAdded: (len) => `Se añadi${len > 1 ? 'eron' : 'ó'} ${len} apuesta${len > 1 ? 's' : ''} con éxito en la tabla.`,
    betsNotExist: (type = null, complement = '', erase = false, items = 'apuestas', verb = 'borrar') => `No existen ${items}${type ? (' de '+type) : ''}${complement ? (' '+complement) : ''} para ${erase ? verb : 'agregar'}.`,
    credentials: 'Debe tener un formato correcto, ej: J-00000000-0',
    defaultErrorMsg: 'Operación no disponible',
    defaultErrorMsgPlus: (mainAction = '', reload = false, extra = '') => `Operación${mainAction ? ' de ' : ''}${mainAction} no disponible${reload ? ', por favor vuelva a cargar la vista.' : '.'}${extra}`,
    deleteAll: (item = 'apuestas', f = true, verb = 'borrar') => `Se ${verb}on tod${f ? 'a' : 'o'}s l${f ? 'a' : 'o'}s ${item} de manera exitosa.`,
    deleteAllSeleted: (item = 'apuestas', f = true, verb = 'borrar') => `Se ${verb}on l${f ? 'a' : 'o'}s ${item} seleccionad${f ? 'a' : 'o'}s de manera exitosa.`,
    deleteOne: (item = 'apuesta', f = true, verb = 'borr') => `Se ${verb}ó un${f ? 'a' : ''} ${item} de manera exitosa.`,
    email: 'Debe tener un formato de correo correcto',
    login: 'Debe tipear letras y/o números sin espacios en blanco',
    max: (max) => `Debe ser un entero positivo menor o igual a ${max}`,
    maximoDeTicket: (money, reached) => `${!reached ? `El monto no puede ser superior a ${money}` : 'Monto inválido, debido a que superará el total permitido por el ticket'}`,
    min: (min) => `Debe ser un entero ${min < 0 ? '' : 'positivo '}mayor o igual a ${min}`,
    minimoDeTicket: (money) => `El monto debe ser como mínimo ${money}`,
    minMult: (min, mult) => `El monto ingresado debe ser mínimo ${min}, y ser múltiplo de ${mult}`,
    // mustHave: (min, max = 0) => `Debe tener ${!max ? `una longitud de ${min}` : (!min ? `máximo ${max}` : `entre ${min} a ${max}`)} caracteres`, OLD
    mustHave: (min, max = 0) => `${!min || !max ? `${!max ? 'Mín' : 'Máx'}imo de c` : 'C'}aracteres permitidos: ${!min || !max ? (!max ? min : max) : `entre ${min} a ${max}`}`,
    mustSelect: (keyword, f = false) => `Debe seleccionar al menos un${f ? 'a' : ''} ${keyword}`,
    notAvailableSorteos: 'No hay sorteos disponibles para esta opción.',
    notInList: 'No es un código válido para la lista de elementos',
    numberAndDecimals: (max) => `Debe tener formato numérico${max ? (' y solo se admiten máximo '+max) : ', se admiten'} decimales`,
    numeric: (int = false) => 'Debe ser solo números'+(int ? ' enteros' : ''),
    onlyAdded: (len, maxInTable) => `Solo se añadi${len > 1 ? 'eron' : 'ó'} ${len} apuesta${len > 1 ? 's' : ''} en la tabla, debido a que el límite máximo permitido de apuestas es ${maxInTable}.`,
    onlyLettersSpecial: 'Debe ser solo letras',
    ordenSeleccion: (caseSelPos) => caseSelPos ? 'El segundo dígito debe ser absolutamente mayor al primero' : 'Debe tener un formato correcto, ej: 0,3 o -0,2',
    passwordRgx: 'Debe tipear letras y/o números sin espacios en blanco',
    phoneNumber: 'Debe tener un formato correcto, ej: 0000-0000000',
    precision: (len, decimals) => `El monto ingresado debe tener una longitud máxima de ${len} dígitos, y una precisión de hasta ${decimals} decimales.`,
    progressLbl: (perc, labl) => +perc == 100 ? labl : (perc+'%'),
    required: 'Campo requerido',
    sameAsPassword: 'Las claves no coinciden',
    size: (len, gt0 = false, min = 0, custom = false) => gt0 ? `El monto ingresado debe ser mayor ${custom ? `o igual a ${len}` : 'que cero (0)'}` : min && min < len ? `Debe tipear entre ${min} a ${len} números` : `Debe tipear ${len} números`,
    sureDelete: (all = true, item = 'apuesta', f = true, extra = '', plural = 's', verb = 'borrar', itemAux = '') => `¿Estás seguro que deseas ${verb} ${all ? ('tod'+(f ? 'a' : 'o')+'s l'+(f ? 'a' : 'o')+'s') : ('est'+(f ? 'a' : 'e'))} ${all ? (itemAux || item) : item}${all ? plural : ''}${extra}?`,
    sureDeleteSelected: (item = 'apuesta', f = true, plural = 's', verb = 'borrar', itemAux = '') => `¿Estás seguro que deseas ${verb} l${f ? 'a' : 'o'}s ${itemAux || item}${plural} seleccionad${f ? 'a' : 'o'}s?`,
    themNotInList: 'Debe tipear código(s) válido(s) para la lista de elementos',
    urlSimbolo: 'Debe tener un formato correcto, ej: Producto/Simbolos/{codigo}.png (png/jpg/jpeg)',
    wholeBet: 'número completo',
});