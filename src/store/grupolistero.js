import { defineStore } from 'pinia';
import { listeros } from '@/_helpers/dataPruebas';
import { useSubGrupo } from './subgrupo';
import { GrupoListero, SubGrupo } from '@/_helpers/classes';

const saveToLocal = (listeros) => {
    localStorage.setItem('listeros', JSON.stringify(listeros));
};
const getFromLocal = () => {
    try {
        const localListeros = JSON.parse(localStorage.getItem('listeros') || '[]');
        const listeros_ = localListeros.length ? localListeros : listeros;
        if (!localListeros.length) saveToLocal(listeros_);
        return listeros_
    } catch (er) {
        return null
    }
};

// MOMENTANEO
export const useGrupolistero = defineStore('grupolistero', {
    state: () => {
        return {
            listeros: [...getFromLocal()],
        }
    },
    getters: {
        listerosEsquemas: (state) => state.listeros.reduce((listers, lister) => ({...listers, [lister.dfRif]: lister.enEsquemaNegociacionData}), {}),
        listerOptions: (state) => state.listeros.map(lister => ({ label: lister.dfName, value: lister.dfRif })),
    },
    actions: {
        addListero(listero) {
            this.listeros = [...this.listeros, listero];
            saveToLocal(this.listeros);
        },
        editListero(data) {
            const listeros_ = [...this.listeros];
            const ind = listeros_.findIndex(listero => listero.dfRif === data.dfRif);
            if (ind > -1) listeros_[ind] = {...listeros_[ind], ...data};
            this.listeros = listeros_;
            saveToLocal(this.listeros);

            if (ind > -1) {
                const { editSubGrupo, subGrupos } = useSubGrupo();
                const currLister = new GrupoListero(this.listeros[ind]);
                const { dfRif, enEsquemaNegociacionData: esquemasId } = currLister;
                const possibleUpdates = subGrupos.filter(subG => subG.lister === dfRif);
                possibleUpdates.forEach(subG => {
                    const sbG = new SubGrupo(subG);
                    const [esquemes, toClean] = sbG.esquemaNegociacionData.reduce((ids, id_) => [
                        ...(esquemasId.includes(id_) 
                            ? [[...(ids[0] || []), id_], [...(ids[1] || [])]] 
                            : [[...(ids[0] || [])], [...(ids[1] || []), id_]])
                    ], []);
                    if (toClean.length) editSubGrupo({...sbG, esquemaNegociacionData: esquemes});
                });
            }
        },
        deleteListero(rif) {
            const listeros_ = [...this.listeros];
            const ind = listeros_.findIndex(item_ => item_.dfRif === rif);
            if (ind > -1) listeros_.splice(ind, 1);
            this.listeros = [...listeros_];
            saveToLocal(this.listeros);
        },
    },
})