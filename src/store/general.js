import { defineStore } from 'pinia'

const saveToLocal = (name, value) => {
    localStorage.setItem(name, JSON.stringify(value));
};

const getFromLocal = (name) => {
    try {
        const data = localStorage.getItem(name) ? JSON.parse(localStorage.getItem(name)) : null;
        return data
    } catch (er) {
        return null
    }
};

export const useGeneral = defineStore('general', {
    state: () => {
        return {
            generalLoading: false,
            ...[
                'dashboard',
                'loteria',
                'loteria2',
                'esquemaNegociacion',
                'EsquemaNegociacionProgramada',
                'grupoListero',
                'subgrupo',
                'puntoVenta',
                'taquillaSistAdministrativo',
                'ticketRepeat',
                'anularTicket',
                'resultTicket',
                'ventaPorFecha',
                'winnerTicket',
                'reporteCuadreCaja',
                'reporteLiquidacion',
                'ticketValidate',
                'gestionComponente',
                'gestionModalidad',
                'gestionProducto',
                'gestionarHorario',
                'configurarSorteo',
                'generarSorteo',
                'reporteResultados',
                'listadoTicketAnulado',
                'listadoTicketPremiado',
                'registrarAbono',
                'cambiarClave',
                'reporteEstadoCuenta',
                'procesarMovimiento',
                'bloquearNumero',
                'monitorVenta',
                'gestionGrupo',
                'configurarCupos',
                'gestionUsuarioAdmin',
                'gestionComprador',
				'buzonMensajes'
            ].reduce((attrs, attr) => ({...attrs, [attr+'Loaded']: false}), {}),
            dailyResult: false,
            dateOfTimeOut: getFromLocal('dateOfTimeOut') ?? null,
        }
    },
    actions: {
        setGralVal(payload) {
            this[payload[0]] = payload[1]
        },
        setGralValLocal(payload) {
            this[payload[0]] = payload[1];
            saveToLocal(payload[0], payload[1]);
        },
        setLoaded(payload) {
            this[payload[0]+'Loaded'] = payload[1]
        },
    },
})