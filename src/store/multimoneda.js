import { numberAndComma } from '@/_helpers/formatters';
import { defineStore } from 'pinia'

const saveToLocal = (name, value) => {
    localStorage.setItem(name, JSON.stringify(value));
};

const getFromLocal = (name) => {
    try {
        const data = localStorage.getItem(name) ? JSON.parse(localStorage.getItem(name)) : null;
        return data
    } catch (er) {
        return null
    }
};

export const useMultimoneda = defineStore('multimoneda', {
    state: () => {
        return {
            monedaB: getFromLocal('monedaB') ?? null,
            moneda: getFromLocal('moneda') ?? null,
            minTicket: getFromLocal('minTicket') ?? null,
            maxTicket: getFromLocal('maxTicket') ?? null,
            monedas: getFromLocal('monedas') ?? [],
            permiteDecimal: getFromLocal('permiteDecimal') ?? null,
        }
    },
    getters: {
        monedaBase: (state) => state.monedaB || state.moneda,
    },
    actions: {
        setMoneda(payload) {
            const { codigoMoneda, nombreMoneda, simboloMoneda, valorMoneda, monMinimoTic, monMaximoTic, permiteDecimal } = payload;
            const moneda = codigoMoneda && nombreMoneda && simboloMoneda ? { codigoMoneda, nombreMoneda, simboloMoneda, valorMoneda, monMinimoTic, monMaximoTic, permiteDecimal } : null;
            this.minTicket = monMinimoTic;
            this.maxTicket = monMaximoTic;
            this.permiteDecimal = permiteDecimal;
            this.moneda = moneda;
            saveToLocal('moneda', moneda);
            saveToLocal('minTicket', monMinimoTic);
            saveToLocal('maxTicket', monMaximoTic);
            saveToLocal('permiteDecimal', permiteDecimal);
        },
        setMonedas(payload) {
            const self = this;
            const monedas = payload instanceof Array ? payload.reduce((coins, coin, i) => {
                const { codigoMoneda, nombreMoneda, simboloMoneda, valorMoneda/* , monedaBase */, monMinimoTic, monMaximoTic, permiteDecimal } = coin;
                const moneda = codigoMoneda && nombreMoneda && simboloMoneda && valorMoneda ? { 
                    codigoMoneda: window.atob(codigoMoneda), 
                    nombreMoneda: /* window.atob( */nombreMoneda/* ) */, 
                    simboloMoneda: window.atob(simboloMoneda),
                    valorMoneda: numberAndComma(window.atob(valorMoneda), true), 
                    monMinimoTic: +monMinimoTic, 
                    monMaximoTic: +monMaximoTic,
                    permiteDecimal: !!+window.atob(permiteDecimal)
                } : null;
                if (moneda) coins.push(moneda);
                // const monB = monedaBase && window.atob(monedaBase);
                if (/* +monB ||  */!i) {
                    self.setMoneda(moneda);
                    self.monedaB = moneda;
                    saveToLocal('monedaB', moneda);
                }
                return coins
            }, []) : [];
            this.monedas = monedas;
            saveToLocal('monedas', monedas);
        },
    },
})