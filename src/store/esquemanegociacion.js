import { defineStore } from 'pinia'
import { testEsquemaData } from '@/_helpers/dataPruebas';
import { useGrupolistero } from './grupolistero';
import { usePuntoventa } from './puntoventa';
import { GrupoListero, PuntoVenta } from '@/_helpers/classes';

const saveToLocal = (esquemas) => {
    localStorage.setItem('esquemas', JSON.stringify(esquemas));
};
const getFromLocal = () => {
    try {
        const localEsquemas = JSON.parse(localStorage.getItem('esquemas') || '[]');
        const esquemas = localEsquemas.length ? localEsquemas : testEsquemaData();
        if (!localEsquemas.length) saveToLocal(esquemas);
        return esquemas
    } catch (er) {
        return null
    }
};

// MOMENTANEO
export const useEsquemanegociacion = defineStore('esquemanegociacion', {
    state: () => {
        return {
            esquemas: [...getFromLocal()],
        }
    },
    actions: {
        addEsquema(esquema) {
            this.esquemas = [...this.esquemas, esquema];
            saveToLocal(this.esquemas);
        },
        editEsquema(data) {
            const esquemas_ = [...this.esquemas];
            const ind = esquemas_.findIndex(esquema => esquema.guid === data.guid);
            if (ind > -1) esquemas_[ind] = {...esquemas_[ind], ...data};
            this.esquemas = esquemas_;
            saveToLocal(this.esquemas);
        },
        deleteEsquema(id) {
            const esquemas_ = [...this.esquemas];
            const ind = esquemas_.findIndex(item_ => item_.guid === id);
            if (ind > -1) esquemas_.splice(ind, 1);
            this.esquemas = [...esquemas_];
            saveToLocal(this.esquemas);

            const esquemasId = this.esquemas.map(esquema => esquema.id);
            const { listeros, editListero } = useGrupolistero();
            const { puntosVenta, editPuntoVenta } = usePuntoventa();

            // ACTUALIZAR DEPENDENCIAS EN RELACIONES
            [listeros, puntosVenta].forEach((current, i) => {
                current.forEach(item => {
                    const curr = !i ? new GrupoListero(item) : new PuntoVenta(item);
                    const [esquemes, toClean] = curr.enEsquemaNegociacionData.reduce((ids, id_) => [
                        ...(esquemasId.includes(id_) 
                            ? [[...(ids[0] || []), id_], [...(ids[1] || [])]] 
                            : [[...(ids[0] || [])], [...(ids[1] || []), id_]])
                    ], []);
                    if (toClean.length) (!i ? editListero : editPuntoVenta)({...curr, enEsquemaNegociacionData: esquemes});
                });
            });
        },
    },
})