import { defineStore } from 'pinia'

export const useSideHelp = defineStore('sidehelp', {
    state: () => {
        return {
            sideHelpVisible: false,
            sideFiguresVisible: false,
            headerHeight: '65px',
        }
    },
    getters: {
        isSideHelpVisible: (state) => state.sideHelpVisible,
    },
    actions: {
        toggleSideHelp() {
            this.sideHelpVisible = !this.sideHelpVisible
        },
        toggleSideFigures() {
            this.sideFiguresVisible = !this.sideFiguresVisible
        },
        updateSideHelpVisible(payload) {
            this.sideHelpVisible = payload
        },
        updateSideFiguresVisible(payload) {
            this.sideFiguresVisible = payload
        },
        updateHeaderHeight(payload) {
            this.headerHeight = payload + 'px'
        },
    },
})