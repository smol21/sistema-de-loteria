// ANIMALITOS
const { animals: animalsReduced } = ['Carnero-red','Toro-black','Ciempiés-red','Alacrán-black','León-red','Sapo-black','Loro-red','Ratón-black','Aguila-red','Tigre-black','Gato-black','Caballo-red','Mono-black','Paloma-red','Zorro-black','Oso-red','Pavo-black','Burro-red','Cabra-red','Cochino-black','Gallo-red','Camello-black','Cebra-red','Iguana-black','Gallina-red','Vaca-black','Perro-red','Zamuro-black','Elefante-black','Caimán-red','Lapa-black','Ardilla-red','Pescado-black','Venado-red','Jirafa-black','Culebra-red']
    .reduce((animalsData, animal, i) => {
        const [name, color] = animal.split('-');
        const horizontal = animalsData.animalPos === 1 ? '-5px' : animalsData.animalPos === 2 ? '-72px' : '-136px', vertical = `-${animalsData.countDown}px`;
        animalsData.countDown += animalsData.animalPos === 3 ? 41 : 0;
        animalsData.animalPos += (animalsData.animalPos === 1 || animalsData.animalPos === 2 ? 1 : (-1 * (animalsData.animalPos - 1)));
        animalsData.animals[`0${i + 1}`] = { name, color, image: `${horizontal} ${vertical}` };
        return animalsData;
    }, { animals: {}, countDown: 42, animalPos: 1 });

export const theAnimalitos = {
    '0' : { name: 'Delfin', color: 'green', image: '-110px -3px' },
    '00' : { name: 'Ballena', color: 'green', image: '-13px -5px' },
    ...animalsReduced
};

// SIGNOS
const { signs: signsReduced } = ['Aries','Tauro','Geminis','Cancer','Leo','Virgo','Libra','Escorpio','Sagitario','Capricornio','Acuario','Piscis']
    .reduce((signsData, sign, i) => {
        const horizontal = signsData.signPos === 1 ? '-7px' : signsData.signPos === 2 ? '-72px' : '-136px', vertical = `-${signsData.countDown}px`;
        signsData.countDown += signsData.signPos === 3 ? 41 : 0;
        signsData.signPos += (signsData.signPos === 1 || signsData.signPos === 2 ? 1 : (-1 * (signsData.signPos - 1)));
        signsData.signs[`${i + 1}`] = { name: sign, image: `${horizontal} ${vertical}` };
        return signsData;
    }, { signs: {}, countDown: 1, signPos: 1 });

export const theSignos = {
    ...signsReduced
};